var express = require("express"),
    app = express(),
    server = require("http").createServer(app),
    io = require("socket.io")(server, {
        path: "/dm_ws"
    }),
    fs = require("fs"),
    bodyParser = require("body-parser"),
    backend = require("./backend/routing"),
    api = require("./backend/api"),
    auth_user = require("./backend/auth_user"),
    scheduled_tasks_manager = require("./backend/scheduled_tasks_manager"),
    exphbs = require("express-handlebars"),
    config = require("./config.js"),
    moment = require("moment"),
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev",
    path = require("path"),
    layer_json_gen = require("./backend/layer_json_gen");

require("events").EventEmitter.defaultMaxListeners = 150;
process.setMaxListeners(0);

if (config.use_websockets) {
    require("./backend/websockets.js")(io);
}

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.engine("handlebars", exphbs({defaultLayout: "main"}));
app.engine("handlebars", exphbs({
    defaultLayout: "main",
    layoutsDir: path.join(__dirname, "views/layouts")
}));
app.set("view engine", "handlebars");
app.set("views", path.join(__dirname, "views"));

app.use(auth_user);

app.use("/resources/images", express.static(path.join(__dirname, "resources/images")));
app.use("/resources/libs", express.static(path.join(__dirname, "node_modules")));
app.use("/resources/font-awesome", express.static(path.join(__dirname, "resources/font-awesome")));
app.use("/resources/fonts", express.static(path.join(__dirname, "resources/fonts")));
app.use("/classic", express.static(path.join(__dirname, "classic")));
app.use("/ext", express.static(path.join(__dirname, "ext")));
app.use("/tmp", express.static(path.join(__dirname, "tmp")));

app.get("/classic.json", function (req, res) {
    res.sendFile(path.join(__dirname, "classic.json"));
});

app.get("/bootstrap.js", function (req, res) {
    res.sendFile(path.join(__dirname, "bootstrap.js"));
});

if (env === "dev") {
    app.use("/build", express.static(path.join(__dirname, "build")));
    app.use("/.sencha", express.static(path.join(__dirname, ".sencha")));
    app.use("/app", express.static(path.join(__dirname, "app")));
    app.get("/app.js", function (req, res) {
        res.sendFile(path.join(__dirname, "app.js"));
    });
}

app.use("/backend", backend);
app.use("/api", api);

app.get("/", function (req, res) {
    res.redirect("app");
});

app.get("/app", function (req, res) {
    res.render("loader");
});

server.setTimeout(1200000);

var listen_host = config.listenHost ? config.listenHost : "localhost";

server.listen(config.port, listen_host, function () {
    console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - listening on *:" + config.port);
});

if (!fs.existsSync(path.join(__dirname, "tmp"))) {
    fs.mkdirSync(path.join(__dirname, "tmp"));
}

scheduled_tasks_manager.init();

layer_json_gen.prepare(undefined, undefined, undefined, undefined);

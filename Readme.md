# Dienstemanager

## Lokale Entwicklungsumgebung einrichten
Es werdenn die ExtJS Bibliotheken benötigt. Diese können [hier](https://www.sencha.com/legal/GPL/) herunter geladen werden.
Der Inhalt muss dann nach /ext entzippt werden.

NodeJS Module installieren:
```
npm install
```
config_template.js umbenennen in config.js und Datenbankverbindung etc. setzen

Projekt starten:
```
npm start
```

Als Buildtool wird Sencha CMD eingesetzt. [download](https://www.sencha.com/products/extjs/cmd-download/):

Starten des lokalen Webservers (Entwicklungsumgebung):
```
sencha app watch
```
Production Build erzeugen:
```
sencha app build production
```

In der app.json können die Ausgabepfade für die Builds konfiguriert werden (standard ist D:\Entwicklung\Dienstemanager\[prod/test/dev]_build)

## Installation
(siehe [Benutzhandbuch](https://bitbucket.org/geowerkstatt-hamburg/dienstemanager/downloads/Benutzerhandbuch_latest_stable.pdf))

- Download der letzten stabilen Version: https://bitbucket.org/geowerkstatt-hamburg/dienstemanager/downloads/Dienstemanager_latest_stable.zip
- Heruntergeladenes Zip entpacken
- Entpackten Ordner auf einen Webserver verschieben
- npm install ausführen um NodeJS Module zu installieren
> zur Verwendung ohne AD Authentifizierung package_no_ad.json verwenden!
- Eine PostgreSQL Datenbank erstellen und einen Benutzer mit dem Namen „dienstemanager“ erzeugen.
> Wenn das metadaten_modul aktiviert ist, wird zusätzlich die PostGIS Erweiterung benötigt!
- SQL Script init_db.sql ausführen
- config.js anpassen
- Vorschauportal konfigurieren:
> https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable/doc/doc.md
- DiensteManager in einem beliebigen Browser öffnen

Ext.define("DiensteManager.store.PortalLayerList", {
    extend: "Ext.data.Store",
    alias: "store.portallayerlist",
    storeId: "PortalLayerList",
    model: "DiensteManager.model.PortalLayerList",
    sorters: [{
        property: "layer_id",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getportallayerlist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

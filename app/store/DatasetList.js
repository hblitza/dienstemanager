Ext.define("DiensteManager.store.DatasetList", {
    extend: "Ext.data.Store",
    alias: "store.datasetlist",
    storeId: "DatasetList",
    model: "DiensteManager.model.Dataset",
    autoLoad: true,
    sorters: [{
        property: "dataset_name",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdatasetlist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

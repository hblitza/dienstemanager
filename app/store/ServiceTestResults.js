Ext.define("DiensteManager.store.ServiceTestResults", {
    extend: "Ext.data.Store",
    alias: "store.servicetestresults",
    storeId: "ServiceTestResults",
    model: "DiensteManager.model.ServiceTestResult",
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservicetestresults",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

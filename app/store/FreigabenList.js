Ext.define("DiensteManager.store.FreigabenList", {
    extend: "Ext.data.Store",
    alias: "store.freigabenlist",
    storeId: "FreigabenList",
    model: "DiensteManager.model.Freigabe",
    sorters: [{
        property: "name",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getfreigabenlist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

Ext.define("DiensteManager.store.LayerLinks", {
    extend: "Ext.data.Store",
    alias: "store.layerlinks",
    storeId: "LayerLinks",
    model: "DiensteManager.model.LayerLink",
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayerlinks",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

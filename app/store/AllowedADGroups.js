Ext.define("DiensteManager.store.AllowedADGroups", {
    extend: "Ext.data.Store",
    alias: "store.allowedadgroups",
    storeId: "AllowedADGroups",
    model: "DiensteManager.model.ADGroup",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "allowed_groups"
        }
    }
});

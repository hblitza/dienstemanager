Ext.define("DiensteManager.store.LinkedFreigaben", {
    extend: "Ext.data.Store",
    alias: "store.linked_freigaben",
    storeId: "LinkedFreigaben",
    model: "DiensteManager.model.LinkedFreigabe",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlinkedfreigaben",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

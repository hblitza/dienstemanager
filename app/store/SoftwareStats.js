Ext.define("DiensteManager.store.SoftwareStats", {
    extend: "Ext.data.Store",
    alias: "store.softwarestats",
    storeId: "SoftwareStats",
    model: "DiensteManager.model.Statistic",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getsoftwarestats",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

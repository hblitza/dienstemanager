Ext.define("DiensteManager.store.ConfigDMInstances", {
    extend: "Ext.data.Store",
    alias: "store.configdminstances",
    storeId: "ConfigDMInstances",
    model: "DiensteManager.model.ConfigDMInstance",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getconfigdminstances",
        method: "GET",
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});

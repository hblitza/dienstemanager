Ext.define("DiensteManager.store.LayerStats", {
    extend: "Ext.data.Store",
    alias: "store.layerstats",
    storeId: "LayerStats",
    model: "DiensteManager.model.Statistic",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayerstats",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

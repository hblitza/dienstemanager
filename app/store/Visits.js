Ext.define("DiensteManager.store.Visits", {
    extend: "Ext.data.Store",
    alias: "store.visits",
    storeId: "Visits",
    model: "DiensteManager.model.Visits",
    sorters: [{
        property: "month",
        direction: "ASC"
    }, {
        property: "visits_total",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getvisits",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

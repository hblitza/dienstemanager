Ext.define("DiensteManager.store.JiraLinkedTickets", {
    extend: "Ext.data.Store",
    alias: "store.jiraLinkedTickets",
    storeId: "JiraLinkedTickets",
    model: "DiensteManager.model.JiraLinkedTickets",
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getLinkedTickets",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

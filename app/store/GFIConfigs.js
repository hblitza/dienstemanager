Ext.define("DiensteManager.store.GFIConfigs", {
    extend: "Ext.data.Store",
    alias: "store.gficonfigs",
    storeId: "GFIConfigs",
    model: "DiensteManager.model.GFIConfig",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "gficonfig"
        }
    }
});

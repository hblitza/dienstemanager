Ext.define("DiensteManager.store.ScheduledTasks", {
    extend: "Ext.data.Store",
    alias: "store.scheduledtasks",
    storeId: "ScheduledTasks",
    model: "DiensteManager.model.ScheduledTask",
    autoLoad: true,
    sorters: [{
        property: "name",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getscheduledtasks",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

Ext.define("DiensteManager.store.WMSTests", {
    extend: "Ext.data.Store",
    alias: "store.wmstests",
    storeId: "WMSTests",
    model: "DiensteManager.model.Test",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "tests"
        }
    }
});

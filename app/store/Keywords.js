Ext.define("DiensteManager.store.Keywords", {
    extend: "Ext.data.Store",
    alias: "store.keywords",
    storeId: "Keywords",
    model: "DiensteManager.model.Keyword",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdistinctkeywords",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

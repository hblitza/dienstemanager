Ext.define("DiensteManager.store.ADGroups", {
    extend: "Ext.data.Store",
    alias: "store.adgroups",
    storeId: "ADGroups",
    model: "DiensteManager.model.ADGroup",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getadgroups",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

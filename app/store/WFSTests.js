Ext.define("DiensteManager.store.WFSTests", {
    extend: "Ext.data.Store",
    alias: "store.wfstests",
    storeId: "WFSTests",
    model: "DiensteManager.model.Test",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "tests"
        }
    }
});

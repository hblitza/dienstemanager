Ext.define("DiensteManager.store.ServiceMDSets", {
    extend: "Ext.data.Store",
    alias: "store.servicemdsets",
    storeId: "ServiceMDSets",
    model: "DiensteManager.model.ServiceMDSet",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "serviceMDSets"
        }
    }
});

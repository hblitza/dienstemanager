Ext.define("DiensteManager.store.Comments", {
    extend: "Ext.data.Store",
    alias: "store.comments",
    storeId: "Comments",
    model: "DiensteManager.model.Comment",
    sorters: [{
        property: "post_date",
        direction: "DESC"
    }],
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getcomments",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});

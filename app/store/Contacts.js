Ext.define("DiensteManager.store.Contacts", {
    extend: "Ext.data.Store",
    alias: "store.contacts",
    storeId: "Contacts",
    model: "DiensteManager.model.Contact",
    autoLoad: true,
    sorters: [{
        property: "surname",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getcontacts",
        method: "GET",
        reader: {
            type: "json",
            rootProperty: "results",
            totalProperty: "total"
        }
    }
});

Ext.define("DiensteManager.store.Config", {
    extend: "Ext.data.JsonStore",
    alias: "store.config",
    storeId: "Config",
    model: "DiensteManager.model.Config",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getclientconfig",
        method: "GET",
        reader: {
            type: "json",
            rootProperty: "config"
        }
    }
});

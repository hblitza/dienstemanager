Ext.define("DiensteManager.Application", {
    extend: "Ext.app.Application",

    requires: [
        "*"
    ],

    name: "DiensteManager",

    stores: [
        "ADContacts",
        "ADGroups",
        "AllowedADGroups",
        "Comments",
        "Config",
        "ConfigMetadataCatalogs",
        "ConfigDMInstances",
        "Contacts",
        "DatasetList",
        "Datasets",
        "DeletedServiceList",
        "FreigabenList",
        "GFIConfigs",
        "JiraTickets",
        "JiraLinkedTickets",
        "Keywords",
        "LayerLinks",
        "LayerList",
        "Layers",
        "LayerStats",
        "LinkedContacts",
        "LinkedFreigaben",
        "NavigationTree",
        "NewLayer",
        "PortalList",
        "PortalLayerList",
        "Portals",
        "ScheduledTasks",
        "ServiceList",
        "ServiceMDSets",
        "ServiceStats",
        "ServiceTestResults",
        "ServiceTestResultsChecked",
        "SoftwareStats",
        "Visits",
        "VisitsTop",
        "VisitsTotal",
        "WFSTests",
        "WMSTests"
    ],

    init: function () {
    },

    launch: function () {
        var me = this,
            store = Ext.getStore("Config");

        store.on("load", function () {
            me.prepareConfig();

            me.setMainView("DiensteManager.view.main.Viewport");

            if (!DiensteManager.Config.getMetadatenModul()) {
                Ext.getStore("NavigationTree").findNode("text", "Datensätze").remove();
            }

            if (!DiensteManager.Config.getFreigabenModul()) {
                Ext.getStore("NavigationTree").findNode("text", "Freigaben").remove();
            }

            if (!DiensteManager.Config.getVerwaltungModul()) {
                Ext.getStore("NavigationTree").findNode("text", "Verwaltung").remove();
            }

            if (DiensteManager.Config.getUseWebsockets()) {
                window.socket.on("update_service_list", function () {
                    Ext.getStore("ServiceList").reload();
                });
            }

            if (window.read_only) {
                Ext.getCmp("service_split_button").setDisabled(true);
                if (DiensteManager.Config.getVerwaltungModul()) {
                    Ext.getStore("NavigationTree").findNode("text", "Verwaltung").remove();
                }
                Ext.getStore("NavigationTree").findNode("text", "Kontakte").remove();
                Ext.getCmp("restore_deleted_service_button").setDisabled(true);
                Ext.getCmp("final_delete_service_button").setDisabled(true);
            }

            Ext.getCmp("dmVersionField").setHtml("v" + DiensteManager.Config.getVersion());
        }, this, {single: true});
    },

    prepareConfig: function () {
        DiensteManager.Config.setVersion(Ext.getStore("Config").getAt(0).getData().config["version"]);
        DiensteManager.Config.setMapPreview(Ext.getStore("Config").getAt(0).getData().config["map_preview"]);
        DiensteManager.Config.setMapPreviewBaseLayerId(Ext.getStore("Config").getAt(0).getData().config["map_preview_base_layer_id"]);
        DiensteManager.Config.setMapPreviewComplexAppend(Ext.getStore("Config").getAt(0).getData().config["map_preview_complex_append"]);
        DiensteManager.Config.setGeoportalUrl(Ext.getStore("Config").getAt(0).getData().config["geoportal_url"]);
        DiensteManager.Config.setGeoportalUrlExt(Ext.getStore("Config").getAt(0).getData().config["geoportal_url_ext"]);
        DiensteManager.Config.setBearbeiter(Ext.getStore("Config").getAt(0).getData().config["bearbeiter"]);
        DiensteManager.Config.setResponsibleParty(Ext.getStore("Config").getAt(0).getData().config["responsible_party"]);
        DiensteManager.Config.setSoftware(Ext.getStore("Config").getAt(0).getData().config["software"]);
        DiensteManager.Config.setServer(Ext.getStore("Config").getAt(0).getData().config["server"]);
        DiensteManager.Config.setSrsDienste(Ext.getStore("Config").getAt(0).getData().config["srs_dienste"]);
        DiensteManager.Config.setEsriUrlNamespace(Ext.getStore("Config").getAt(0).getData().config["esri_url_namespace"]);
        DiensteManager.Config.setExterneUrlPrefix(Ext.getStore("Config").getAt(0).getData().config["externe_url_prefix"]);
        DiensteManager.Config.setSecurityType(Ext.getStore("Config").getAt(0).getData().config["security_type"]);
        DiensteManager.Config.setSecurityProxyUrlSso(Ext.getStore("Config").getAt(0).getData().config["security_proxy_url_sso"]);
        DiensteManager.Config.setSecurityProxyUrlAuth(Ext.getStore("Config").getAt(0).getData().config["security_proxy_url_auth"]);
        DiensteManager.Config.setJiraProjects(Ext.getStore("Config").getAt(0).getData().config["jira_projects"]);
        if (Ext.getStore("Config").getAt(0).getData().config["use_websockets"]) {
            DiensteManager.Config.setUseWebsockets(Ext.getStore("Config").getAt(0).getData().config["use_websockets"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["apply_dpi_factor"]) {
            DiensteManager.Config.setApplyDpiFactor(Ext.getStore("Config").getAt(0).getData().config["apply_dpi_factor"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["endpointer"]) {
            DiensteManager.Config.setEndpointer(Ext.getStore("Config").getAt(0).getData().config["endpointer"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["ldap_query_url"]) {
            DiensteManager.Config.setLdapQueryUrl(Ext.getStore("Config").getAt(0).getData().config["ldap_query_url"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["fme_json_modul"]) {
            DiensteManager.Config.setFmeJsonModul(Ext.getStore("Config").getAt(0).getData().config["fme_json_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["freigaben_modul"]) {
            DiensteManager.Config.setFreigabenModul(Ext.getStore("Config").getAt(0).getData().config["freigaben_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["metadaten_modul"]) {
            DiensteManager.Config.setMetadatenModul(Ext.getStore("Config").getAt(0).getData().config["metadaten_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["gekoppelteportale_modul"]) {
            DiensteManager.Config.setGekoppelteportaleModul(Ext.getStore("Config").getAt(0).getData().config["gekoppelteportale_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["proxyupdate_modul"]) {
            DiensteManager.Config.setProxyupdateModul(Ext.getStore("Config").getAt(0).getData().config["proxyupdate_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["deegree_modul"]) {
            DiensteManager.Config.setDeegreeModul(Ext.getStore("Config").getAt(0).getData().config["deegree_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["anmerkungen_modul"]) {
            DiensteManager.Config.setAnmerkungenModul(Ext.getStore("Config").getAt(0).getData().config["anmerkungen_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["verwaltung_modul"]) {
            DiensteManager.Config.setVerwaltungModul(Ext.getStore("Config").getAt(0).getData().config["verwaltung_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["zugriffsstatistik_modul"]) {
            DiensteManager.Config.setZugriffsstatistikModul(Ext.getStore("Config").getAt(0).getData().config["zugriffsstatistik_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["ldap_modul"]) {
            DiensteManager.Config.setLdapModul(Ext.getStore("Config").getAt(0).getData().config["ldap_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["security_modul"]) {
            DiensteManager.Config.setSecurityModul(Ext.getStore("Config").getAt(0).getData().config["security_modul"]);
        }
        if (Ext.getStore("Config").getAt(0).getData().config["jira_modul"]) {
            DiensteManager.Config.setJiraModul(Ext.getStore("Config").getAt(0).getData().config["jira_modul"]);
            DiensteManager.Config.setJiraBrowseUrl(Ext.getStore("Config").getAt(0).getData().config["jira_browse_url"]);
            DiensteManager.Config.setJiraProjects(Ext.getStore("Config").getAt(0).getData().config["jira_projects"]);
            DiensteManager.Config.setJiraLabels(Ext.getStore("Config").getAt(0).getData().config["jira_labels"]);
        }

        var bbox_getmap = Ext.getStore("Config").getAt(0).getData().config["bbox_getmap"],
            bboxGetmap_name = [],
            bboxGetmap_extent = {};

        for (var i = 0; i < bbox_getmap.length; i++) {
            bboxGetmap_name.push(bbox_getmap[i].name);
            bboxGetmap_extent[bbox_getmap[i].name] = bbox_getmap[i].bbox;
        }

        DiensteManager.Config.setBboxGetmap_name(bboxGetmap_name);
        DiensteManager.Config.setBboxGetmap_extent(bboxGetmap_extent);

        var dienst_typen = Ext.getStore("Config").getAt(0).getData().config["dienst_typen"],
            dienstTypen = [],
            dienstVersionen = {};

        for (var i = 0; i < dienst_typen.length; i++) {
            dienstTypen.push(dienst_typen[i].name);
            dienstVersionen[dienst_typen[i].name] = dienst_typen[i].versionen;
        }

        DiensteManager.Config.setDienstTypen(dienstTypen);
        DiensteManager.Config.setDienstVersionen(dienstVersionen);

        if (window.auth_user === "" || window.auth_user === undefined || window.auth_user === null) {
            var chooseeditorwindow = Ext.getCmp("chooseeditor-window");

            if (chooseeditorwindow) {
                chooseeditorwindow.show();
            }
            else {
                Ext.create("editor.ChooseEditor").show();
            }
        }
    }

});

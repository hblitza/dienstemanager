Ext.define("DiensteManager.model.LayerLink", {
    extend: "Ext.data.Model",
    fields: [
        {name: "layer_links_id", type: "int"},
        {name: "layer_id", type: "int"},
        {name: "md_id", type: "string"},
        {name: "service_id", type: "int"},
        {name: "dataset_name", type: "string"},
        {name: "show_doc_url", type: "string"}
    ]
});

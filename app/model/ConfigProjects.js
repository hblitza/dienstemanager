Ext.define("DiensteManager.model.ConfigProjects", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "name_alias", type: "string"},
        {name: "new_as_subtask", type: "boolean"},
        {name: "use_in_ticketsearch", type: "boolean"},
        {name: "epic_link_customfield_id", type: "string"},
        {name: "epic_link_key", type: "string"}
    ],
    belongsTo: "Config"
});

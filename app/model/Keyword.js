Ext.define("DiensteManager.model.Keyword", {
    extend: "Ext.data.Model",
    fields: [
        {name: "id", type: "integer"},
        {name: "service_id", type: "integer"},
        {name: "text", type: "string"}
    ]
});

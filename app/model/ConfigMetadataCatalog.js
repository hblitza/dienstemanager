Ext.define("DiensteManager.model.ConfigMetadataCatalog", {
    extend: "Ext.data.Model",
    fields: [
        {name: "config_metadata_catalog_id", type: "integer"},
        {name: "name", type: "string"},
        {name: "csw_url", type: "string"},
        {name: "show_doc_url", type: "string"},
        {name: "srs_metadata", type: "integer"},
        {name: "proxy", type: "boolean"}
    ],
    belongsTo: "Config"
});

Ext.define("DiensteManager.model.Statistic", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "count", type: "integer"}
    ]
});

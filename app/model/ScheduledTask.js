Ext.define("DiensteManager.model.ScheduledTask", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "descr", type: "string"},
        {name: "last_run", type: "string"},
        {name: "status", type: "string"},
        {name: "message", type: "string"},
        {name: "schedule", type: "string"},
        {name: "enabled", type: "boolean"}
    ]
});

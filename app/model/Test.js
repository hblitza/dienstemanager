Ext.define("DiensteManager.model.Test", {
    extend: "Ext.data.Model",
    fields: [
        {name: "title", type: "string"},
        {name: "status", type: "string"},
        {name: "gml", type: "string"}
    ]
});

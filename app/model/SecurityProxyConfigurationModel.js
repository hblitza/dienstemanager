Ext.define("SecurityProxyConfigurationModel", {
    extend: "Ext.data.Model",
    fields: [
        {name: "endpoint_name", type: "string"},
        {name: "proxy_url", type: "string"},
        {name: "url_int", type: "string"},
        {name: "endpoint_id", type: "int", convert: null},
        {name: "object_id", type: "int", convert: null},
        {name: "grant_right", type: "string"},
        {name: "objects"},
        {name: "users", type: "auto"}
    ]
});

Ext.define("DiensteManager.model.Dataset", {
    extend: "Ext.data.Model",
    fields: [
        {name: "dataset_name", type: "string"},
        {name: "dataset_shortname", type: "string"},
        {name: "description", type: "string"},
        {name: "keywords", type: "string"},
        {name: "md_id", type: "string"},
        {name: "rs_id", type: "string"},
        {name: "cat_opendata", type: "string"},
        {name: "cat_inspire", type: "string"},
        {name: "cat_hmbtg", type: "string"},
        {name: "cat_org", type: "string"},
        {name: "bbox", type: "string"},
        {name: "fees", type: "string"},
        {name: "access_constraints", type: "string"},
        {name: "source_csw", type: "string"},
        {name: "show_doc_url", type: "string"},
        {name: "config_metadata_catalog_id", type: "integer"}
    ]
});

Ext.define("DiensteManager.model.LayerList", {
    extend: "Ext.data.Model",
    fields: [
        {name: "layer_id", type: "int"},
        {name: "name", type: "string"},
        {name: "title", type: "string"},
        {name: "service_id", type: "int"},
        {name: "service_title", type: "string"},
        {name: "service_type", type: "string"}
    ]
});

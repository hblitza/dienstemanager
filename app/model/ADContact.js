Ext.define("DiensteManager.model.ADContact", {
    extend: "Ext.data.Model",
    fields: [
        {name: "givenName", type: "string"},
        {name: "sn", type: "string"},
        {name: "company", type: "string"},
        {name: "sAMAccountName", type: "string"},
        {name: "mail", type: "string"},
        {name: "telephoneNumber", type: "string"}
    ]
});

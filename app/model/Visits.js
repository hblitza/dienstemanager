Ext.define("DiensteManager.model.Visits", {
    extend: "Ext.data.Model",
    fields: [
        {name: "service_id", type: "integer"},
        {name: "title", type: "string"},
        {name: "name_ext", type: "string"},
        {name: "visits_intranet", type: "integer"},
        {name: "visits_internet", type: "integer"},
        {name: "visits_total", type: "integer"},
        {name: "month", type: "string"}
    ]
});

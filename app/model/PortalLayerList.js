Ext.define("DiensteManager.model.PortalLayerList", {
    extend: "Ext.data.Model",
    fields: [
        {name: "layer_id", type: "int"},
        {name: "layer_name", type: "string"},
        {name: "title", type: "string"},
        {name: "service_id", type: "int"}
    ]
});

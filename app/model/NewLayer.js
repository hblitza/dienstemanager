Ext.define("DiensteManager.model.NewLayer", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "title", type: "string"},
        {name: "service_id", type: "int"},
        {name: "namespace", type: "string"},
        {name: "queryable", type: "boolean"},
        {name: "scale_min", type: "string"},
        {name: "scale_max", type: "string"}
    ]
});

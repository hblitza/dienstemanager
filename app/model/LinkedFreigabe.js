Ext.define("DiensteManager.model.LinkedFreigabe", {
    extend: "Ext.data.Model",
    fields: [
        {name: "freigabe_id", type: "integer"},
        {name: "datenbezeichnung", type: "string"},
        {name: "link", type: "string"},
        {name: "freigaben_link_id", type: "integer"}
    ]
});

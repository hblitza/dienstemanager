# Changelog LGV Dienstemanager 4.8.4

NEU:
    

UPDATE:
    

FIX:
    - Metadaten wurden nicht hinzugefügt, wenn das log_level in der config auf debug eingestellt war
    - Wenn kein Namensraum für WFS Layer in den Capabilities vorhanden ist, wird der Namensraum nun auf einen leeren String gesetzt

# Changelog LGV Dienstemanager 4.8.3

NEU:
    - Für die GFI Attribute Konfiguration kann ausgewählt werden, ob Anfangsbuchstaben und Sonderzeichen automatisch angepasst werden sollen, oder wie in der Konfiguration
        angegeben, behandelt werden sollen

UPDATE:
    - JIRA Tickets anlegen:
        - Wenn das Ticket erfolgreich angelegt wurde, schließt sich das Fenster zur Ticketerstellung automatisch
        - Wird die Checkbox "Database" bei Dienst aktualisieren ausgewählt, erscheint im Anmerkungstextfeld ein Hinweis, was in Bezug auf die Datenbank kopiert werden soll
        - Das Feld "Komponenten" kann jetzt über die config.js gefüllt werden
        - Bei der Ticketverknüpfung werden nur noch diejenigen Tickets angezeigt, je nachdem welches Projekt ausgewählt wurde
    - HMDK URL aufgrund der HMDK Migration angepasst
    - Dienstprüfung erweitert um Prüfung, ob Feld 'Nutzungsbedingungen' gefüllt ist

FIX:
    - Eigener Parameter in der config, der die Mailadresse für die Rückmeldung bei einem Löschvermerk beinhaltet
    - Dienstprüfung auf verknüpfte Jira Tickets - Übergabe der service_id  für Datenbankabfrage war fehlerhaft
    - bei nicht eingesetztem Security Modul, konnten die Dienstdetails nicht mehr aufgerufen werden
    - beim Regenerieren der Layer JSON Objekte werden nicht mehr initial alle Objekte gelöscht
    


# Changelog LGV Dienstemanager 4.8.2

NEU:
    - NotSupportedFor3D Checkbox für WMS Layer hinzugefügt
    - es kann konfiguriert werden, unter welchem Host die Anwendung erreichbar ist.

UPDATE:
    - Die URL für das Vorschauportal wurde auf die aktuelle Masterportal Version angepasst
    - GFI Theme Parameter können für alle Diensttypen eingegegen werden
    - JIRA Tickets anlegen:
        - Ticket kann nach dem Anlegen automatisch in beliebige Board-Spalte verschoben werden
        - Wird ein Ticket als Sub-Taks angelegt, wird automatisch die Priorität des dazugehörigen Haupttickets übernommen
        - Die Fehlermeldung "mindestens eine Checkbox markieren" wurde entfernt - bei Dienst aktualisieren, Komponenten zu aktualisieren
        - Ansicht der zu aktualisierenden Komponenten in der Ticketbeschreibung wurde verändert
        - Beim Anlegen von JIRA Tickets wird automatisch ein Assignee (User in config.js definierbar) zugewiesen
        - Beim Anlegen von JIRA Tickets wird das Feld Storypoints (konfigurierbar) mit befüllt
        - für JIRA Projekte kann ein Alias mit angegeben werden, dieser taucht in GUI bei der Projektauswahl auf
        - Bei der Erstellung von JIRA Tickets wurde die Auswahlliste für Komponenten, die aktualisiert werden sollen, erweitert

FIX:
    - Bei externen Dienste wurden interne Testaufrufe angeboten
    - Bei einem Fehler während des Dienstimports wurde das Import-Fenster nicht wieder demaskiert.
    - unangehakte NotSupportedFor3D Checkbox gibt jetzt FALSE anstatt NULL zurück.


# Changelog LGV Dienstemanager 4.8.1

NEU:
    -

UPDATE:
    - Datetime Spalte wurde der layers_type_ows Tabelle hinzugefügt und ein dazugehöriges Feld wurde den Layereigenschaften hinzugefügt

FIX:
    - Als JSON Objekte konfigurierte GFI Attribute werden auch beim Speichern der Layereigenschaften als JSON Objekte erkannt
    - Beim Import eines Dienstes wird die layer_links_id nicht mehr übergeben, sondern immer neu vergeben
    - Beim verändern des Feldes Ordner/Webapp wird bei gesetztem BasicAuth Schutz nun auch die abgesicherte URL automatisch angepasst und korrekt in den Secure Admin übertragen
    - Wenn im Secure Admin keine Objekte (Layer/Features) für eine Rolle freigegeben sind, erscheint nun eine Fehlermeldung, die darauf hinweist und das Konfigurationsfenster ist weiterhin aufrufbar
    - Wenn WFS-T als Berechtigungstyp bei der BasicAuth Konfiguration gewählt wird, wird automatisch im Secure Admin ein Objekt "FeatureCollection" mit GML 3.1 und 3.2 Namespaces eingetragen und dem jeweiligen Endpunkt zugeordnet
    - init_db SQL Scripte enthielten noch einen veralteten Verweis auf eine nicht mehr existierende Tabelle "portals"


# Changelog LGV Dienstemanager 4.8.0

NEU:
    - Jira Tickets Tab in Dienstdetails wurde angepasst:
        - Neuer Button/Fenster zum automatisierten anlegen von Jira Tickets
        - Das neu angelegte Ticket wird in der Tabelle angezeigt
        - Die Tabelle wurde um die Spalte Projekt erweitert und die Zusammenfassung des Tickets wird angezeigt

UPDATE:
    - Datensatzliste wurde um die Spalte Kurzname ergänzt, diese Splate ist durchsuchbar
    - genau URL Prüfung im Diensteprüfprozess konfigurierbar
    - im Fenster GFI Attribute Konfigurieren können auch JSON Objekte eingefügt werden

FIX:
    - Fehler beim Prüfen von WFS: Capabilities wurden nicht richtig geparset.
    - nicht alle Bearbeitungsfunktionen waren bei read_only deaktiviert
    - Fehler beim Suchen nach AD Gruppen für die Dienstabsicherung


# Changelog LGV Dienstemanager 4.7.0

NEU:
    - Buttons zum neu generieren der layer JSON Objekte
    - Neues Feld für Verantwortliche Stelle in Detailansicht und Dienstliste
    - Neuer Tab in Navigationsleiste für Portale mit Übersicht zu allen Portalen und den enthaltenen Layern
    - VectorTiles als neuer Diensttyp wurde integriert

UPDATE:
    - Basic Auth Passwörter werden maskiert dargestellt
    - mouseHoverField Attribut für SensorThings Layer hinzugefügt
    - Prüfung von Diensten auch bei Nichtvorhandensein von Metadatenkopplung ausführbar, zusätzliche Tests implementiert
    - für externe Dienstemanager Instanzen können die Login-Daten mit gespeichert werden

FIX:
    - Attribute für GFI Konfiguration werden aus komplexen Schemas nicht ausgelesen
    - Prüfen, ob tmp Pfad existiert fehelerhaft
    - Keywords werden nicht komplett aus Metadaten ausegelesen


# Changelog LGV Dienstemanager 4.6.0

NEU:
    - Neue Parameter root, related_wms_layers, styleId, clusterDistance und loadThingsOnlyInCurrentExtent für SensorThings Layer
    - Aufruf der SensorThings API für i-Knopf
    - Aufruf Vorschauportal für SensorThings Layer
    - Es kann jetzt auch nach service-IDs gesucht werden

UPDATE:
    - SSPI Node Modul entfernt
    - Node Module aktualisiert
    - Inhalt der Versionsanzeige im Client wird aus process.env.npm_package_version gelesen

FIX:
    - Fehler beim Import von Diensten ohne Layer
    - Fehler beim Import von Diensten aus verschiedenen Quellen
    - Korrektes setzen der layer / service ID Sequenzen nach Import
    - Auslesen von Layer IDs aus komplexen Masterportal Konfigurationen
    - Suchen nach JIRA Tickets über alle Tickets
    - Layer Auswahlfenster öffnet sich nicht bei bestimmten Konstellationen

# Changelog LGV Dienstemanager 4.5.1

NEU:
    - Neuer Parameter in den Layer Details, um GFI in einem neuen Fenster zu öffnen.
    - Neuer Parameter in den Layer Details, um die Service-Adresse in den Layerinformation im Masterportal auszublenden.

UPDATE:
    - verbessertes Error Handling bei der Dienstmetadatenkopplung

FIX:
    - Speichern Button nicht aktiv nach ändern von GFI deaktivieren.
    - Speichern Button nicht aktiv nach Eintragen einer Dienstemetadatenkopplung

# Changelog LGV Dienstemanager 4.5.0

NEU:
    - Vor dem Löschen eines Layers kann ein Löschvermerk gesetzt werden, damit Portale in denen der Layer enthalten ist vorher angepasst werden können.
    - In den Dienstdetails im Tab "Jira Tickets" können dem Dienst über eine Suchfunktion Jira Tickets zugewiesen werden.
    - Es können Diensteinträge aus einer anderen Dienstemanager-Instanz importiert werden.
    - Konfiguration und Statusübersicht der Scheduled Tasks in der Benutzeroberfläche. HINWEIS: die bestehende Konfiguration aus der config.js muss hier manuell neu eingegeben werden!
    - Über einen Scheduled Task können WebDAV Ressourcen auf Masterportal Instanzen geprüft und verlinkte LayerIDs ausgelesen werden. Diese Verknüpfung kann dann in den Layerdetails eingesehen werden.

UPDATE:
    - Prüfung, ob die angefragte Dienst-Version zum Wert in der Antwort passt.
    - Speicherbutton in Dienst- und Layerdetails erst nach Änderung aktiv
    - statische Bibliotheken wurden entfernt

FIX:
    - Errorhandling bei der Layerabfrage und http 404 Antworten

# Changelog LGV Dienstemanager 4.4.0

NEU:
    - Integration von Elasticsearch
    - Scheduled Task zum neu-Generieren aller Layer JSON Objekte
    - Metadaten auf Layerebene können auch manuell, ohne CSW Abruf eingegeben werden.

UPDATE:
    - AccessConstraints aus Dienstmetadaten werden nun auch aus dem Element //gmd:otherConstraints/gco:CharacterString ausgelesen.
    - Anlegen von mehreren Benutzern in der Basic Auth Konfiguration

FIX:
    - Problem bei der Abfrage externer Dienste via http
    - manuelle Auswahl von Bearbeitern funktioniert nicht

# Changelog LGV Dienstemanager 4.3.5

NEU:

UPDATE:
    - Umorganisation der Requestgenerierung mit HTTPS bei Verwendung eines Proxies
    - Hinzufügen von Test auf eindeutige Layernamen

FIX:
    - Zeilenumbrüche bei ESRI INSPIRE Capabilities entfernt
    - Bei mehreren Keywords zu einer Kategorie wurde nur das erste ausgelesen
    - Beim endgültigen Löschen eines Dienstes werden jetzt auch die Schlagworte gelöscht
    - Beim Layerupdate wurden in der Zuordnungsansicht nicht die Einträge aus der rechten Liste nach update entfernt
    - Anlegen von 3D-Layern nicht möglich
    - Benachrichtigung, wenn keine Layer für das Vorschauportal vorhanden sind
    - service_checker meldet Fehler bei Gruppenlayern
    - Test bei WMS 1.1.1 schlägt fehl
    - Namespace wird beim aktualisieren bestehder Layer nicht mit aktualisiert

# Changelog LGV Dienstemanager 4.3.4

NEU:
    - API Abruf aller AD User zu einer AD Gruppe
    - Die Layerliste kann mit den Pfeiltasten durchnavigiert werden
    - Konfigrationsmöglichkeit die SSL Validierung bei Requests zu deaktivieren

UPDATE:
    - neue ESLint Regeln

FIX:
    - Asynchrones nachladen der Layerliste nach speichern
    - Eintrag einer 0 in den WFS Namespace nach dem Speichern der Layerdetails
    - Überarbeitung der Basic Auth Konfiguration

# Changelog LGV Dienstemanager 4.3.3

NEU:

UPDATE:
    - WFS Namespace wird in den Layerdetails angezeigt
    - verbessertes Error-Handling beim Layeraktualisieren
    - Debug Modus hinzugefügt

FIX:
    - Fehler, dass sehr viele CRS:84 EPSG Codes in den Prüfergenissen auftauchen wurde behoben.
    - Aktualisierungen der Dienstmetadaten wurde nicht in die Datenbank übertragen
    - WFS Berechtigungen für security-proxy wurden unvollständig eingetragen
    - In die Internet JSON wurde die Legenden URL nicht übernommen
    - Gekoppelte Datensätze tauchen nicht komplett in der JSON Aktualisierung der Metadaten auf

# Changelog LGV Dienstemanager 4.3.2

NEU:

UPDATE:
    - SensorThings API Layerparameter angepasst

FIX:
    - System erzeugt falsche Requests, wenn GET Parameter in internen URLs enthalten sind
    - Auslesen der Metadateninformationen überarbeitet
    - Fehler, wenn security_modul deaktiviert ist

# Changelog LGV Dienstemanager 4.3.1

NEU:

UPDATE:
    - SensorThings API Layerparameter angepasst

FIX:
    - Fehler in der Dienstemanager API bei bestimmten Parameterzusammenstellungen
    - Layer-Testrequests werden dynamisch erzeugt und nicht mehr aus der Datenbank gelesen
    - Absturz, wenn der tmp Ordner nicht vorhanden ist
    - Eintrag von Diensten mit nicht in der config definiertem Typ nicht möglich
    - Fehler bei Eintrag von über dEE security-proxy abgesicherten WFS

# Changelog LGV Dienstemanager 4.3.0

NEU:
    - Aktualisierung aller deegree Capabilities über scheduled Task
    - Konfiguration der Basic Auth mit dem dEE security-proxy
    - neues Logo
    - Auswahl mehrerer Bounding-Boxes für WMS Testrequests

UPDATE

FIX
    - Intranet und Internet Layer JSON sind jetzt korrekt befüllt

# Changelog LGV Dienstemanager 4.2.1

NEU:
    - Die Prozesse "testen aller Dienste" und "Metadaten aktualisieren" können in der Oberfläche über Verwalten angestoßen werden

UPDATE:
    - Bei Erstellen von Kommentare kann jetzt Formatierung verwendet werden.

FIX:
    - Filter Funktionalität wieder hergestellt
    - Fehler bei readonly, wenn kein AD benutzt wird
    - Filtern in der API

# Changelog LGV Dienstemanager 4.2.0

NEU:

    - Beim WMS/WFS Layer Import können auch Gruppenlayer ausgewählt werden.
    - Über einen Scheduled Task können Datensatz und Dienstmetadaten regelmäßig aus dem Metadatenkatalog aktualisiert werden.
    - Über einen Scheduled Task können alle WMS und WFS Dienste getestet werden.
    - Es können mehrere Bounding-Boxes für WMS Testrequests definiert werden.

UPDATE

    - Datenbankstruktur angepasst.
    - JQuery Abhängigkeiten entfernt.
    - GetCapabilities Aufruf aus der Diensteliste auch für WPS.
    - GFI Attributabruf für alle Softwaretypen erlaubt.

FIX

    - Löschen von Diensten funktionierte nicht.
    - Wiederherstellen von gelöschten Diensten funktionierte nicht.
    - Wenn das extern Attribut gesetzt und direkt wieder entfernt wird, werden die Feldinhalte wieder hergestellt.

# Changelog LGV Dienstemanager 4.1.0

NEU:

    - Metadatenkataloge können in der Oberfläche gepflegt werden
    - Für Dienste können Tests (abgleich Dienstemanager, Capabilities, Metadaten) durchgeführt werden
    - Dienste können in der Detailansicht als externer Dienst markiert werden
    - Layer können einzeln mit Datensatzmetadaten gekoppelt werden

UPDATE

    - INSPIRE Capabilities können auch generiert werden, wenn nicht alle Layer gekoppelt sind.
    - Integration der csw_url und show_doc_url in layer JSON
    - Kontextabhängige Buttons in der Dienstdetails Layeransicht
    - Erweiterte Statistikansicht für intern und extern eingebundene Dienste

FIXES

    - JQuery für Bilder Download integriert
    - Datum und Bearbeiter bei Anpassung von Layerkonfiguration direkt in Dienstdetails sichtbar
    - Alle AccessConstraints und Fees Elemente aus den Metadaten werden kommasepariert ausgelesen
    - Element md_name fehlt in generiertem Layer JSON Objekt
    - Beim neu generieren aller Layer JSONs wurde die Tabelle nicht korrekt bereinigt

# Changelog LGV Dienstemanager 4.0.3

FIXES

    - Fehler im deegree WFS Metadata Template im ExtendedCapabilities Element
    - Fehler bei der Freigabenzuordnung, anschließend konnte nicht mehr in den anderen Listen gesucht werden


# Changelog LGV Dienstemanager 4.0.2

NEU:

    - Erlaubte AD-Usergroups werden in der config.js konfiguriert
    - Für Erlaubte Benutzergruppen können die Rechte auf nur lesen eingeschränkt werden
    - Die Proxy-Konfiguration kann für produktiv und Entwicklungsumgebung getrennt konfiguriert werden
    - Zur AD Absicherung von Dienstes kann ein angebundenes AD durchsucht und Gruppen pro Dienst berechtigt werden

UPDATE

    - Beim GFI Attribute Zuordnen kann die vorgenommene Konfiguration unabhängig von den Layerdetails abgespeichert werden

FIXES

    - Dienstmetadaten wurden bei mehreren Treffern in der CSW nicht alle in der Auswahl zum Dienst koppeln angeboten
    - Fehler bei Abfrage von Ressouren über HTTPS
    - Fehler beim Export der Datensatzliste
    - Attributabfrage bei WFS schlägt fehl


# Changelog LGV Dienstemanager 4.0.1

NEU:

    - Unterstützung für Die Diensttypen Terrain3D, TileSet3D und Oblique. Wird für die 3D Funktionalität des Master-Portal benötigt.

FIXES

    - Beim Kopieren eines Dienstes ist der neue Titel undefined
    - GML Zips werden nicht komprimiert

# Changelog LGV Dienstemanager 4.0.0

NEU:

    - Beim Erzeugen der INSPIRE Capabilities für deegree Dienste werden nun auch die Keywords berücksichtigt.
    - Es können nun Abgesicherte URLs definiert werden.
    - Im CSV Download der Datensatzliste sind jetzt Links auf Metadatenkatalog und Geoportal enthalten

FIXES

    - CSV Download in UTF-8

# Changelog LGV Dienstemanager 3.6.7

NEU:

    - GFI-Konfiguration kann nun beim Speichern auf alle Layer eines Dienstes übertragen werden.
    - Elasticsearch Dienste können nun erfasst werden. In der Layerliste können manuell typeNames definiert werden.

FIXES

    - Wenn das GFI disabled wird (ignore), werden jetzt die entsprechenden Buttons korrekt enabled / disabled.

# Changelog LGV Dienstemanager 3.6.6

NEU:

    - WFS Downloader unterstützt nun auch WFS 2.0.0
    - Downloadpaket enthält das Master-Portal 1.5.0

FIXES:

    - Ist bereits eine externe URL vergeben wird diese nicht durch Änderungen anderer Parameter überschrieben.
    - Scales in den Capabilities werden bei Bedarf von Exponentialschreibweise in Fliesskomma umgewandelt.


# Changelog LGV Dienstemanager 3.6.5

NEU:

    - Deegree Dienste können aus der Diensteliste heraus im Endpointer geöffnet werden.
    - Min- und MaxScaleDenominator können nun beim Auslesen aus den Capabilties direkt auf 96DPI umgerechnet werden. In config.json aktivierbar.

FIXES:

    - Hintergrund von gespeicherten Grafiken ist jetzt komplett weiß
    - Prozess beim koppeln aller Layer eines Dienstes optimiert
    - NodeJS Backend
    - Beschreibung, Nutzungsbedingungen und Zugriffsbeschränkungen werden aus dem Metadatenkatalog ausgelesen
    - Es können mehere parallele CSW angebunden werden
    - Layer JSONs können in einen Elastic Search Index geschrieben werden
    - Dienste Changelog
    - Unterstützung 3D Layer (Caesium)
    - Unterstützung von Sensor-Layer (SensorThings)
    - Prüfergebnisse können bestätigt werden

FIXES

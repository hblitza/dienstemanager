ALTER TABLE public.layers_type_sensorthings
    ALTER COLUMN cluster_distance SET DEFAULT NULL;

ALTER TABLE public.layers_type_sensorthings
    ADD COLUMN mouse_hover_field text;

ALTER TABLE public.layers_type_sensorthings
    ADD COLUMN sta_test_url text;

CREATE TABLE public.layers_type_vectortiles
(
    layer_id integer NOT NULL,
    extent text COLLATE pg_catalog."default",
    origin text COLLATE pg_catalog."default",
    resolutions text COLLATE pg_catalog."default",
    visibility boolean DEFAULT false,
    vtstyles text COLLATE pg_catalog."default",
    CONSTRAINT layers_type_vectortiles_pkey PRIMARY KEY (layer_id),
    CONSTRAINT layer_id_fkey FOREIGN KEY (layer_id)
        REFERENCES public.layers (layer_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_vectortiles
    OWNER to dienstemanager;

COMMENT ON TABLE public.layers_type_vectortiles
    IS 'Tabelle für alle Vector Tile Typen';

DROP VIEW public.service_layers;
CREATE OR REPLACE VIEW public.service_layers
    AS
     SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    layers.gfi_asnewwindow,
    layers.gfi_windowspecs,
	layers.service_url_is_visible,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.rootel,
    sensorthings.filter,
    sensorthings.expand,
    sensorthings.related_wms_layers,
    sensorthings.style_id,
    sensorthings.cluster_distance,
    sensorthings.load_things_only_in_current_extent,
    sensorthings.mouse_hover_field,
    sensorthings.sta_test_url,
    vectortiles.extent,
    vectortiles.origin,
    vectortiles.resolutions,
    vectortiles.visibility,
    vectortiles.vtstyles,
    (EXISTS ( SELECT layer_delete_requests_1.comment
           FROM layer_delete_requests layer_delete_requests_1
          WHERE (layer_delete_requests_1.layer_id = layers.layer_id))) AS layer_delete_request,
    layer_delete_requests.comment AS layer_delete_request_comment
   FROM ((((((layers
     LEFT JOIN layers_type_ows ows ON ((layers.layer_id = ows.layer_id)))
     LEFT JOIN layers_type_3d type3d ON ((layers.layer_id = type3d.layer_id)))
     LEFT JOIN layers_type_oblique oblique ON ((layers.layer_id = oblique.layer_id)))
     LEFT JOIN layers_type_sensorthings sensorthings ON ((layers.layer_id = sensorthings.layer_id)))
     LEFT JOIN layers_type_vectortiles vectortiles ON ((layers.layer_id = vectortiles.layer_id)))
     LEFT JOIN layer_delete_requests layer_delete_requests ON ((layers.layer_id = layer_delete_requests.layer_id)))
  WHERE (layers.deleted = false);

ALTER TABLE public.service_layers OWNER TO dienstemanager;


ALTER TABLE public.services
    ADD COLUMN responsible_party text;

DROP VIEW public.service_list;
CREATE OR REPLACE VIEW public.service_list
 AS
 SELECT b.service_id,
    b.service_checked_status,
    to_char(b.last_edit_date, 'YYYY-MM-DD HH24:MI'::text) AS last_edit_date,
    b.service_name,
    b.title,
    b.fees,
    b.access_constraints,
    b.description,
    b.editor,
    b.responsible_party,
    b.status,
    b.service_type,
    b.software,
    b.server,
    b.folder,
    b.url_ext,
    b.url_int,
    b.url_sec,
    b.security_type,
    b.allowed_groups::text AS allowed_groups,
    b.only_intranet,
    b.only_internet,
    b.datasource,
    b.mapping_description,
    b.cached,
    b.service_md_id,
    ( SELECT a.csw_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS source_csw,
    ( SELECT a.show_doc_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS show_doc_url,
    ( SELECT a.name
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS csw_name,
    b.dsc_complete,
    b.inspire_relevant,
    b.last_edited_by,
    b.version,
    b.qa,
    to_char(b.update_date::timestamp with time zone, 'YYYY-MM-DD'::text) AS update_date,
    b.update_unit,
    b.update_type,
    b.update_process_type,
    b.update_process_name,
    b.update_process_location,
    b.update_process_description,
    b.update_number,
    b.external
   FROM services b
  WHERE b.deleted = false;

ALTER TABLE public.service_list
    OWNER TO dienstemanager;

ALTER TABLE public.config_dm_instances
    ADD COLUMN pw text;

ALTER TABLE public.config_dm_instances
    ADD COLUMN db_user text;

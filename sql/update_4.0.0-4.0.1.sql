ALTER TABLE layers ADD COLUMN hittolerance integer;
ALTER TABLE deleted_layers ADD COLUMN hittolerance integer;

DROP VIEW service_layers;

CREATE VIEW service_layers AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.gutter,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.singletile,
    layers.transparent,
    layers.transparency,
    layers.isbaselayer,
    layers.namespace,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.legend_type,
    layers.legend_path,
    layers.featurecount,
    layers.gfi_format,
    layers.gfi_theme,
    layers.gfi_config,
    layers.gfi_complex,
    layers.request_vertex_normals,
    layers.maximum_screen_space_error,
    layers.epsg,
    layers.gfi_theme_params,
    layers.resolution,
    layers.hidelevels,
    layers.minzoom,
    layers.terrainurl,
    layers.projection,
    layers.url_params,
    layers.hittolerance,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_int
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_int
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_int,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_ext
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_ext
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_ext
   FROM layers;


ALTER TABLE service_layers OWNER TO dienstemanager;

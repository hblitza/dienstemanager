ALTER TABLE services ADD COLUMN allowed_groups json;

ALTER TABLE deleted_services ADD COLUMN allowed_groups json;

DROP VIEW service_list;

CREATE OR REPLACE VIEW service_list AS 
 SELECT services.service_id,
    services.service_checked_status,
    to_char(services.last_edit_date, 'YYYY-MM-DD HH24:MI'::text) AS last_edit_date,
    services.service_name,
    services.title,
    services.fees,
    services.access_constraints,
    services.description,
    services.editor,
    services.status,
    services.service_type,
    services.software,
    services.server,
    services.folder,
    services.url_ext,
    services.url_int,
    services.url_sec,
    services.security_type,
    services.allowed_groups::text AS allowed_groups,
    services.only_intranet,
    services.only_internet,
    services.datasource,
    services.mapping_description,
    services.cached,
    services.service_md_id,
    services.source_csw,
    services.show_doc_url,
    services.dsc_complete,
    services.inspire_relevant,
    services.last_edited_by,
    services.version,
    services.qa,
    to_char(services.update_date::timestamp with time zone, 'YYYY-MM-DD'::text) AS update_date,
    services.update_unit,
    services.update_type,
    services.update_process_type,
    services.update_process_name,
    services.update_process_location,
    services.update_process_description,
    services.update_number
   FROM services;

ALTER TABLE service_list
  OWNER TO dienstemanager;

DROP VIEW service_layers;

CREATE VIEW service_layers AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.gutter,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.singletile,
    layers.transparent,
    layers.transparency,
    layers.namespace,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.legend_type,
    layers.legend_path,
    layers.featurecount,
    layers.gfi_format,
    layers.gfi_theme,
    layers.gfi_config,
    layers.gfi_complex,
    layers.request_vertex_normals,
    layers.maximum_screen_space_error,
    layers.epsg,
    layers.gfi_theme_params,
    layers.resolution,
    layers.hidelevels,
    layers.minzoom,
    layers.terrainurl,
    layers.projection,
    layers.url_params,
    layers.hittolerance,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_int
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_int
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_int,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_ext
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_ext
               FROM services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_ext
   FROM layers;


ALTER TABLE service_layers OWNER TO dienstemanager;

ALTER TABLE layers DROP COLUMN isbaselayer;

ALTER TABLE deleted_layers DROP COLUMN isbaselayer;
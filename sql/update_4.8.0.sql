ALTER TABLE public.datasets
    ADD COLUMN dataset_shortname text;

DROP VIEW public.dataset_list;
CREATE OR REPLACE VIEW public.dataset_list
 AS
 SELECT d.dataset_name,
    d.md_id,
    d.rs_id,
    d.cat_opendata,
    d.cat_inspire,
    d.cat_hmbtg,
    d.cat_org,
    d.bbox,
    d.fees,
    d.description,
    d.keywords,
    d.access_constraints,
    ( SELECT a.show_doc_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS show_doc_url,
    ( SELECT a.csw_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS source_csw,
    d.dataset_shortname
   FROM datasets d;

ALTER TABLE public.dataset_list
    OWNER TO dienstemanager;

ALTER TABLE public.service_jira_tickets
    ADD COLUMN project text;

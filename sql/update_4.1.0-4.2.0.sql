---------------------------------------------------------------------------
-- ACHTUNG!!
-- Die Tabellen mit den gelöschten Diensten und Layern werden entfernt!
-- Bei Bedarf Backup erzeugen
---------------------------------------------------------------------------


ALTER TABLE public.services ADD COLUMN external boolean DEFAULT False;

-- Layertabelle für alle OWS Typen (WMS, WMTS, WFS)
CREATE TABLE public.layers_type_ows
(
    layer_id integer NOT NULL,
    gutter integer DEFAULT 0,
    singletile boolean DEFAULT false,
    legend_type text COLLATE pg_catalog."default",
    legend_path text COLLATE pg_catalog."default",
    featurecount integer DEFAULT 1,
    namespace text COLLATE pg_catalog."default",
    hittolerance integer,
    gfi_format text COLLATE pg_catalog."default",
    gfi_complex text COLLATE pg_catalog."default" DEFAULT false,
    CONSTRAINT layers_type_ows_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_ows OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_ows IS 'Tabelle für alle OWS Typen (WMS, WMTS, WFS)';

INSERT INTO public.layers_type_ows
( layer_id, gutter, singletile, legend_type, legend_path, featurecount, gfi_format, gfi_complex )
SELECT layer_id, gutter, singletile, legend_type, legend_path, featurecount, gfi_format, gfi_complex
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'WMS';

INSERT INTO public.layers_type_ows
( layer_id, gutter, singletile, legend_type, legend_path, featurecount, gfi_format, gfi_complex )
SELECT layer_id, gutter, singletile, legend_type, legend_path, featurecount, gfi_format, gfi_complex
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'WMTS';

INSERT INTO public.layers_type_ows
( layer_id, gutter, singletile, namespace, hittolerance, featurecount, gfi_format, gfi_complex )
SELECT layer_id, gutter, singletile, namespace, hittolerance, featurecount, gfi_format, gfi_complex
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'WFS';

-- Layertabelle für alle 3D Typen (Terrain3D, TileSet3D)
CREATE TABLE public.layers_type_3d
(
    layer_id integer NOT NULL,
    request_vertex_normals boolean DEFAULT false,
    maximum_screen_space_error integer,
    CONSTRAINT layers_type_3d_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_3d OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_3d IS 'Tabelle für alle 3D Typen (Terrain3D, TileSet3D)';

INSERT INTO public.layers_type_3d
( layer_id, maximum_screen_space_error )
SELECT layer_id, maximum_screen_space_error
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'TileSet3D';

INSERT INTO public.layers_type_3d
( layer_id, request_vertex_normals )
SELECT layer_id, request_vertex_normals
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'Terrain3D';

-- Tabelle für SensorThings Layer
CREATE TABLE public.layers_type_sensorthings
(
    layer_id integer NOT NULL,
    epsg text COLLATE pg_catalog."default",
    gfi_theme_params text COLLATE pg_catalog."default",
    url_params text COLLATE pg_catalog."default",
    CONSTRAINT layers_type_sensorthings_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_sensorthings OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_sensorthings IS 'Tabelle für SensorThings Layer';

INSERT INTO public.layers_type_sensorthings
( layer_id, epsg, gfi_theme_params, url_params )
SELECT layer_id, epsg, gfi_theme_params, url_params
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'SensorThings';

-- Tabelle für Oblique Layer
CREATE TABLE public.layers_type_oblique
(
    layer_id integer NOT NULL,
    resolution integer,
    terrainurl text COLLATE pg_catalog."default",
    projection text COLLATE pg_catalog."default",
    minzoom integer,
    hidelevels integer,
    CONSTRAINT layers_type_oblique_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_oblique OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_oblique IS 'Tabelle für Oblique Layer';

INSERT INTO public.layers_type_oblique
( layer_id, resolution, terrainurl, projection, minzoom, hidelevels )
SELECT layer_id, resolution, terrainurl, projection, minzoom, hidelevels
FROM public.layers
WHERE (SELECT service_type FROM services WHERE service_id=layers.service_id) = 'Oblique';

DROP VIEW public.service_layers;

ALTER TABLE public.layers DROP COLUMN IF EXISTS resolution;
ALTER TABLE public.layers DROP COLUMN IF EXISTS terrainurl;
ALTER TABLE public.layers DROP COLUMN IF EXISTS projection;
ALTER TABLE public.layers DROP COLUMN IF EXISTS minzoom;
ALTER TABLE public.layers DROP COLUMN IF EXISTS hidelevels;
ALTER TABLE public.layers DROP COLUMN IF EXISTS epsg;
ALTER TABLE public.layers DROP COLUMN IF EXISTS gfi_theme_params;
ALTER TABLE public.layers DROP COLUMN IF EXISTS url_params;
ALTER TABLE public.layers DROP COLUMN IF EXISTS request_vertex_normals;
ALTER TABLE public.layers DROP COLUMN IF EXISTS maximum_screen_space_error;
ALTER TABLE public.layers DROP COLUMN IF EXISTS gutter;
ALTER TABLE public.layers DROP COLUMN IF EXISTS singletile;
ALTER TABLE public.layers DROP COLUMN IF EXISTS legend_type;
ALTER TABLE public.layers DROP COLUMN IF EXISTS legend_path;
ALTER TABLE public.layers DROP COLUMN IF EXISTS featurecount;
ALTER TABLE public.layers DROP COLUMN IF EXISTS namespace;
ALTER TABLE public.layers DROP COLUMN IF EXISTS hittolerance;
ALTER TABLE public.layers DROP COLUMN IF EXISTS gfi_format;
ALTER TABLE public.layers DROP COLUMN IF EXISTS gfi_complex;

ALTER TABLE public.layers ADD COLUMN deleted boolean DEFAULT False;

CREATE OR REPLACE VIEW public.service_layers
 AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.url_params,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE layers.service_id = services.service_id)) = 'WFS'::text THEN ((((( SELECT services.url_int
               FROM services
              WHERE layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || (( SELECT services.version
               FROM services
              WHERE layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name
            WHEN (( SELECT services.service_type
               FROM services
              WHERE layers.service_id = services.service_id)) = 'WMS'::text THEN ((((( SELECT services.url_int
               FROM services
              WHERE layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || (( SELECT services.version
               FROM services
              WHERE layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name
            ELSE ''::text
        END AS testlink_int,
        CASE
            WHEN (( SELECT services.service_type
               FROM services
              WHERE layers.service_id = services.service_id)) = 'WFS'::text THEN ((((( SELECT services.url_ext
               FROM services
              WHERE layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || (( SELECT services.version
               FROM services
              WHERE layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name
            WHEN (( SELECT services.service_type
               FROM services
              WHERE layers.service_id = services.service_id)) = 'WMS'::text THEN ((((( SELECT services.url_ext
               FROM services
              WHERE layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || (( SELECT services.version
               FROM services
              WHERE layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name
            ELSE ''::text
        END AS testlink_ext
   FROM layers
     LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id
     LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id
     LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id
     LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id
  WHERE layers.deleted = false;

ALTER TABLE public.service_layers OWNER TO dienstemanager;

ALTER TABLE public.services ADD COLUMN deleted boolean DEFAULT False;

DROP VIEW public.service_list;

CREATE OR REPLACE VIEW public.service_list
AS
SELECT b.service_id,
b.service_checked_status,
to_char(b.last_edit_date, 'YYYY-MM-DD HH24:MI'::text) AS last_edit_date,
b.service_name,
b.title,
b.fees,
b.access_constraints,
b.description,
b.editor,
b.status,
b.service_type,
b.software,
b.server,
b.folder,
b.url_ext,
b.url_int,
b.url_sec,
b.security_type,
b.allowed_groups::text AS allowed_groups,
b.only_intranet,
b.only_internet,
b.datasource,
b.mapping_description,
b.cached,
b.service_md_id,
( SELECT a.csw_url
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS source_csw,
( SELECT a.show_doc_url
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS show_doc_url,
( SELECT a.name
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS csw_name,
b.dsc_complete,
b.inspire_relevant,
b.last_edited_by,
b.version,
b.qa,
to_char(b.update_date::timestamp with time zone, 'YYYY-MM-DD'::text) AS update_date,
b.update_unit,
b.update_type,
b.update_process_type,
b.update_process_name,
b.update_process_location,
b.update_process_description,
b.update_number,
b.external
FROM services b
  WHERE b.deleted = false;

CREATE OR REPLACE VIEW public.layer_list AS
 SELECT layers.layer_id,
    layers.layer_name,
    layers.title,
    layers.service_id,
    ( SELECT services.title
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_title,
    ( SELECT services.service_type
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_type,
        CASE
            WHEN (( SELECT services.service_type
               FROM public.services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_int
               FROM public.services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM public.services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM public.services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_int
               FROM public.services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM public.services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_int,
        CASE
            WHEN (( SELECT services.service_type
               FROM public.services
              WHERE (layers.service_id = services.service_id)) = 'WFS'::text) THEN ((((( SELECT services.url_ext
               FROM public.services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WFS&VERSION='::text) || ( SELECT services.version
               FROM public.services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetFeature&maxFeatures=1&typename='::text) || layers.layer_name)
            WHEN (( SELECT services.service_type
               FROM public.services
              WHERE (layers.service_id = services.service_id)) = 'WMS'::text) THEN ((((( SELECT services.url_ext
               FROM public.services
              WHERE (layers.service_id = services.service_id)) || '?SERVICE=WMS&VERSION='::text) || ( SELECT services.version
               FROM public.services
              WHERE (layers.service_id = services.service_id))) || '&REQUEST=GetMap&FORMAT=image/png&BBOX=562394,5932816,564194,5934216&WIDTH=800&HEIGHT=600&BGCOLOR=0xFFFFFF&EXCEPTIONS=application/vnd.ogc.se_inimage&TRANSPARENT=TRUE&STYLES=&CRS=EPSG:25832&LAYERS='::text) || layers.layer_name)
            ELSE ''::text
        END AS testlink_ext
   FROM public.layers
  WHERE (layers.deleted = false);
  
ALTER TABLE public.layer_list OWNER TO dienstemanager;

DROP TABLE public.deleted_services;
DROP TABLE public.deleted_layers;

ALTER TABLE public.service_list OWNER TO dienstemanager;

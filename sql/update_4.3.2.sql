DROP VIEW public.service_layers;

CREATE OR REPLACE VIEW public.service_layers AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.filter,
    sensorthings.expand
   FROM public.layers
     LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id
     LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id
     LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id
     LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id
  WHERE layers.deleted = false;

ALTER TABLE public.service_layers OWNER TO dienstemanager;

-- Nicht mehr benötigte Spalte mqttEntity entfernen

ALTER TABLE layers_type_sensorthings DROP COLUMN IF EXISTS mqttEntity;

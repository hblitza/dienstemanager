--
-- Dienstemanager 4.3.1
--


--
-- TOC entry 1289 (class 1255 OID 176735785)
-- Name: upsert_dataset(text, text, text, text, text, text, text, text, text, text, integer, text, text, integer, integer); Type: FUNCTION; Schema: public; Owner: dienstemanager
--

CREATE OR REPLACE FUNCTION public.upsert_dataset(
	_dataset_name text,
	_md_id text,
	_rs_id text,
	_cat_opendata text,
	_cat_inspire text,
	_cat_hmbtg text,
	_cat_org text,
	_bbox text,
	_fees text,
	_access_constraints text,
	_config_metadata_catalog_id integer,
	_description text,
	_keywords text,
	_srs_metadata integer,
	_srs_services integer)
RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
DECLARE
BEGIN
    UPDATE datasets SET dataset_name = _dataset_name, rs_id = _rs_id, cat_opendata = _cat_opendata, cat_inspire = _cat_inspire, cat_hmbtg = _cat_hmbtg, cat_org = _cat_org, fees = _fees, access_constraints = _access_constraints, description = _description, keywords = _keywords, config_metadata_catalog_id = _config_metadata_catalog_id, bbox = (SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')', _srs_metadata),_srs_services))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')',_srs_metadata),_srs_services))::text)-5),' ', ',')) WHERE md_id = _md_id;
    IF NOT FOUND THEN
    INSERT INTO datasets(
            dataset_name, md_id, rs_id, cat_opendata, cat_inspire, cat_hmbtg,
            cat_org, bbox, fees, access_constraints, config_metadata_catalog_id, description, keywords)
    VALUES (_dataset_name, _md_id, _rs_id, _cat_opendata, _cat_inspire, _cat_hmbtg,
            _cat_org, (SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')', _srs_metadata),_srs_services))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')',_srs_metadata),_srs_services))::text)-5),' ', ',')), _fees, _access_constraints, _config_metadata_catalog_id, _description, _keywords);
    END IF;
END;
$BODY$;

ALTER FUNCTION public.upsert_dataset(text, text, text, text, text, text, text, text, text, text, integer, text, text, integer, integer)
    OWNER TO dienstemanager;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 217 (class 1259 OID 143616318)
-- Name: changelog; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.changelog (
    changelog_id integer NOT NULL,
    username text,
    service_id integer,
    change_date date,
    change text
);


ALTER TABLE public.changelog OWNER TO dienstemanager;

--
-- TOC entry 216 (class 1259 OID 143616316)
-- Name: changelog_changelog_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.changelog_changelog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.changelog_changelog_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4043 (class 0 OID 0)
-- Dependencies: 216
-- Name: changelog_changelog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.changelog_changelog_id_seq OWNED BY public.changelog.changelog_id;


--
-- TOC entry 205 (class 1259 OID 141259181)
-- Name: comments; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.comments (
    comment_id integer NOT NULL,
    author text,
    text text,
    post_date date,
    service_id integer
);


ALTER TABLE public.comments OWNER TO dienstemanager;

--
-- TOC entry 204 (class 1259 OID 141259179)
-- Name: comments_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.comments_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_comment_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4044 (class 0 OID 0)
-- Dependencies: 204
-- Name: comments_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.comments_comment_id_seq OWNED BY public.comments.comment_id;


--
-- TOC entry 221 (class 1259 OID 176691206)
-- Name: config_metadata_catalogs; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.config_metadata_catalogs (
    config_metadata_catalog_id integer NOT NULL,
    name text,
    csw_url text,
    show_doc_url text,
    proxy boolean,
    srs_metadata integer DEFAULT 4326
);


ALTER TABLE public.config_metadata_catalogs OWNER TO dienstemanager;

--
-- TOC entry 220 (class 1259 OID 176691204)
-- Name: config_metadata_catalogs_config_metadata_catalogs_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.config_metadata_catalogs_config_metadata_catalogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.config_metadata_catalogs_config_metadata_catalogs_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4045 (class 0 OID 0)
-- Dependencies: 220
-- Name: config_metadata_catalogs_config_metadata_catalogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.config_metadata_catalogs_config_metadata_catalogs_id_seq OWNED BY public.config_metadata_catalogs.config_metadata_catalog_id;


--
-- TOC entry 200 (class 1259 OID 141259153)
-- Name: contact_links; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.contact_links (
    contact_link_id integer NOT NULL,
    service_id integer,
    contact_id integer
);


ALTER TABLE public.contact_links OWNER TO dienstemanager;

--
-- TOC entry 199 (class 1259 OID 141259151)
-- Name: contact_links_contact_link_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.contact_links_contact_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_links_contact_link_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4046 (class 0 OID 0)
-- Dependencies: 199
-- Name: contact_links_contact_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.contact_links_contact_link_id_seq OWNED BY public.contact_links.contact_link_id;


--
-- TOC entry 190 (class 1259 OID 140595057)
-- Name: contacts; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.contacts (
    contact_id integer NOT NULL,
    name text,
    given_name text,
    surname text,
    company text,
    ad_account text,
    email text,
    tel text,
    uid text
);


ALTER TABLE public.contacts OWNER TO dienstemanager;

--
-- TOC entry 189 (class 1259 OID 140595055)
-- Name: contacts_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.contacts_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contacts_contact_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4047 (class 0 OID 0)
-- Dependencies: 189
-- Name: contacts_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.contacts_contact_id_seq OWNED BY public.contacts.contact_id;


--
-- TOC entry 191 (class 1259 OID 140595066)
-- Name: datasets; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.datasets (
    dataset_name text,
    md_id text NOT NULL,
    rs_id text,
    cat_opendata text,
    cat_inspire text,
    cat_hmbtg text,
    cat_org text,
    bbox text,
    fees text,
    description text,
    keywords text,
    access_constraints text,
    config_metadata_catalog_id integer
);


ALTER TABLE public.datasets OWNER TO dienstemanager;

--
-- TOC entry 224 (class 1259 OID 176738667)
-- Name: dataset_list; Type: VIEW; Schema: public; Owner: dienstemanager
--

CREATE OR REPLACE VIEW public.dataset_list AS
 SELECT d.dataset_name,
    d.md_id,
    d.rs_id,
    d.cat_opendata,
    d.cat_inspire,
    d.cat_hmbtg,
    d.cat_org,
    d.bbox,
    d.fees,
    d.description,
    d.keywords,
    d.access_constraints,
    ( SELECT a.show_doc_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS show_doc_url,
    ( SELECT a.csw_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS source_csw
   FROM datasets d;

ALTER TABLE public.dataset_list
	OWNER TO dienstemanager;


--
-- TOC entry 207 (class 1259 OID 141259200)
-- Name: freigaben; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.freigaben (
    freigabe_id integer NOT NULL,
    datenbezeichnung text,
    link text,
    dateneigentuemer text,
    ansprechpartner text,
    hmdk_eintrag text,
    hmdk_daten boolean,
    hmdk_dienst boolean,
    einschraenkung text,
    aaa_web boolean,
    geoportal_hh boolean,
    frei_nutzbar boolean,
    wms_intranet boolean,
    wms_internet boolean,
    wfs_intranet boolean,
    wfs_internet boolean,
    internet boolean,
    bemerkungen text,
    url_wms text,
    alias_name text,
    hambtg boolean,
    opendata boolean,
    fhh_atlas boolean,
    url_wfs text,
    erhalten_am text,
    leitzeichen text
);


ALTER TABLE public.freigaben OWNER TO dienstemanager;

--
-- TOC entry 206 (class 1259 OID 141259198)
-- Name: freigaben_freigabe_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.freigaben_freigabe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.freigaben_freigabe_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4048 (class 0 OID 0)
-- Dependencies: 206
-- Name: freigaben_freigabe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.freigaben_freigabe_id_seq OWNED BY public.freigaben.freigabe_id;


--
-- TOC entry 202 (class 1259 OID 141259161)
-- Name: freigaben_links; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.freigaben_links (
    freigaben_link_id integer NOT NULL,
    freigabe_id integer,
    service_id integer
);


ALTER TABLE public.freigaben_links OWNER TO dienstemanager;

--
-- TOC entry 201 (class 1259 OID 141259159)
-- Name: freigaben_links_freigaben_link_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.freigaben_links_freigaben_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.freigaben_links_freigaben_link_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4049 (class 0 OID 0)
-- Dependencies: 201
-- Name: freigaben_links_freigaben_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.freigaben_links_freigaben_link_id_seq OWNED BY public.freigaben_links.freigaben_link_id;


--
-- TOC entry 193 (class 1259 OID 140595076)
-- Name: keywords; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.keywords (
    keyword_id integer NOT NULL,
    service_id integer,
    text text
);


ALTER TABLE public.keywords OWNER TO dienstemanager;

--
-- TOC entry 192 (class 1259 OID 140595074)
-- Name: keywords_keyword_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.keywords_keyword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keywords_keyword_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4050 (class 0 OID 0)
-- Dependencies: 192
-- Name: keywords_keyword_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.keywords_keyword_id_seq OWNED BY public.keywords.keyword_id;


SET default_with_oids = true;

--
-- TOC entry 218 (class 1259 OID 144625798)
-- Name: layer_excludes; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.layer_excludes (
    layer_id integer,
    exclude_from text
);


ALTER TABLE public.layer_excludes OWNER TO dienstemanager;

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 141482885)
-- Name: layer_json; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.layer_json (
    layer_id integer,
    service_id integer,
    only_intranet boolean,
    only_internet boolean,
    status text,
    json_intranet json,
    json_internet json
);


ALTER TABLE public.layer_json OWNER TO dienstemanager;

--
-- TOC entry 198 (class 1259 OID 140914548)
-- Name: layer_links; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.layer_links (
    layer_links_id integer NOT NULL,
    layer_id integer NOT NULL,
    md_id character varying NOT NULL,
    service_id integer
);


ALTER TABLE public.layer_links OWNER TO dienstemanager;

--
-- TOC entry 223 (class 1259 OID 176723286)
-- Name: layer_links_dataset; Type: VIEW; Schema: public; Owner: dienstemanager
--

CREATE VIEW public.layer_links_dataset AS
 SELECT ll.layer_links_id,
    ll.layer_id,
    ll.md_id,
    ll.service_id,
    d.dataset_name,
    ( SELECT a.show_doc_url
           FROM public.config_metadata_catalogs a
          WHERE (a.config_metadata_catalog_id = d.config_metadata_catalog_id)) AS show_doc_url
   FROM (public.layer_links ll
     JOIN public.datasets d ON (((ll.md_id)::text = d.md_id)));


ALTER TABLE public.layer_links_dataset OWNER TO dienstemanager;

--
-- TOC entry 197 (class 1259 OID 140914546)
-- Name: layer_links_layer_links_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.layer_links_layer_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.layer_links_layer_links_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4051 (class 0 OID 0)
-- Dependencies: 197
-- Name: layer_links_layer_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.layer_links_layer_links_id_seq OWNED BY public.layer_links.layer_links_id;


--
-- TOC entry 212 (class 1259 OID 141318561)
-- Name: layers; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.layers (
    layer_id integer NOT NULL,
    service_id integer,
    layer_name text,
    title text,
    title_alt text,
    layerattribution text,
    additional_categories text,
    tilesize integer DEFAULT 512,
    legend_url_intranet text,
    legend_url_internet text,
    transparent boolean DEFAULT true,
    transparency integer DEFAULT 0,
    output_format text,
    scale_min text DEFAULT '0'::text,
    scale_max text DEFAULT '2500000'::text,
    gfi_theme text DEFAULT 'default'::text,
    gfi_config text DEFAULT 'showAll'::text,
    deleted boolean DEFAULT false
);


ALTER TABLE public.layers OWNER TO dienstemanager;


--
-- TOC entry 188 (class 1259 OID 140595039)
-- Name: services; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.services (
    service_id integer NOT NULL,
    service_checked_status text DEFAULT 'Fehler'::text,
    last_edit_date timestamp without time zone,
    service_name text,
    title text,
    editor text,
    status text,
    service_type text,
    software text,
    server text,
    folder text,
    url_ext text,
    url_int text,
    url_sec text,
    security_type text,
    only_intranet boolean DEFAULT true,
    only_internet boolean DEFAULT false,
    datasource text,
    mapping_description text,
    cached boolean DEFAULT false,
    service_md_id text,
    dsc_complete boolean DEFAULT false,
    inspire_relevant boolean DEFAULT false,
    last_edited_by text,
    version text,
    qa boolean DEFAULT false,
    update_date date,
    update_unit text,
    update_type text,
    update_process_type text,
    update_process_name text,
    update_process_location text,
    update_process_description text,
    update_number text,
    description text,
    fees text,
    access_constraints text,
    allowed_groups json,
    config_metadata_catalog_id integer,
    external boolean DEFAULT False,
    deleted boolean DEFAULT False
);


ALTER TABLE public.services OWNER TO dienstemanager;

--
-- TOC entry 213 (class 1259 OID 141318627)
-- Name: layer_list; Type: VIEW; Schema: public; Owner: dienstemanager
--

CREATE VIEW public.layer_list AS
 SELECT layers.layer_id,
    layers.layer_name,
    layers.title,
    layers.service_id,
    ( SELECT services.title
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_title,
    ( SELECT services.service_type
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_type
   FROM public.layers
   WHERE layers.deleted = false;


ALTER TABLE public.layer_list OWNER TO dienstemanager;

--
-- TOC entry 211 (class 1259 OID 141318559)
-- Name: layers_layer_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.layers_layer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.layers_layer_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4057 (class 0 OID 0)
-- Dependencies: 211
-- Name: layers_layer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.layers_layer_id_seq OWNED BY public.layers.layer_id;


SET default_with_oids = true;

--
-- TOC entry 209 (class 1259 OID 141311595)
-- Name: portal_links; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.portal_links (
    layer_id integer,
    portal_md_id text
);


ALTER TABLE public.portal_links OWNER TO dienstemanager;

--
-- TOC entry 208 (class 1259 OID 141311587)
-- Name: portals; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.portals (
    md_id text NOT NULL,
    name text,
    url text
);


ALTER TABLE public.portals OWNER TO dienstemanager;

--
-- TOC entry 222 (class 1259 OID 176716120)
-- Name: service_list; Type: VIEW; Schema: public; Owner: dienstemanager
--

CREATE VIEW public.service_list AS
 SELECT b.service_id,
    b.service_checked_status,
    to_char(b.last_edit_date, 'YYYY-MM-DD HH24:MI'::text) AS last_edit_date,
    b.service_name,
    b.title,
    b.fees,
    b.access_constraints,
    b.description,
    b.editor,
    b.status,
    b.service_type,
    b.software,
    b.server,
    b.folder,
    b.url_ext,
    b.url_int,
    b.url_sec,
    b.security_type,
    (b.allowed_groups)::text AS allowed_groups,
    b.only_intranet,
    b.only_internet,
    b.datasource,
    b.mapping_description,
    b.cached,
    b.service_md_id,
    ( SELECT a.csw_url
           FROM public.config_metadata_catalogs a
          WHERE (a.config_metadata_catalog_id = b.config_metadata_catalog_id)) AS source_csw,
    ( SELECT a.show_doc_url
           FROM public.config_metadata_catalogs a
          WHERE (a.config_metadata_catalog_id = b.config_metadata_catalog_id)) AS show_doc_url,
    ( SELECT a.name
           FROM public.config_metadata_catalogs a
          WHERE (a.config_metadata_catalog_id = b.config_metadata_catalog_id)) AS csw_name,
    b.dsc_complete,
    b.inspire_relevant,
    b.last_edited_by,
    b.version,
    b.qa,
    to_char((b.update_date)::timestamp with time zone, 'YYYY-MM-DD'::text) AS update_date,
    b.update_unit,
    b.update_type,
    b.update_process_type,
    b.update_process_name,
    b.update_process_location,
    b.update_process_description,
    b.update_number,
    b.external
   FROM public.services b
   WHERE b.deleted = false;


ALTER TABLE public.service_list OWNER TO dienstemanager;

SET default_with_oids = false;

--
-- TOC entry 195 (class 1259 OID 140914492)
-- Name: service_test_results; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.service_test_results (
    service_test_results_id integer NOT NULL,
    service_id integer,
    text text
);


ALTER TABLE public.service_test_results OWNER TO dienstemanager;

--
-- TOC entry 215 (class 1259 OID 143564378)
-- Name: service_test_results_checked; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.service_test_results_checked (
    service_test_results_id integer NOT NULL,
    service_id integer,
    text text
);


ALTER TABLE public.service_test_results_checked OWNER TO dienstemanager;

--
-- TOC entry 194 (class 1259 OID 140914490)
-- Name: service_test_results_service_test_results_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.service_test_results_service_test_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_test_results_service_test_results_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4060 (class 0 OID 0)
-- Dependencies: 194
-- Name: service_test_results_service_test_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.service_test_results_service_test_results_id_seq OWNED BY public.service_test_results.service_test_results_id;


--
-- TOC entry 187 (class 1259 OID 140595037)
-- Name: services_service_id_seq; Type: SEQUENCE; Schema: public; Owner: dienstemanager
--

CREATE SEQUENCE public.services_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.services_service_id_seq OWNER TO dienstemanager;

--
-- TOC entry 4061 (class 0 OID 0)
-- Dependencies: 187
-- Name: services_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dienstemanager
--

ALTER SEQUENCE public.services_service_id_seq OWNED BY public.services.service_id;


--
-- TOC entry 203 (class 1259 OID 141259165)
-- Name: visits; Type: TABLE; Schema: public; Owner: dienstemanager
--

CREATE TABLE public.visits (
    service_id integer,
    title text,
    name_ext text,
    visits_intranet integer,
    visits_internet integer,
    visits_total integer,
    month text
);


ALTER TABLE public.visits OWNER TO dienstemanager;

--
-- TOC entry 3882 (class 2604 OID 143616321)
-- Name: changelog changelog_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.changelog ALTER COLUMN changelog_id SET DEFAULT nextval('public.changelog_changelog_id_seq'::regclass);


--
-- TOC entry 3869 (class 2604 OID 141259184)
-- Name: comments comment_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.comments ALTER COLUMN comment_id SET DEFAULT nextval('public.comments_comment_id_seq'::regclass);


--
-- TOC entry 3883 (class 2604 OID 176691209)
-- Name: config_metadata_catalogs config_metadata_catalog_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.config_metadata_catalogs ALTER COLUMN config_metadata_catalog_id SET DEFAULT nextval('public.config_metadata_catalogs_config_metadata_catalogs_id_seq'::regclass);


--
-- TOC entry 3867 (class 2604 OID 141259156)
-- Name: contact_links contact_link_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.contact_links ALTER COLUMN contact_link_id SET DEFAULT nextval('public.contact_links_contact_link_id_seq'::regclass);


--
-- TOC entry 3863 (class 2604 OID 140595060)
-- Name: contacts contact_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.contacts ALTER COLUMN contact_id SET DEFAULT nextval('public.contacts_contact_id_seq'::regclass);


--
-- TOC entry 3870 (class 2604 OID 141259203)
-- Name: freigaben freigabe_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.freigaben ALTER COLUMN freigabe_id SET DEFAULT nextval('public.freigaben_freigabe_id_seq'::regclass);


--
-- TOC entry 3868 (class 2604 OID 141259164)
-- Name: freigaben_links freigaben_link_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.freigaben_links ALTER COLUMN freigaben_link_id SET DEFAULT nextval('public.freigaben_links_freigaben_link_id_seq'::regclass);


--
-- TOC entry 3864 (class 2604 OID 140595079)
-- Name: keywords keyword_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.keywords ALTER COLUMN keyword_id SET DEFAULT nextval('public.keywords_keyword_id_seq'::regclass);


--
-- TOC entry 3866 (class 2604 OID 140914551)
-- Name: layer_links layer_links_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layer_links ALTER COLUMN layer_links_id SET DEFAULT nextval('public.layer_links_layer_links_id_seq'::regclass);


--
-- TOC entry 3871 (class 2604 OID 141318564)
-- Name: layers layer_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layers ALTER COLUMN layer_id SET DEFAULT nextval('public.layers_layer_id_seq'::regclass);


--
-- TOC entry 3865 (class 2604 OID 140914495)
-- Name: service_test_results service_test_results_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.service_test_results ALTER COLUMN service_test_results_id SET DEFAULT nextval('public.service_test_results_service_test_results_id_seq'::regclass);


--
-- TOC entry 3855 (class 2604 OID 140595042)
-- Name: services service_id; Type: DEFAULT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.services ALTER COLUMN service_id SET DEFAULT nextval('public.services_service_id_seq'::regclass);


--
-- TOC entry 3909 (class 2606 OID 143616326)
-- Name: changelog changelog_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.changelog
    ADD CONSTRAINT changelog_pk PRIMARY KEY (changelog_id);


--
-- TOC entry 3897 (class 2606 OID 141259158)
-- Name: contact_links contact_links_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.contact_links
    ADD CONSTRAINT contact_links_pk PRIMARY KEY (contact_link_id);


--
-- TOC entry 3887 (class 2606 OID 140595065)
-- Name: contacts contacts_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.contacts
    ADD CONSTRAINT contacts_pk PRIMARY KEY (contact_id);


--
-- TOC entry 3889 (class 2606 OID 140595073)
-- Name: datasets datasets_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.datasets
    ADD CONSTRAINT datasets_pk PRIMARY KEY (md_id);


--
-- TOC entry 3891 (class 2606 OID 140595084)
-- Name: keywords keywords_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.keywords
    ADD CONSTRAINT keywords_pk PRIMARY KEY (keyword_id);


--
-- TOC entry 3899 (class 2606 OID 141259189)
-- Name: comments kommentare_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT kommentare_pk PRIMARY KEY (comment_id);


--
-- TOC entry 3905 (class 2606 OID 141318578)
-- Name: layers layer_pkey; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layers
    ADD CONSTRAINT layer_pkey PRIMARY KEY (layer_id);


--
-- TOC entry 3901 (class 2606 OID 141259208)
-- Name: freigaben pk_freigaben; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.freigaben
    ADD CONSTRAINT pk_freigaben PRIMARY KEY (freigabe_id);


--
-- TOC entry 3895 (class 2606 OID 140914556)
-- Name: layer_links pk_layerlink; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layer_links
    ADD CONSTRAINT pk_layerlink PRIMARY KEY (layer_links_id);


--
-- TOC entry 3903 (class 2606 OID 141311594)
-- Name: portals portals_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.portals
    ADD CONSTRAINT portals_pk PRIMARY KEY (md_id);


--
-- TOC entry 3907 (class 2606 OID 143564385)
-- Name: service_test_results_checked service_test_results_checked_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.service_test_results_checked
    ADD CONSTRAINT service_test_results_checked_pk PRIMARY KEY (service_test_results_id);


--
-- TOC entry 3893 (class 2606 OID 140914500)
-- Name: service_test_results service_test_results_pk; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.service_test_results
    ADD CONSTRAINT service_test_results_pk PRIMARY KEY (service_test_results_id);


--
-- TOC entry 3885 (class 2606 OID 140595054)
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_pkey PRIMARY KEY (service_id);


--
-- TOC entry 3911 (class 2606 OID 145206231)
-- Name: layer_links fk_layer_id; Type: FK CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layer_links
    ADD CONSTRAINT fk_layer_id FOREIGN KEY (layer_id) REFERENCES public.layers(layer_id) ON DELETE CASCADE;


--
-- TOC entry 3910 (class 2606 OID 145206226)
-- Name: layer_links fk_service_id; Type: FK CONSTRAINT; Schema: public; Owner: dienstemanager
--

ALTER TABLE ONLY public.layer_links
    ADD CONSTRAINT fk_service_id FOREIGN KEY (service_id) REFERENCES public.services(service_id) ON DELETE CASCADE;


--
-- TOC entry 4040 (class 0 OID 0)
-- Dependencies: 8
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 4058 (class 0 OID 0)
-- Dependencies: 195
-- Name: TABLE service_test_results; Type: ACL; Schema: public; Owner: dienstemanager
--

REVOKE ALL ON TABLE public.service_test_results FROM PUBLIC;
REVOKE ALL ON TABLE public.service_test_results FROM dienstemanager;
GRANT ALL ON TABLE public.service_test_results TO dienstemanager;


--
-- TOC entry 4059 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE service_test_results_checked; Type: ACL; Schema: public; Owner: dienstemanager
--

REVOKE ALL ON TABLE public.service_test_results_checked FROM PUBLIC;
GRANT ALL ON TABLE public.service_test_results_checked TO dienstemanager;

-- Layertabelle für alle OWS Typen (WMS, WMTS, WFS)
CREATE TABLE public.layers_type_ows
(
    layer_id integer NOT NULL,
    gutter integer DEFAULT 0,
    singletile boolean DEFAULT false,
    legend_type text COLLATE pg_catalog."default",
    legend_path text COLLATE pg_catalog."default",
    featurecount integer DEFAULT 1,
    namespace text COLLATE pg_catalog."default",
    hittolerance integer,
    gfi_format text COLLATE pg_catalog."default",
    gfi_complex text COLLATE pg_catalog."default" DEFAULT false,
    CONSTRAINT layers_type_ows_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_ows OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_ows IS 'Tabelle für alle OWS Typen (WMS, WMTS, WFS)';

-- Layertabelle für alle 3D Typen (Terrain3D, TileSet3D)
CREATE TABLE public.layers_type_3d
(
    layer_id integer NOT NULL,
    request_vertex_normals boolean DEFAULT false,
    maximum_screen_space_error integer,
    CONSTRAINT layers_type_3d_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_3d OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_3d IS 'Tabelle für alle 3D Typen (Terrain3D, TileSet3D)';

-- Tabelle für SensorThings Layer
CREATE TABLE public.layers_type_sensorthings
(
    layer_id integer NOT NULL,
    epsg text COLLATE pg_catalog."default",
    gfi_theme_params text COLLATE pg_catalog."default",
    filter text COLLATE pg_catalog."default",
    expand text COLLATE pg_catalog."default",
    CONSTRAINT layers_type_sensorthings_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_sensorthings OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_sensorthings IS 'Tabelle für SensorThings Layer';

-- Tabelle für Oblique Layer
CREATE TABLE public.layers_type_oblique
(
    layer_id integer NOT NULL,
    resolution integer,
    terrainurl text COLLATE pg_catalog."default",
    projection text COLLATE pg_catalog."default",
    minzoom integer,
    hidelevels integer,
    CONSTRAINT layers_type_oblique_pkey PRIMARY KEY (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layers_type_oblique OWNER to dienstemanager;
COMMENT ON TABLE public.layers_type_oblique IS 'Tabelle für Oblique Layer';

--
-- TOC entry 219 (class 1259 OID 147513513)
-- Name: service_layers; Type: VIEW; Schema: public; Owner: dienstemanager
--

CREATE VIEW public.service_layers AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.filter,
    sensorthings.expand
   FROM public.layers
     LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id
     LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id
     LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id
     LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id
  WHERE layers.deleted = false;


ALTER TABLE public.service_layers OWNER TO dienstemanager;

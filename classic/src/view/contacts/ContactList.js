Ext.define("DiensteManager.view.contacts.ContactList", {
    extend: "Ext.form.Panel",
    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],
    id: "contact_list",
    controller: "contact_list",
    layout: "column",
    height: Ext.Element.getViewportHeight() - 70,

    viewModel: {
        data: {
            Contact: null
        }
    },

    items: [{
        xtype: "gridpanel",
        id: "contacts_grid",
        autoScroll: true,
        height: Ext.Element.getViewportHeight() - 70,
        columnWidth: 0.65,
        bind: {
            selection: "{Contact}"
        },
        store: "Contacts",
        columns: [
            {
                dataIndex: "given_name",
                text: "Vorname",
                align: "center",
                width: 150
            },
            {
                dataIndex: "surname",
                text: "Nachname",
                align: "center",
                width: 150
            },
            {
                dataIndex: "company",
                text: "Unternehmen",
                align: "left",
                flex: 1
            }
        ]
    }, {
        xtype: "fieldset",
        title: "Kontaktdetails",

        columnWidth: 0.35,
        margin: "0 10 0 10",
        layout: "anchor",
        defaultType: "textfield",

        items: [
            {
                xtype: "fieldcontainer",
                id: "contactDetailsButtons",
                layout: "hbox",
                labelWidth: 0,
                width: "100%"
            }, {
                fieldLabel: "Vorname",
                id: "contactgivenname",
                bind: "{Contact.given_name}",
                width: "100%"
            }, {
                fieldLabel: "Nachname",
                id: "contactsurname",
                bind: "{Contact.surname}",
                width: "100%"
            }, {
                fieldLabel: "Unternehmen",
                id: "contactcompany",
                bind: "{Contact.company}",
                width: "100%"
            }, {
                fieldLabel: "E-Mail",
                id: "contactemail",
                bind: "{Contact.email}",
                width: "100%"
            }, {
                fieldLabel: "Telefonnummer",
                id: "contacttel",
                bind: "{Contact.tel}",
                width: "100%"
            }, {
                fieldLabel: "LDAP Account",
                id: "contactadaccount",
                bind: "{Contact.ad_account}",
                width: "100%"
            }, {
                xtype: "button",
                id: "save_contact",
                margin: "10 0 5 0",
                tooltip: "Speichert die Kontaktdetails",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Speichert die Kontaktdetails"
                },
                text: "Speichern",
                listeners: {
                    click: "onSaveContact"
                }
            }
        ]
    }]
});

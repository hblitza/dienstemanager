Ext.define("DiensteManager.view.servicedetails.freigaben.LinkFreigaben", {
    extend: "Ext.window.Window",
    id: "linkfreigaben-window",
    alias: "servicedetails.freigaben.linkfreigaben",
    height: 435,
    width: 730,
    title: "Alle Verfügbaren Freigaben",
    controller: "linkfreigaben",

    initComponent: function () {
        this.items = [
            {
                width: 300,
                xtype: "dmsearchfield",
                id: "freigaben_search_input",
                storeId: "FreigabenList",
                margin: "2px 2px 2px 2px"
            },
            {
                xtype: "grid",
                store: "FreigabenList",
                id: "linkfreigaben-grid",
                height: 310,
                autoScroll: true,
                viewConfig: {
                    enableTextSelection: true,
                    getRowClass: function (record) {
                        var freigabe = Ext.getStore("LinkedFreigaben").findRecord("freigabe_id", record.get("id"), 0, false, false, true);

                        return freigabe ? "general-rule" : "";
                    }
                },
                listeners: {
                    beforeselect: function (row, model) {
                        var freigabe = Ext.getStore("LinkedFreigaben").findRecord("freigabe_id", model.data.id, 0, false, false, true);

                        if (freigabe) {
                            return false;
                        }
                    }
                },
                columns: [
                    {
                        header: "Datenbezeichnung",
                        dataIndex: "datenbezeichnung",
                        align: "left",
                        flex: 1
                    }, {
                        header: "Link",
                        dataIndex: "link",
                        width: 80,
                        align: "center",
                        renderer: function (value) {
                            return "<a href=\"" + value + "\" target=\"_blank\">Link</a>";
                        }
                    }
                ],
                columnLines: true,
                selModel: {
                    type: "checkboxmodel",
                    checkOnly: false,
                    mode: "SINGLE"
                }
            }
        ];
        this.buttons = [{
            text: "Speichern",
            tooltip: "Die gewählte Freigabe wird mit dem Dienst verknüpft.",
            listeners: {
                click: "onSaveFreigabeLink"
            }
        }];
        this.listeners = {
            close: function () {
                Ext.getStore("FreigabenList").removeFilter("searchFilter");
                var search_store = "ServiceList";

                if (window.location.href.split("#")[1] === "layer_list") {
                    search_store = "LayerList";
                }
                Ext.getCmp("search_input").storeId = search_store;
            }
        };

        this.callParent(arguments);
    }
});

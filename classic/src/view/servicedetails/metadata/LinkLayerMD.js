Ext.define("DiensteManager.view.servicedetails.metadata.LinkLayerMD", {
    extend: "Ext.window.Window",
    id: "linklayermd-window",
    alias: "servicedetails.metadata.LinkLayerMD",
    height: 400,
    width: 400,
    title: "Layer mit Metadaten koppeln",
    controller: "linklayermd",
    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "Datasets",
                name: "linkDatasetsGrid",
                id: "linkdatasets-grid",
                height: 314,
                autoScroll: true,
                viewConfig: {
                    enableTextSelection: true
                },
                columns: [{
                    header: "Titel",
                    dataIndex: "dataset_name",
                    align: "left",
                    flex: 1
                }],
                columnLines: true,
                selModel: {
                    type: "checkboxmodel",
                    checkOnly: false,
                    mode: "SINGLE"
                }
            }
        ];
        this.buttons = [
            {
                xtype: "checkboxfield",
                id: "linkalllayers",
                boxLabel: "Alle Layer koppeln",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Alle Layer des Dienstes werden mit diesem Datensatz verknüpft."
                },
                margin: "0 0 0 0",
                labelWidth: 0
            },
            {
                text: "Layer koppeln",
                tooltip: "Der gewählte Datensatz wird mit dem Layer verknüpft und das Fenster geschlossen.",
                margin: "0 5 0 10",
                listeners: {
                    click: "onSaveLayerLinks"
                }
            }
        ];

        this.callParent(arguments);
    }
});

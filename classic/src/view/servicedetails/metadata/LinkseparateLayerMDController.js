Ext.define("DiensteManager.view.servicedetails.metadata.LinkseparateLayerMDController", {
    extend: "Ext.app.ViewController",
    alias: "controller.linkseparatelayermd",
    loadingsMask: null,

    onChangeLayerLinks: function () {
        var separatelayeruuid = Ext.getCmp("separatelayeruuid").getValue(),
            reg = separatelayeruuid.match(/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/); // regex Abfrage uuid = valid

        if (reg !== null && separatelayeruuid !== "" && Ext.getCmp("servicesourcecswname2").getValue() !== null) {
            Ext.getCmp("layer_md_koppeln").enable();
        }
        else {
            Ext.getCmp("layer_md_koppeln").disable();
        }
    },

    onChangeManualTitle: function () {
        var dataset_name = Ext.getCmp("manualinserttitle").getSubmitValue();

        if (dataset_name.length > 0) {
            Ext.getCmp("manualinsertsave").setDisabled(false);
        }
    },

    onSaveLayerLinks: function () {
        var layergrid = Ext.getCmp("layers-grid"),
            selectedLayer = layergrid.getSelectionModel().getSelection()[0].data,
            layersourcecsw = Ext.getCmp("servicesourcecswname2").getSubmitValue(),
            linkseparate_service_id = selectedLayer.service_id,
            linkseparate_layer_id = selectedLayer.layer_id,
            metadata_uuid = Ext.getCmp("separatelayeruuid").getSubmitValue(),
            store = Ext.getStore("Datasets");

        store.removeAll();

        Ext.Ajax.request({
            url: "backend/setlayerdatasetmetadata",
            method: "POST",
            jsonData: {
                uuid: metadata_uuid.trim(),
                source_csw: layersourcecsw,
                md_seperate_layer: true,
                linkseparate_layer_id: linkseparate_layer_id,
                linkseparate_service_id: linkseparate_service_id,
                linkalllayers: Ext.getCmp("linkalllayers").getValue()
            },
            success: function (response) {
                var results = Ext.decode(response.responseText);

                if (results.success) {
                    DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(Ext.getCmp("serviceid").getSubmitValue());
                    Ext.getStore("Datasets").reload();
                    Ext.getStore("LayerLinks").reload();
                    Ext.getCmp("layer_md_koppeln").disable();
                    Ext.getCmp("linkseparatelayermd-window").close();
                    Ext.MessageBox.alert("Erfolg", "Layer wurde erfolgreich mit Metadatensatz gekoppelt!");
                }
                else {
                    if (results.csw) {
                        Ext.MessageBox.alert("Fehler", "Einzelne Layer müssen mit der uuid eines Datenatzes gekoppelt werden!");
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", "Mit der uuid konnte keine Kopplung durchgeführt werden!");
                    }
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + layersourcecsw);
            }
        });
    },

    onSaveManualLayerLinks: function () {
        var md_id = Ext.getCmp("manualinsertmdid").getSubmitValue(),
            dataset_name = Ext.getCmp("manualinserttitle").getSubmitValue(),
            descr = Ext.getCmp("manualinsertdescription").getSubmitValue(),
            access_constraints = Ext.getCmp("manualinsertaccessconstraints").getSubmitValue(),
            fees = Ext.getCmp("manualinsertfees").getSubmitValue(),
            cat_opendata = Ext.getCmp("manualinsertcatopendata").getSubmitValue(),
            cat_inspire = Ext.getCmp("manualinsertcatinspire").getSubmitValue(),
            cat_org = Ext.getCmp("manualinsertcatorg").getSubmitValue(),
            keywords = Ext.getCmp("manualinsertkeywords").getSubmitValue(),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            layer_id = Ext.getCmp("layerid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/setLayerManualDatasetMetadata",
            method: "POST",
            jsonData: {
                md_id: md_id,
                dataset_name: dataset_name,
                descr: descr,
                access_constraints: access_constraints,
                fees: fees,
                cat_opendata: cat_opendata,
                cat_inspire: cat_inspire,
                cat_org: cat_org,
                keywords: keywords,
                service_id: service_id,
                layer_id: layer_id,
                linkalllayers: Ext.getCmp("manualinsertlinkalllayers").getValue()
            },
            success: function (response) {
                var results = Ext.decode(response.responseText);

                if (results.success) {
                    DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(Ext.getCmp("serviceid").getSubmitValue());
                    Ext.getStore("LayerLinks").reload();
                    Ext.getCmp("linkseparatelayermd-window").close();
                    Ext.MessageBox.alert("Erfolg", "Layer wurde erfolgreich mit Metadatensatz gekoppelt!");
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Fehler beim Speichern des Datensatzes");
            }
        });
    }
});

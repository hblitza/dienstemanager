Ext.define("DiensteManager.view.servicedetails.metadata.LinkServiceMD", {
    extend: "Ext.window.Window",

    alias: "servicedetails.metadata.linkservicemd",

    id: "linkservicemd-window",
    height: 400,
    width: 400,
    title: "Dienst Metadatensatz Wählen",
    resizable: false,
    controller: "linkservicemd",

    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "ServiceMDSets",
                id: "linkservicemd-grid",
                height: 314,
                autoScroll: true,
                viewConfig: {
                    enableTextSelection: true
                },
                columns: [{
                    header: "Titel",
                    dataIndex: "title",
                    align: "left",
                    flex: 1
                }],
                columnLines: true,
                selModel: {
                    type: "checkboxmodel",
                    mode: "SINGLE"
                }
            }
        ];
        this.buttons = [{
            text: "OK",
            tooltip: "Die UUID des gewählten Metadatensatzes wird eingefügt.",
            listeners: {
                click: "onSaveServiceMDLink"
            }
        }];

        this.callParent(arguments);
    }

});

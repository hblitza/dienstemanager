Ext.define("DiensteManager.view.servicedetails.metadata.LinkseparateLayerMD", {
    extend: "Ext.window.Window",
    id: "linkseparatelayermd-window",
    alias: "servicedetails.metadata.LinkseparateLayerMD",
    height: 600,
    width: 700,
    title: "Layer mit Metadaten koppeln",
    controller: "linkseparatelayermd",
    items: [
        {
            xtype: "tabpanel",
            id: "linkseparatelayermd-tabpanel",
            activeTab: 0,
            defaults: {
                layout: "anchor"
            },
            items: [
                {
                    xtype: "panel",
                    id: "linkseparatelayermd-externaltab",
                    title: "Aus Metadatenkatalog koppeln",
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: "textfield",
                            id: "separatelayeruuid",
                            fieldLabel: "UUID",
                            width: 670,
                            labelWidth: 100,
                            allowBlank: false,
                            style: "font-weight: bold; color: #003168;",
                            listeners: {
                                change: "onChangeLayerLinks"
                            }
                        },
                        {
                            xtype: "combo",
                            fieldLabel: "Quellkatalog",
                            store: "ConfigMetadataCatalogs",
                            displayField: "name",
                            allowBlank: false,
                            id: "servicesourcecswname2",
                            editable: false,
                            margin: "0 10 0 0",
                            labelWidth: 100,
                            width: 260,
                            listeners: {
                                change: "onChangeLayerLinks"
                            }
                        }
                    ],
                    buttons: [
                        {
                            xtype: "checkboxfield",
                            id: "linkalllayers",
                            boxLabel: "Alle Layer koppeln",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Alle Layer des Dienstes werden mit diesem Datensatz verknüpft."
                            },
                            margin: "0 0 0 0",
                            labelWidth: 0
                        },
                        {
                            text: "Layer koppeln",
                            tooltip: "Der gewählte Datensatz wird mit dem Layer verknüpft und das Fenster geschlossen.",
                            margin: "0 5 0 10",
                            id: "layer_md_koppeln",
                            disabled: true,
                            listeners: {
                                click: "onSaveLayerLinks"
                            }
                        }
                    ]
                },
                {
                    xtype: "panel",
                    id: "linkseparatelayermd-manualtab",
                    title: "Manuelle Eingabe",
                    bodyPadding: 10,
                    height: 505,
                    autoScroll: true,
                    items: [
                        {
                            xtype: "textfield",
                            id: "manualinsertmdid",
                            hidden: true
                        },
                        {
                            xtype: "textfield",
                            id: "manualinserttitle",
                            fieldLabel: "Titel",
                            width: 670,
                            labelWidth: 170,
                            allowBlank: false,
                            listeners: {
                                change: "onChangeManualTitle"
                            }
                        },
                        {
                            xtype: "fieldset",
                            title: "Beschreibung",
                            padding: "0 5 0 5",
                            cls: "prio-fieldset",
                            collapsible: true,
                            collapsed: true,
                            items: [{
                                xtype: "textareafield",
                                grow: false,
                                id: "manualinsertdescription",
                                height: 120,
                                width: "100%"
                            }]
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertfees",
                            fieldLabel: "Nutzungsbedingungen",
                            width: 670,
                            labelWidth: 170
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertaccessconstraints",
                            fieldLabel: "Zugriffsbeschränkungen",
                            width: 670,
                            labelWidth: 170
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertcatopendata",
                            fieldLabel: "Kategorie OpenData",
                            width: 670,
                            labelWidth: 170
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertcatinspire",
                            fieldLabel: "Kategorie INSPIRE",
                            width: 670,
                            labelWidth: 170
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertcatorg",
                            fieldLabel: "Kategorie Behörde",
                            width: 670,
                            labelWidth: 170
                        },
                        {
                            xtype: "textfield",
                            id: "manualinsertkeywords",
                            fieldLabel: "Stichworte",
                            width: 670,
                            labelWidth: 170
                        }
                    ],
                    buttons: [
                        {
                            xtype: "checkboxfield",
                            id: "manualinsertlinkalllayers",
                            boxLabel: "Alle Layer koppeln",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Alle Layer des Dienstes werden mit diesem Datensatz verknüpft."
                            },
                            margin: "0 0 0 0",
                            labelWidth: 0
                        },
                        {
                            text: "Layer koppeln",
                            tooltip: "Der gewählte Datensatz wird mit dem Layer verknüpft und das Fenster geschlossen.",
                            margin: "0 5 0 10",
                            id: "manualinsertsave",
                            disabled: true,
                            listeners: {
                                click: "onSaveManualLayerLinks"
                            }
                        }
                    ]
                }
            ]
        }
    ]
});

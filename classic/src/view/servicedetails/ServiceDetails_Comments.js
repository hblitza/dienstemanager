Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Comments", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-comments",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*"
    ],

    controller: "servicedetails_comments",

    bodyPadding: "5 5 5 5",
    items: [
        {
            xtype: "grid",
            cls: "commentsCls",
            store: "Comments",
            name: "commentsGrid",
            autoScroll: true,
            maxHeight: 677,
            height: Ext.Element.getViewportHeight() - 90,
            maxWidth: 1570,
            width: Ext.Element.getViewportWidth(),
            id: "comments-grid",
            hideHeaders: true,
            border: false,
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            selModel: "rowmodel",
            columns: [
                {
                    xtype: "gridcolumn",
                    align: "left",
                    renderer: function (value, metaData, record) {
                        var comment = "<div><div class=\"commentsAuthorCls\">" + record.data.author + " - " + record.data.post_date + "</div><div>" + record.data.text + "</div></div>";

                        return comment;
                    },
                    dataIndex: "text",
                    cellWrap: true,
                    flex: 1
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                items: [{
                    id: "addcommentbutton",
                    disabled: window.read_only,
                    text: "Hinzufügen",
                    handler: "onAddComment"
                }, {
                    id: "editcommentbutton",
                    disabled: window.read_only,
                    text: "Bearbeiten",
                    handler: "onEditComment"
                }, {
                    id: "deletecommentbutton",
                    disabled: window.read_only,
                    text: "Löschen",
                    handler: "onDeleteComment"
                }]
            }]
        }
    ]
});

Ext.define("DiensteManager.view.servicedetails.gfieditor.GFIEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.gfieditor",

    onWindowClose: function (gfiEditor) {
        var selectedLayer = Ext.getCmp("layers-grid").getSelectionModel().getSelection()[0].data,
            gfiConfigStore = Ext.getStore("GFIConfigs");

        gfiConfigStore.removeAll();

        if (this.isJsonString(selectedLayer.gfi_config)) {
            var gfiConfigJson = JSON.parse(selectedLayer.gfi_config),
                index = 0;

            for (var attr_name in gfiConfigJson) {
                var map_name = gfiConfigJson[attr_name];

                if (map_name.constructor === {}.constructor) {
                    map_name = JSON.stringify(gfiConfigJson[attr_name]);
                }

                var rec = new DiensteManager.model.GFIConfig({
                    attr_name: attr_name,
                    map_name: map_name
                });

                gfiConfigStore.insert(index, rec);
                index++;
            }
        }
        gfiEditor.hide();
    },

    onAddRow: function () {
        var rec = new DiensteManager.model.GFIConfig({
                attr_name: "",
                map_name: ""
            }),
            count = Ext.getStore("GFIConfigs").count();

        Ext.getStore("GFIConfigs").insert(count, rec);
        Ext.getCmp("gfieditor-grid").findPlugin("cellediting").startEdit(rec, 0);
    },

    onGetAttributes: function () {
        var type = Ext.getCmp("servicetype").getSubmitValue(),
            url_int = Ext.getCmp("serviceurlint").getSubmitValue(),
            layername = Ext.getCmp("layername").getSubmitValue(),
            version = Ext.getCmp("serviceversion").getSubmitValue(),
            externalService = Ext.getCmp("externalServiceCheckBox").getValue();

        if (type === "WMS" || type === "WFS") {
            Ext.getCmp("getgfiattributes").setDisabled(true);

            if (url_int.slice(-1) === "&") {
                url_int = url_int.slice(0, -1);
            }

            Ext.Ajax.request({
                url: "backend/getgfiattributes",
                method: "POST",
                jsonData: {
                    url: url_int,
                    version: version,
                    type: type,
                    layername: layername,
                    externalService: externalService
                },
                success: function (response) {
                    var results = Ext.decode(response.responseText);

                    if (results.length === 0) {
                        Ext.MessageBox.alert("Hinweis", "Keine Attribute abrufbar!");
                    }
                    else {
                        Ext.getStore("GFIConfigs").removeAll();
                        Ext.getStore("GFIConfigs").add(results);
                    }

                    Ext.getCmp("getgfiattributes").setDisabled(false);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
                    Ext.getCmp("getgfiattributes").setDisabled(false);
                }
            });
        }
        else {
            Ext.MessageBox.alert("Achtung", "GFI Abfrage nur mit deegree Diensten möglich!");
        }
    },

    onMoveDown: function (grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex);

        if (!record) {
            return;
        }

        var index = rowIndex;

        if (index < grid.getStore().getCount()) {
            index++;
            grid.getStore().removeAt(rowIndex);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().select(index, true);
        }
    },

    onMoveUp: function (grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex);

        if (!record) {
            return;
        }

        var index = rowIndex;

        if (index > 0) {
            index--;
            grid.getStore().removeAt(rowIndex);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().select(index, true);
        }
    },

    onSaveAttributes: function () {
        var gfi_config,
            gfi_useforalllayers = Ext.getCmp("gfi_useforalllayers").getSubmitValue();

        if (Ext.getStore("GFIConfigs").count() > 0) {
            var gfi_config_json = {};

            Ext.getStore("GFIConfigs").each(function (record) {
                if (record.data.attr_name.length > 0 && record.data.map_name.length > 0) {
                    if (record.data.map_name.trim().startsWith("{")) {
                        gfi_config_json[record.data.attr_name] = JSON.parse(record.data.map_name);
                    }
                    else {
                        gfi_config_json[record.data.attr_name] = record.data.map_name;
                    }
                }
            });

            gfi_config = JSON.stringify(gfi_config_json);

            Ext.Ajax.request({
                url: "backend/updategficonfig",
                method: "POST",
                jsonData: {
                    layer_id: Ext.getCmp("layerid").getSubmitValue(),
                    service_id: Ext.getCmp("serviceid").getSubmitValue(),
                    last_edited_by: window.auth_user,
                    last_edit_date: moment().format("YYYY-MM-DD HH:mm"),
                    gfi_config: gfi_config,
                    gfi_useforalllayers: gfi_useforalllayers
                },
                success: function () {
                    var layergrid = Ext.getCmp("layers-grid"),
                        scroll_x = layergrid.getView().getScrollX(),
                        scroll_y = layergrid.getView().getScrollY(),
                        selectedLayer = layergrid.getSelectionModel().getSelection()[0].data;

                    Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                    Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));

                    Ext.getStore("Layers").reload({
                        params: {service_id: selectedLayer.service_id},
                        callback: function (records) {
                            layergrid.getView().setScrollX(scroll_x);
                            layergrid.getView().setScrollY(scroll_y);

                            if (records.length > 0) {
                                for (var i = 0; i < records.length; i++) {
                                    if (records[i].data.layer_id === selectedLayer.layer_id) {
                                        var row = layergrid.store.indexOf(records[i]);

                                        layergrid.getSelectionModel().select(row);
                                        layergrid.ensureVisible(row);
                                        Ext.getCmp("gfieditor-window").close();
                                        break;
                                    }
                                }
                            }
                        }
                    });
                },
                failure: function (response) {
                    Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onDeleteRow: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();
    },

    isJsonString: function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    }
});

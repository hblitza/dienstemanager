Ext.define("DiensteManager.view.servicedetails.gfieditor.GFIEditor", {
    extend: "Ext.window.Window",
    id: "gfieditor-window",
    alias: "servicedetails.gfieditor.GFIEditor",
    height: 400,
    width: 750,
    title: "GFI Attribute Konfigurieren",
    resizable: false,
    requires: [
        "Ext.selection.CellModel"
    ],
    closeAction: "hide",
    controller: "gfieditor",
    listeners: {
        close: {
            fn: "onWindowClose"
        }
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "GFIConfigs",
                id: "gfieditor-grid",
                height: 355,
                autoScroll: true,
                columnLines: true,
                plugins: {
                    ptype: "cellediting",
                    clicksToEdit: 1
                },
                selModel: {
                    type: "cellmodel"
                },
                viewConfig: {
                    enableTextSelection: true
                },
                columns: [{
                    header: "Attributname",
                    dataIndex: "attr_name",
                    align: "left",
                    flex: 1,
                    sortable: false,
                    editor: {
                        allowBlank: false
                    }
                }, {
                    header: "Anzeigename",
                    dataIndex: "map_name",
                    flex: 1,
                    sortable: false,
                    align: "left",
                    editor: {
                        allowBlank: false
                    }
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onMoveUp",
                    hidden: window.read_only,
                    items: [{
                        icon: "resources/images/up.png"
                    }]
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onMoveDown",
                    hidden: window.read_only,
                    items: [{
                        icon: "resources/images/down.png"
                    }]
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteRow",
                    hidden: window.read_only,
                    items: [{
                        icon: "resources/images/drop-no.png"
                    }]
                }],
                tbar: [{
                    text: "Zeile hinzufügen",
                    tooltip: "Fügt eine leere Zeile am Ende der Tabelle ein",
                    hidden: window.read_only,
                    handler: "onAddRow"
                }, {
                    text: "Attribute abfragen",
                    id: "getgfiattributes",
                    tooltip: "Funktioniert nur mit WMS, wenn der Request GetFeatureInfoSchema unterstützt wird!",
                    hidden: window.read_only,
                    handler: "onGetAttributes"
                }, {
                    xtype: "checkboxfield",
                    id: "gfi_useforalllayers",
                    fieldLabel: "Konfiguration für alle Layer übernehmen",
                    hidden: window.read_only,
                    labelWidth: 270
                }, {
                    text: "speichern",
                    id: "gfi_attributessave",
                    tooltip: "speichert die Attributzuordnung",
                    hidden: window.read_only,
                    handler: "onSaveAttributes"
                }]
            }
        ];

        this.callParent(arguments);
    }

});

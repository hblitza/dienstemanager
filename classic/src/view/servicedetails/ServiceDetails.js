Ext.define("DiensteManager.view.servicedetails.ServiceDetails", {
    extend: "Ext.window.Window",

    alias: "servicedetails.ServiceDetails",

    requires: [
        "Ext.window.*",
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.tab.*",
        "Ext.layout.container.Border",
        "Ext.panel.Panel",
        "DiensteManager.view.servicedetails.ServiceDetails_Stats"
    ],

    id: "servicedetails-window",
    maxHeight: 810,
    height: Ext.Element.getViewportHeight(),
    maxWidth: 1570,
    width: Ext.Element.getViewportWidth(),
    resizable: false,
    title: "Dienstdetails",
    header: true,
    controller: "dienst_details",

    initComponent: function () {
        var tagfield = Ext.create("Ext.form.field.Tag", {
            id: "servicecontacts",
            autoEl: {
                tag: "div",
                "data-qtip": "Auswahl über Dropddown"
            },
            store: "Contacts",
            displayField: "name",
            valueField: "name",
            forceSelection: false,
            filterPickList: true,
            queryMode: "local",
            listeners: {
                change: "enableSaveButton"
            }
        });

        tagfield.selectionModel.on("selectionchange", function (model, selected) {
            var contactDetailsWindow = Ext.getCmp("contactdetails-window"),
                data = null;

            if (selected[0] !== undefined) {
                data = selected[0].data;
                if (contactDetailsWindow === undefined) {
                    contactDetailsWindow = Ext.create("contacts.contactdetails");
                    contactDetailsWindow.show();
                }
                else {
                    contactDetailsWindow.show();
                }
                Ext.getCmp("contactdetailsgivenname").setValue(data.given_name);
                Ext.getCmp("contactdetailssurname").setValue(data.surname);
                Ext.getCmp("contactdetailscompany").setValue(data.company);
                Ext.getCmp("contactdetailsemail").setValue(data.email);
                Ext.getCmp("contactdetailstel").setValue(data.tel);
                Ext.getCmp("contactdetailsadaccount").setValue(data.ad_account);
            }
            else {
                if (contactDetailsWindow) {
                    contactDetailsWindow.close();
                }
            }
        });

        var items_west = [{
            fieldLabel: "Dienst-ID",
            id: "serviceid",
            name: "service_id",
            readOnly: true,
            labelWidth: 160,
            width: 360
        }, {
            xtype: "combo",
            fieldLabel: "Prüfstatus",
            store: ["Geprüft", "Ungeprüft", "Hinweis", "Fehler"],
            allowBlank: false,
            id: "servicecheckedstatus",
            name: "service_checked_status",
            labelWidth: 160,
            width: 360
        }, {
            fieldLabel: "Dienst-Titel",
            id: "servicetitle",
            name: "title",
            allowBlank: false,
            labelWidth: 160,
            width: "100%",
            fieldStyle: "font-weight: bold;"
        },
        {
            xtype: "combo",
            fieldLabel: "Ersteller",
            store: DiensteManager.Config.getBearbeiter(),
            id: "serviceeditor",
            name: "editor",
            labelWidth: 160,
            width: 360
        },
        {
            xtype: "fieldcontainer",
            layout: "hbox",
            labelWidth: 160,
            fieldLabel: "Zuletzt bearbeitet von",
            width: "100%",
            items: [
                {
                    xtype: "textfield",
                    id: "servicelasteditedby",
                    name: "last_edited_by",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Name des Bearbeiters, der zuletzt den Dienst editiert hat. Wird automatisch ausgefüllt"
                    },
                    readOnly: true,
                    labelWidth: 0,
                    width: 195
                }, {
                    xtype: "datefield",
                    fieldLabel: "am",
                    id: "servicelasteditdate",
                    name: "last_edit_date",
                    allowBlank: false,
                    readOnly: true,
                    format: "Y-m-d H:i",
                    margin: "0 0 0 20",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Zeitpunkt, zu dem der Diensteintrag zuletzt editiert wurden. Wird automatisch ausgefüllt"
                    },
                    labelWidth: 30,
                    width: 200
                }
            ]
        },
        {
            xtype: "combo",
            fieldLabel: "Verantwortliche Stelle",
            id: "serviceresponsibleparty",
            store: DiensteManager.Config.getResponsibleParty(),
            allowBlank: false,
            labelWidth: 160,
            width: 360
        }
        ];

        if (DiensteManager.Config.getFreigabenModul()) {
            items_west.push(
                {
                    xtype: "fieldset",
                    title: "Freigabeerklärung",
                    padding: "0 5 0 5",
                    cls: "prio-fieldset",
                    collapsible: true,
                    collapsed: true,
                    items: [{
                        xtype: "addin_freigaben"
                    }]
                }
            );
        }

        if (DiensteManager.Config.getMetadatenModul()) {
            items_west.push(
                {
                    xtype: "fieldset",
                    title: "Metadatenkopplung",
                    padding: "0 5 0 5",
                    cls: "prio-fieldset",
                    collapsible: true,
                    collapsed: true,
                    items: [{
                        xtype: "addin_metadaten"
                    }]
                }
            );
        }

        items_west.push(
            {
                xtype: "fieldset",
                title: "Beschreibung",
                padding: "0 5 0 5",
                cls: "prio-fieldset",
                collapsible: true,
                collapsed: true,
                items: [{
                    xtype: "textareafield",
                    grow: false,
                    id: "servicedescription",
                    height: 120,
                    width: "100%",
                    listeners: {
                        change: "enableSaveButton"
                    }
                }]
            },
            {
                xtype: "fieldset",
                title: "Nutzungsbedingungen",
                padding: "0 5 0 5",
                cls: "prio-fieldset",
                collapsible: true,
                collapsed: true,
                items: [{
                    xtype: "textareafield",
                    grow: false,
                    id: "servicefees",
                    height: 80,
                    width: "100%",
                    listeners: {
                        change: "enableSaveButton"
                    }
                }]
            },
            {
                xtype: "fieldset",
                title: "Zugriffsbeschränkungen",
                padding: "0 5 0 5",
                cls: "prio-fieldset",
                collapsible: true,
                collapsed: true,
                items: [{
                    xtype: "textareafield",
                    grow: false,
                    id: "serviceaccessconstraints",
                    height: 80,
                    width: "100%",
                    listeners: {
                        change: "enableSaveButton"
                    }
                }]
            },
            {
                xtype: "fieldset",
                title: "Schlagworte",
                padding: "0 5 0 5",
                cls: "prio-fieldset",
                collapsible: true,
                collapsed: true,
                items: [{
                    xtype: "tagfield",
                    id: "servicekeywords",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Auswahl über Dropddown, oder durch Eingabe, gefolgt von \",\""
                    },
                    listeners: {
                        beforeselect: function (combo, record) {
                            var keyword_array = Ext.getCmp("servicekeywords").getValueRecords(),
                                is_mrh = false,
                                is_only_mrh = false;

                            for (var i = 0; i < keyword_array.length; i++) {
                                if (keyword_array[i].data.text === "mrh") {
                                    is_mrh = true;
                                }
                                if (keyword_array[i].data.text === "only_mrh") {
                                    is_only_mrh = true;
                                }
                            }
                            if (record.data.text === "mrh" && is_only_mrh) {
                                return false;
                            }
                            if (record.data.text === "only_mrh" && is_mrh) {
                                return false;
                            }
                        },
                        change: "enableSaveButton"
                    },
                    store: "Keywords",
                    displayField: "text",
                    valueField: "text",
                    forceSelection: false,
                    queryMode: "local",
                    createNewOnEnter: true,
                    filterPickList: true
                }]
            },
            {
                xtype: "fieldset",
                title: "Ansprechpartner",
                padding: "0 5 0 5",
                cls: "prio-fieldset",
                collapsible: true,
                collapsed: true,
                items: [tagfield]
            }
        );

        items_west.push(
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: "100%",
                defaults: {
                    listeners: {
                        change: "enableSaveButton"
                    }
                },
                items: [
                    {
                        xtype: "checkboxfield",
                        name: "inspire_relevant",
                        id: "serviceinspirerelevant",
                        fieldLabel: "INSPIRE-identifiziert",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Sind die Daten INSPIRE relevant? Wird aus den Metadaten ausgelesen."
                        },
                        labelWidth: 160
                    }, {
                        xtype: "checkboxfield",
                        name: "cached",
                        id: "servicecached",
                        fieldLabel: "Cache",
                        labelWidth: 80,
                        margin: "0 0 0 140",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Gibt es einen Cache für Layer des Dienstes?"
                        }
                    }
                ]
            }
        );

        var items_east = [
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                items: [
                    {
                        xtype: "combo",
                        fieldLabel: "Status",
                        store: ["In Bearbeitung", "Produktiv", "Test"],
                        allowBlank: false,
                        name: "status",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Angabe, ob ein Dienst fertig ist und produktiv eingestezt werden kann oder nicht."
                        },
                        id: "servicestatus",
                        labelWidth: 160,
                        width: 360,
                        listeners: {
                            beforeRender: function (e) {
                                var qs = Ext.getCmp("serviceqa").getValue();

                                if (e.rawValue === "Produktiv") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#A8D44D 50%,#A8D44D 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#A8D44D;");
                                    }
                                }
                                else if (e.rawValue === "In Bearbeitung") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FAD801 50%,#FAD801 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#FAD801;");
                                    }
                                }
                                else if (e.rawValue === "Test") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FF234C 50%,#FF234C 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#FF234C;");
                                    }
                                }
                            }, change: function (e) {
                                var qs = Ext.getCmp("serviceqa").getValue();

                                if (e.rawValue === "Produktiv") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#A8D44D 50%,#A8D44D 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#A8D44D;");
                                    }
                                }
                                else if (e.rawValue === "In Bearbeitung") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FAD801 50%,#FAD801 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#FAD801;");
                                    }
                                }
                                else if (e.rawValue === "Test") {
                                    if (qs) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FF234C 50%,#FF234C 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background-color:#FF234C;");
                                    }
                                }
                                if (!window.read_only) {
                                    Ext.getCmp("save_service_details_button").setDisabled(false);
                                }

                            }
                        }
                    },
                    {
                        xtype: "checkboxfield",
                        name: "qa",
                        id: "serviceqa",
                        boxLabel: "Befindet sich in der QA Prüfung",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Wenn angehakt, wird automatisiert eine Benachrichtigungsmail verschickt"
                        },
                        labelWidth: 100,
                        margin: "0 0 0 20",
                        listeners: {
                            change: "enableSaveButton"
                        }
                    }
                ]
            }, {
                xtype: "combo",
                fieldLabel: "Typ",
                store: DiensteManager.Config.getDienstTypen(),
                allowBlank: false,
                id: "servicetype",
                name: "type",
                labelWidth: 160,
                width: 360,
                listeners: {
                    change: "onFieldValueChanged"
                }
            }, {
                xtype: "combo",
                fieldLabel: "Version",
                store: [""],
                allowBlank: false,
                id: "serviceversion",
                name: "version",
                labelWidth: 160,
                width: 360
            }, {
                xtype: "fieldcontainer",
                layout: "hbox",
                defaults: {
                    listeners: {
                        change: "enableSaveButton"
                    }
                },
                items: [
                    {
                        xtype: "combo",
                        fieldLabel: "Software",
                        store: DiensteManager.Config.getSoftware(),
                        allowBlank: false,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Name der Software, mit der der Dienst bereit gestellt wird"
                        },
                        id: "servicesoftware",
                        name: "software",
                        labelWidth: 160,
                        width: 360
                    }, {
                        xtype: "checkboxfield",
                        name: "reverseProxy",
                        id: "reverseProxyCheckBox",
                        boxLabel: "Reverse Proxy",
                        hidden: true,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Als ReverseProxy markieren"
                        },
                        listeners: {
                            change: "onReverseProxyCheckBoxChanged"
                        },
                        labelWidth: 100,
                        margin: "0 0 0 20"
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                items: [
                    {
                        xtype: "combo",
                        fieldLabel: "Server",
                        store: DiensteManager.Config.getServerListe(),
                        allowBlank: false,
                        id: "serviceserver",
                        name: "server",
                        labelWidth: 160,
                        width: 360,
                        listeners: {
                            change: "onFieldValueChanged"
                        }
                    }, {
                        xtype: "checkboxfield",
                        name: "externalService",
                        id: "externalServiceCheckBox",
                        boxLabel: "Externer Dienst",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Als externen Dienst markieren"
                        },
                        listeners: {
                            change: "onExternalServiceChanged"
                        },
                        labelWidth: 100,
                        margin: "0 0 0 20"
                    }
                ]
            }, {
                fieldLabel: "Ordner / Webapp",
                id: "servicefolder",
                name: "folder",
                allowBlank: false,
                autoEl: {
                    tag: "div",
                    "data-qtip": "Name des Ordners auf dem ArcGis Server, oder Name der deegree-WebApp"
                },
                width: "100%",
                labelWidth: 160,
                listeners: {
                    change: "onFieldValueChanged"
                }
            }, {
                fieldLabel: "Dienstname / Endpoint",
                id: "servicename",
                name: "service_name",
                allowBlank: false,
                autoEl: {
                    tag: "div",
                    "data-qtip": "Dienstname bzw. Name des Endpoints"
                },
                width: "100%",
                labelWidth: 160,
                listeners: {
                    change: "onFieldValueChanged"
                }
            }, {
                fieldLabel: "interne URL",
                id: "serviceurlint",
                labelWidth: 160,
                width: "100%",
                name: "url_int"
            }, {
                fieldLabel: "externe URL",
                id: "serviceurlext",
                labelWidth: 160,
                width: "100%",
                name: "url_ext"
            }
        ];

        if (DiensteManager.Config.getSecurityModul()) {
            items_east.push(
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    items: [{
                        xtype: "combo",
                        fieldLabel: "Absicherung",
                        store: DiensteManager.Config.getSecurityType(),
                        value: "keine",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Auswahl der Absicherung"
                        },
                        id: "servicesecuritytype",
                        name: "security_type",
                        labelWidth: 160,
                        width: 360,
                        listeners: {
                            change: "onSecurityTypeChange"
                        }
                    },
                    {
                        xtype: "button",
                        id: "onOpenSetAllowedGroups",
                        hidden: true,
                        margin: "0 0 0 5",
                        tooltip: "AD Gruppen berechtigen",
                        text: "AD Gruppen berechtigen",
                        listeners: {
                            click: "onOpenSetAllowedGroups"
                        }
                    },
                    {
                        xtype: "button",
                        id: "basicAuthConfigurationButton",
                        hidden: true,
                        margin: "0 0 0 5",
                        tooltip: "Basic Auth konfigurieren",
                        text: "Basic Auth konfigurieren",
                        listeners: {
                            click: "onBasicAuthConfiguration"
                        }
                    }]
                },
                {
                    fieldLabel: "Abgesicherte URL",
                    id: "serviceurlsec",
                    labelWidth: 160,
                    width: "100%",
                    name: "url_sec"
                }
            );
        }

        items_east.push(
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                defaultType: "checkboxfield",
                labelWidth: 160,
                margin: "5 0 5 165",
                items: [
                    {
                        xtype: "checkboxfield",
                        name: "only_intranet",
                        id: "serviceonlyintranet",
                        boxLabel: "Nur für Intranet",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Dienst ist ausschließlich aus dem Intranet abrufbar"
                        },
                        listeners: {
                            change: "onOnlyIntranetChanged"
                        }
                    },
                    {
                        xtype: "checkboxfield",
                        name: "only_internet",
                        id: "serviceonlyinternet",
                        boxLabel: "Nur für Internet",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Dienst ist ausschließlich aus dem Internet abrufbar"
                        },
                        labelWidth: 100,
                        margin: "0 0 0 20",
                        listeners: {
                            change: "onOnlyInternetChanged"
                        }
                    }
                ]
            },
            {
                fieldLabel: "Datenquelle",
                id: "servicedatasource",
                tooltip: "Ablageort der FGDB, oder DB-Connect-String",
                labelWidth: 160,
                width: "100%",
                name: "datasource"
            },
            {
                fieldLabel: "Kartendefinition",
                id: "servicemappingdescription",
                labelWidth: 160,
                width: "100%",
                name: "mapping_description"
            });

        var tab_items = [{
            xtype: "form",
            title: "Details",
            id: "servicedetails-form",
            bodyPadding: "5 10 0 10",
            maxHeight: 720,
            height: Ext.Element.getViewportHeight() - 90,
            maxWidth: 1570,
            width: Ext.Element.getViewportWidth(),
            layout: "column",
            autoScroll: true,
            defaults: {
                xtype: "container",
                defaultType: "textfield",
                style: "width: 50%",
                bodyPadding: 10,
                defaults: {
                    listeners: {
                        change: "enableSaveButton"
                    }
                }
            },
            items: [{
                minWidth: 560,
                padding: "0 30 10 0",
                items: items_west
            },
            {
                minWidth: 560,
                items: items_east
            }
            ],
            buttons: [
                {
                    text: "Speichern",
                    id: "save_service_details_button",
                    margin: "0 7 0 0",
                    listeners: {
                        click: "onSaveServiceDetails"
                    }
                }
            ]

        }, {
            id: "servicedetails-layers",
            xtype: "servicedetails-layer",
            title: "Layer / FeatureTypes",
            alias: "servicedetails.ServiceDetails_Layer"
        }, {
            id: "servicedetails-dataupdatestatus",
            xtype: "servicedetails-dataupdatestatus",
            title: "Datenaktualität"
        }, {
            id: "servicedetails-comments",
            xtype: "servicedetails-comments",
            title: "Kommentare"
        }
        ];

        if (DiensteManager.Config.getAnmerkungenModul()) {
            tab_items.push({
                id: "servicedetails-testresults",
                xtype: "servicedetails-testresults",
                title: "Prüfergebnisse"
            });
        }

        if (DiensteManager.Config.getZugriffsstatistikModul()) {
            tab_items.push({
                id: "servicedetails-stats",
                xtype: "servicedetails-stats",
                title: "Statistik"
            });
        }

        if (DiensteManager.Config.getJiraModul()) {
            tab_items.push({
                id: "servicedetails-jira",
                xtype: "servicedetails-jira",
                title: "Jira Tickets"
            });
        }

        this.items = [{
            xtype: "tabpanel",
            id: "servicedetails-tabpanel",
            activeTab: 0,
            defaults: {
                layout: "anchor"
            },
            items: tab_items
        }
        ];

        this.callParent(arguments);
    }
});

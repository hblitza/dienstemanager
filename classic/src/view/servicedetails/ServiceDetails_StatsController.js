Ext.define("DiensteManager.view.servicedetails.ServiceDetails_StatsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_stats",

    onBarTooltipRender: function (tooltip, record, item) {
        var fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field),
            quelle = item.series.getTitle()[fieldIndex];

        tooltip.setHtml("Zugriffe - " + quelle + " " + record.get("month") + ": " + record.get(item.field));
    },

    onLabelRender: function (text, sprite, config, rendererData, index) {
        var store = rendererData.store,
            storeItems = store.getData().items,
            record = storeItems[index];

        return "gesamt: " + record.data.visits_total;
    },

    onSaveVisitsStats: function () {
        var serviceTitle = Ext.getCmp("servicetitle").getSubmitValue(),
            dt = Ext.getCmp("serviceVisitsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");

        if (dt) {
            var download_link = document.createElement("a");

            download_link.href = dt;
            download_link.target = "_blank";
            download_link.download = "Zugriffe " + serviceTitle + ".png";
            download_link.click();
            download_link.remove();
        }
    }
});

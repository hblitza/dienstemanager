Ext.define("DiensteManager.view.servicedetails.ServiceDetails_LayerController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_layer",

    id: "servicedetails_layercontroller",

    onSelectLayer: function (view, record) {
        this.onLayerClick(view, undefined, undefined, record);
    },

    onLayerClick: function (view, td, cellIndex, record) {
        var selectedLayer = record.data,
            gfiConfigStore = Ext.getStore("GFIConfigs"),
            service_type = Ext.getCmp("servicetype").getSubmitValue(),
            software = Ext.getCmp("servicesoftware").getSubmitValue();

        gfiConfigStore.removeAll();

        if (DiensteManager.Config.getMetadatenModul()) {
            var service_md_id = Ext.getCmp("metadata_uuid").getSubmitValue();

            if (service_md_id !== "" && !window.read_only) {
                Ext.getCmp("linkdatasetbutton").setDisabled(false);
            }
            Ext.getStore("LayerLinks").load({
                params: {layer_id: selectedLayer.layer_id}
            });
        }

        if (DiensteManager.Config.getGekoppelteportaleModul()) {
            Ext.getStore("Portals").load({
                params: {layer_id: selectedLayer.layer_id}
            });
        }

        Ext.getCmp("layer-details-form").getForm().setValues({
            layerid: selectedLayer.layer_id,
            layername: selectedLayer.layer_name,
            layertitle: selectedLayer.title,
            layercategories: selectedLayer.additional_categories,
            layeralttitle: selectedLayer.title_alt,
            layergfiformat: selectedLayer.gfi_format,
            layergfitheme: selectedLayer.gfi_theme,
            layergficomplex: selectedLayer.gfi_complex,
            layergfiasnewwindow: selectedLayer.gfi_asnewwindow,
            layergfiwindowspecs: selectedLayer.gfi_windowspecs,
            layerattribution: selectedLayer.layerattribution,
            layergutter: selectedLayer.gutter,
            layertransparency: selectedLayer.transparency,
            layertilesize: selectedLayer.tilesize,
            layerminscale: selectedLayer.scale_min,
            layermaxscale: selectedLayer.scale_max,
            layerfeaturecount: selectedLayer.featurecount,
            layerlegendurlinternet: selectedLayer.legend_url_internet,
            layerlegendurlintranet: selectedLayer.legend_url_intranet,
            layerhittolerance: selectedLayer.hittolerance,
            layerlegendtype: selectedLayer.legend_type,
            layerlegendpath: selectedLayer.legend_path,
            layersingletile: selectedLayer.singletile,
            layertransparent: selectedLayer.transparent,
            layeroutputformat: selectedLayer.output_format,
            layerrequestvertexnormals: selectedLayer.request_vertex_normals,
            layermaximumscreenspaceerror: selectedLayer.maximum_screen_space_error,
            layergfithemeparams: selectedLayer.gfi_theme_params,
            layerhidelevels: selectedLayer.hidelevels,
            layerprojection: selectedLayer.projection,
            layerminzoom: selectedLayer.minzoom,
            layerresolution: selectedLayer.resolution,
            layerterrainurl: selectedLayer.terrainurl,
            layernamespace: selectedLayer.namespace,
            layerdatetimecolumn: selectedLayer.datetime_column,
            layerurlisvisible: selectedLayer.service_url_is_visible,
            layerextent: selectedLayer.extent,
            layerorigin: selectedLayer.origin,
            layerresolutions: selectedLayer.resolutions,
            layervtstyles: selectedLayer.vtstyles,
            layervisibility: selectedLayer.visibility,
            layernotsupportedfor3d: selectedLayer.notsupportedfor3d,
            layergfibeautifykeys: selectedLayer.gfi_beautifykeys
        });

        var index = 0;

        if (selectedLayer.gfi_config === "ignore") {
            Ext.getCmp("layergfidisable").setValue(true);
            Ext.getCmp("editGFIConfig").setDisabled(true);
        }
        else if (this.isJsonString(selectedLayer.gfi_config)) {
            var gfiConfigJson = JSON.parse(selectedLayer.gfi_config);

            for (var attr_name in gfiConfigJson) {
                var map_name = gfiConfigJson[attr_name];

                if (map_name.constructor === {}.constructor) {
                    map_name = JSON.stringify(gfiConfigJson[attr_name]);
                }

                var rec = new DiensteManager.model.GFIConfig({
                    attr_name: attr_name,
                    map_name: map_name
                });

                gfiConfigStore.insert(index, rec);
                index++;
            }
            Ext.getCmp("layergfidisable").setValue(false);
            Ext.getCmp("editGFIConfig").setDisabled(false);
        }
        else {
            Ext.getCmp("layergfidisable").setValue(false);
            Ext.getCmp("editGFIConfig").setDisabled(false);
        }

        if (selectedLayer.legend_type === "statisch") {
            Ext.getCmp("layerlegendpath").setVisible(true);
        }
        else {
            Ext.getCmp("layerlegendpath").setVisible(false);
        }

        if (service_type === "WMS") {
            Ext.getCmp("layertestrequest").setDisabled(false);
            Ext.getCmp("layergutter").setHidden(false);
            Ext.getCmp("layertilesize").setHidden(false);
            Ext.getCmp("layersingletile").setHidden(false);
            Ext.getCmp("layertransparent").setHidden(false);
            Ext.getCmp("layernotsupportedfor3d").setHidden(false);

            var items_wms = [{
                text: "Im Vorschauportal",
                type: "vorschauportal",
                itemId: "vorschauportal",
                listeners: {
                    click: "onLayerTestRequest"
                }
            }];

            if (software !== "Extern") {
                items_wms.push({
                    text: "getMap - intern",
                    type: "getmap_intern",
                    itemId: "getmap_intern",
                    listeners: {
                        click: "onLayerTestRequest_getMap"
                    }
                }, {
                    text: "getMap alle Layer - intern",
                    type: "getmap_alle_layer_intern",
                    itemId: "getmap_alle_layer_intern",
                    listeners: {
                        click: "onLayerTestRequest_getMap"
                    }
                }, {
                    text: "getLegendGraphic - intern",
                    type: "getlegendgraphic_intern",
                    itemId: "getlegendgraphic_intern",
                    listeners: {
                        click: "onLayerTestRequest"
                    }
                });
            }

            items_wms.push({
                text: "getMap - extern",
                type: "getmap_extern",
                itemId: "getmap_extern",
                listeners: {
                    click: "onLayerTestRequest_getMap"
                }
            }, {
                text: "getMap alle Layer - extern",
                type: "getmap_alle_layer_extern",
                itemId: "getmap_alle_layer_extern",
                listeners: {
                    click: "onLayerTestRequest_getMap"
                }
            }, {
                text: "getLegendGraphic - extern",
                type: "getlegendgraphic_extern",
                itemId: "getlegendgraphic_extern",
                listeners: {
                    click: "onLayerTestRequest"
                }
            });

            Ext.getCmp("layertestrequest").setMenu(
                new Ext.menu.Menu({
                    items: items_wms
                })
            );
        }
        else if (service_type === "WFS") {
            Ext.getCmp("layertestrequest").setDisabled(false);
            Ext.getCmp("layerhittolerance").setHidden(false);
            Ext.getCmp("layernamespace").setHidden(false);
            Ext.getCmp("layerdatetimecolumn").setHidden(false);

            var items_wfs = [];

            if (software !== "Extern") {
                items_wfs.push({
                    text: "getFeature - intern",
                    type: "getfeature_intern",
                    itemId: "getfeature_intern",
                    listeners: {
                        click: "onLayerTestRequest"
                    }
                }, {
                    text: "getFeature alle FeatureTypes - intern",
                    type: "getfeature_alle_featuretypes_intern",
                    itemId: "getfeature_alle_featuretypes_intern",
                    listeners: {
                        click: "onLayerTestRequest"
                    }
                }, {
                    text: "describeFeature - intern",
                    type: "describefeature_intern",
                    itemId: "describefeature_intern",
                    listeners: {
                        click: "onLayerTestRequest"
                    }
                });
            }

            items_wfs.push({
                text: "getFeature - extern",
                type: "getfeature_extern",
                itemId: "getfeature_extern",
                listeners: {
                    click: "onLayerTestRequest"
                }
            }, {
                text: "getFeature alle FeatureTypes - extern",
                type: "getfeature_alle_featuretypes_extern",
                itemId: "getfeature_alle_featuretypes_extern",
                listeners: {
                    click: "onLayerTestRequest"
                }
            }, {
                text: "describeFeature - extern",
                type: "describefeature_extern",
                itemId: "describefeature_extern",
                listeners: {
                    click: "onLayerTestRequest"
                }
            });

            Ext.getCmp("layertestrequest").setMenu(
                new Ext.menu.Menu({
                    items: items_wfs
                })
            );
        }
        else if (service_type === "Terrain3D") {
            Ext.getCmp("layer3dsettings").setHidden(false);
            Ext.getCmp("layerrequestvertexnormals").setHidden(false);
        }
        else if (service_type === "TileSet3D") {
            Ext.getCmp("layer3dsettings").setHidden(false);
            Ext.getCmp("layermaximumscreenspaceerror").setHidden(false);
        }
        else if (service_type === "SensorThings") {
            Ext.getCmp("layertestrequest").setDisabled(false);
            Ext.getCmp("layertestrequest").setMenu(
                new Ext.menu.Menu({
                    items: [
                        {
                            text: "Im Vorschauportal",
                            type: "vorschauportal",
                            itemId: "vorschauportal",
                            listeners: {
                                click: "onLayerTestRequest"
                            }
                        }
                    ]}
                )
            );
            Ext.getCmp("layersensorthingssettings").setHidden(false);
            Ext.getCmp("layerEPSG").setValue(selectedLayer.epsg);
            Ext.getCmp("layerURLParamsRootEl").setValue(selectedLayer.rootel);
            Ext.getCmp("layerURLParamsExpand").setValue(selectedLayer.expand);
            Ext.getCmp("layerURLParamsFilter").setValue(selectedLayer.filter);
            Ext.getCmp("layerRelatedWMSLayerIDs").setValue(selectedLayer.related_wms_layers);
            Ext.getCmp("layerStyleID").setValue(selectedLayer.style_id);
            if (selectedLayer.cluster_distance) {
                Ext.getCmp("layerClusterDistance").setValue(selectedLayer.cluster_distance);
            }
            else {
                Ext.getCmp("layerClusterDistance").setValue("");
            }
            Ext.getCmp("layerLoadThingsOnlyInCurrentExtent").setValue(selectedLayer.load_things_only_in_current_extent);
            Ext.getCmp("layerMouseHoverField").setValue(selectedLayer.mouse_hover_field);
            Ext.getCmp("layerStaTestUrl").setValue(selectedLayer.sta_test_url);
        }
        else if (service_type === "Oblique") {
            Ext.getCmp("layerobliquesettings").setHidden(false);
        }
        else if (service_type === "VectorTiles") {
            Ext.getCmp("layervectortilessettings").setHidden(false);
        }

        this.updateJsonField();

        if (!window.read_only) {
            Ext.getCmp("layerjsonupdate").setDisabled(false);
            Ext.getCmp("layerjsonupdateall").setDisabled(false);
        }

        Ext.getCmp("savelayerdetails").setDisabled(true);
    },

    updateJsonField: function () {
        if (Ext.getCmp("layerid").getSubmitValue() !== "") {
            Ext.Ajax.request({
                url: "api?layers=" + Ext.getCmp("layerid").getSubmitValue() + "&scope=all&style=pretty",
                method: "GET",
                success: function (response) {
                    Ext.getCmp("layer_json_field").setValue(response.responseText);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onLayerTestRequest: function (btn) {
        var req = btn.text,
            layergrid = Ext.getCmp("layers-grid"),
            selectedLayer = layergrid.getSelectionModel().getSelection()[0].data,
            allLayers = "";

        Ext.getStore("Layers").each(function (layer) {
            allLayers += layer.get("layer_name") + ",";
        });

        allLayers = allLayers.substr(0, allLayers.length - 1);

        var url_int = Ext.getCmp("serviceurlint").getSubmitValue(),
            url_ext = Ext.getCmp("serviceurlext").getSubmitValue(),
            version = Ext.getCmp("serviceversion").getSubmitValue();

        var url = "",
            maxFeatures = "maxFeatures";

        if (version === "2.0.0") {
            maxFeatures = "count";
        }

        if (req === "getFeature - intern") {
            url = url_int + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&" + maxFeatures + "=1&typename=" + selectedLayer.layer_name;
        }
        else if (req === "getFeature - extern") {
            url = url_ext + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&" + maxFeatures + "=1&typename=" + selectedLayer.layer_name;
        }
        else if (req === "getFeature alle FeatureTypes - intern") {
            url = url_int + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&typename=" + allLayers;
        }
        else if (req === "getFeature alle FeatureTypes - extern") {
            url = url_ext + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&typename=" + allLayers;
        }
        else if (req === "describeFeature - intern") {
            url = url_int + "?service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + selectedLayer.layer_name;
        }
        else if (req === "describeFeature - extern") {
            url = url_ext + "?service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + selectedLayer.layer_name;
        }
        else if (req === "getLegendGraphic - intern") {
            url = url_int + "?service=WMS&request=GetLegendGraphic&version=" + version + "&format=image/png&layer=" + selectedLayer.layer_name;
        }
        else if (req === "getLegendGraphic - extern") {
            url = url_ext + "?service=WMS&request=GetLegendGraphic&version=" + version + "&format=image/png&layer=" + selectedLayer.layer_name;
        }

        if (req === "Im Vorschauportal") {
            var previewURL = DiensteManager.Config.getMapPreview() + "?Map/layerids=" + DiensteManager.Config.getMapPreviewBaseLayerId() + "," + selectedLayer.layer_id;

            window.open(previewURL, "_blank");
        }
        else {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "Request ausführen";
            mb.buttonText.no = "Abbrechen";

            mb.confirm("Testrequest", "<input id=\"testRequestInput\" value=\"" + url + "\">", function (btnConfirm) {
                if (btnConfirm === "yes") {
                    window.open(url, "_blank");
                }
                else if (btnConfirm === "no") {
                    mb.close();
                }
            });
        }
    },

    onLayerTestRequest_getMap: function (btn) {

        var getMapTestLinkWindow = Ext.create({
            xtype: "ServiceDetails_Layer_getMap",
            viewModel: {
                data: {
                    layerData: Ext.getCmp("layers-grid").getSelectionModel().getSelection()[0],
                    req: btn.text
                }
            }
        });

        var combo = Ext.getCmp("bbox_getMap");

        combo.select(combo.getStore().getAt(0));

        getMapTestLinkWindow.show();
    },

    onSaveLayerDetails: function () {
        if (window.read_only) {
            Ext.MessageBox.alert("Keine Berechtigung", "Sie haben nur lesenden Zugriff. Änderungen wurden nicht gespeichert!");
            Ext.getCmp("savelayerdetails").setDisabled(true);
        }
        else {
            var layer_id = Ext.getCmp("layerid").getSubmitValue(),
                additional_categories = Ext.getCmp("layercategories").getSubmitValue(),
                title_alt = Ext.getCmp("layeralttitle").getSubmitValue(),
                gfi_format = Ext.getCmp("layergfiformat").getSubmitValue(),
                gfi_theme = Ext.getCmp("layergfitheme").getSubmitValue(),
                gfi_complex = Ext.getCmp("layergficomplex").getValue(),
                gfi_asnewwindow = Ext.getCmp("layergfiasnewwindow").getValue(),
                gfi_windowspecs = Ext.getCmp("layergfiwindowspecs").getValue(),
                layerattribution = Ext.getCmp("layerattribution").getSubmitValue(),
                gutter = Ext.getCmp("layergutter").getSubmitValue(),
                transparency = Ext.getCmp("layertransparency").getSubmitValue(),
                tilesize = Ext.getCmp("layertilesize").getSubmitValue(),
                legend_url_internet = Ext.getCmp("layerlegendurlinternet").getSubmitValue(),
                legend_url_intranet = Ext.getCmp("layerlegendurlintranet").getSubmitValue(),
                singletile = Ext.getCmp("layersingletile").getValue(),
                transparent = Ext.getCmp("layertransparent").getValue(),
                legend_type = Ext.getCmp("layerlegendtype").getSubmitValue(),
                legend_path = Ext.getCmp("layerlegendpath").getSubmitValue(),
                scale_min = Ext.getCmp("layerminscale").getValue(),
                scale_max = Ext.getCmp("layermaxscale").getValue(),
                featurecount = Ext.getCmp("layerfeaturecount").getValue(),
                output_format = Ext.getCmp("layeroutputformat").getSubmitValue(),
                maximum_screen_space_error = Ext.getCmp("layermaximumscreenspaceerror").getSubmitValue(),
                request_vertex_normals = Ext.getCmp("layerrequestvertexnormals").getValue(),
                sensorThingsrelatedWMSLayerIDs = Ext.getCmp("layerRelatedWMSLayerIDs").getSubmitValue(),
                sensorThingsRootEl = Ext.getCmp("layerURLParamsRootEl").getSubmitValue(),
                sensorThingsFilter = Ext.getCmp("layerURLParamsFilter").getSubmitValue(),
                sensorThingsExpand = Ext.getCmp("layerURLParamsExpand").getSubmitValue(),
                sensorThingsEPSG = Ext.getCmp("layerEPSG").getSubmitValue(),
                sensorThingsStyleID = Ext.getCmp("layerStyleID").getSubmitValue(),
                sensorThingsClusterDistance = Ext.getCmp("layerClusterDistance").getSubmitValue(),
                sensorThingsLoadThingsOnlyInCurrentExtent = Ext.getCmp("layerLoadThingsOnlyInCurrentExtent").getValue(),
                sensorMouseHoverField = Ext.getCmp("layerMouseHoverField").getValue(),
                sensorTestUrl = Ext.getCmp("layerStaTestUrl").getValue(),
                gfi_theme_params_string = Ext.getCmp("layergfithemeparams").getSubmitValue(),
                resolution = Ext.getCmp("layerresolution").getSubmitValue(),
                hidelevels = Ext.getCmp("layerhidelevels").getSubmitValue(),
                minzoom = Ext.getCmp("layerminzoom").getSubmitValue(),
                terrainurl = Ext.getCmp("layerterrainurl").getSubmitValue(),
                projection = Ext.getCmp("layerprojection").getSubmitValue(),
                hittolerance = Ext.getCmp("layerhittolerance").getSubmitValue(),
                namespace = Ext.getCmp("layernamespace").getSubmitValue(),
                datetime_column = Ext.getCmp("layerdatetimecolumn").getSubmitValue(),
                service_url_is_visible = Ext.getCmp("layerurlisvisible").getSubmitValue(),
                gfi_theme_params = "",
                gfi_config = "",
                gfi_useforalllayers = false,
                vectorTilesExtent = Ext.getCmp("layerextent").getSubmitValue(),
                vectorTilesOrigin = Ext.getCmp("layerorigin").getSubmitValue(),
                vectorTilesResolutions = Ext.getCmp("layerresolutions").getSubmitValue(),
                vectorTilesVTStyles = Ext.getCmp("layervtstyles").getSubmitValue(),
                vectorTilesVisibility = Ext.getCmp("layervisibility").getValue(),
                notsupportedfor3d = Ext.getCmp("layernotsupportedfor3d").getValue(),
                gfi_beautifykeys = Ext.getCmp("layergfibeautifykeys").getValue();

            if (Ext.getCmp("layergfidisable").getValue()) {
                gfi_config = "ignore";
            }
            else {
                if (Ext.getStore("GFIConfigs").count() > 0) {
                    var gfi_config_json = {};

                    Ext.getStore("GFIConfigs").each(function (record) {
                        if (record.data.attr_name.length > 0 && record.data.map_name.length > 0) {
                            if (record.data.map_name.trim().startsWith("{")) {
                                gfi_config_json[record.data.attr_name] = JSON.parse(record.data.map_name);
                            }
                            else {
                                gfi_config_json[record.data.attr_name] = record.data.map_name;
                            }
                        }
                    });

                    gfi_config = JSON.stringify(gfi_config_json);
                    if (Ext.getCmp("gfi_useforalllayers")) {
                        gfi_useforalllayers = Ext.getCmp("gfi_useforalllayers").getValue();
                    }
                }
            }

            var layergrid = Ext.getCmp("layers-grid"),
                scroll_x = layergrid.getView().getScrollX(),
                scroll_y = layergrid.getView().getScrollY();

            if (gfi_theme_params_string !== "") {
                try {
                    gfi_theme_params = JSON.stringify(JSON.parse(gfi_theme_params_string));
                }
                catch (e) {
                    Ext.MessageBox.alert("Fehler", "GFI - Theme Parameter ist kein valides JSON Objekt!");
                }
            }

            Ext.Ajax.request({
                url: "backend/updatelayerdetails",
                method: "POST",
                jsonData: {
                    layer_id: parseInt(layer_id),
                    service_id: Ext.getCmp("serviceid").getSubmitValue(),
                    service_type: Ext.getCmp("servicetype").getSubmitValue(),
                    additional_categories: additional_categories,
                    title_alt: title_alt,
                    gfi_config: gfi_config,
                    gfi_format: gfi_format,
                    gfi_theme: gfi_theme,
                    gfi_complex: gfi_complex,
                    gfi_asnewwindow: gfi_asnewwindow,
                    gfi_windowspecs: gfi_windowspecs,
                    layerattribution: layerattribution,
                    gutter: parseInt(gutter),
                    transparency: parseInt(transparency),
                    tilesize: parseInt(tilesize),
                    legend_url_internet: legend_url_internet,
                    legend_url_intranet: legend_url_intranet,
                    legend_type: legend_type,
                    legend_path: legend_path,
                    singletile: singletile,
                    transparent: transparent,
                    scale_min: scale_min,
                    scale_max: scale_max,
                    featurecount: featurecount,
                    output_format: output_format,
                    gfi_useforalllayers: gfi_useforalllayers,
                    maximum_screen_space_error: maximum_screen_space_error,
                    request_vertex_normals: request_vertex_normals,
                    gfi_theme_params: gfi_theme_params,
                    related_wms_layers: sensorThingsrelatedWMSLayerIDs,
                    rootel: sensorThingsRootEl,
                    filter: sensorThingsFilter,
                    expand: sensorThingsExpand,
                    epsg: sensorThingsEPSG,
                    style_id: sensorThingsStyleID,
                    cluster_distance: parseInt(sensorThingsClusterDistance),
                    load_things_only_in_current_extent: sensorThingsLoadThingsOnlyInCurrentExtent,
                    mouse_hover_field: sensorMouseHoverField,
                    sta_test_url: sensorTestUrl,
                    resolution: resolution,
                    hidelevels: hidelevels,
                    minzoom: minzoom,
                    terrainurl: terrainurl,
                    projection: projection,
                    hittolerance: hittolerance,
                    namespace: namespace,
                    datetime_column: datetime_column,
                    service_url_is_visible: service_url_is_visible,
                    last_edited_by: window.auth_user,
                    last_edit_date: moment().format("YYYY-MM-DD HH:mm"),
                    extent: vectorTilesExtent,
                    origin: vectorTilesOrigin,
                    resolutions: vectorTilesResolutions,
                    vtstyles: vectorTilesVTStyles,
                    visibility: vectorTilesVisibility,
                    notsupportedfor3d: notsupportedfor3d,
                    gfi_beautifykeys: gfi_beautifykeys
                },
                success: function () {
                    var selectedLayer = layergrid.getSelectionModel().getSelection()[0],
                        row = layergrid.store.indexOf(selectedLayer);

                    if (row > -1) {
                        Ext.getStore("Layers").reload({
                            params: {service_id: selectedLayer.data.service_id},
                            callback: function () {
                                layergrid.getSelectionModel().select(selectedLayer);
                                layergrid.getView().select(row);
                                layergrid.getView().setScrollX(scroll_x);
                                layergrid.getView().setScrollY(scroll_y);
                            }
                        });
                    }

                    Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                    Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
                    Ext.getCmp("savelayerdetails").setDisabled(true);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onUpdateLayerList: function () {
        var me = this,
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            service_title = Ext.getCmp("servicetitle").getSubmitValue(),
            service_type = Ext.getCmp("servicetype").getSubmitValue(),
            version = Ext.getCmp("serviceversion").getSubmitValue(),
            software = Ext.getCmp("servicesoftware").getSubmitValue(),
            url = Ext.getCmp("serviceurlint").getSubmitValue(),
            url_ext = Ext.getCmp("serviceurlext").getSubmitValue(),
            externalService = Ext.getCmp("externalServiceCheckBox").getValue(),
            reverseProxy = Ext.getCmp("reverseProxyCheckBox").getValue();

        if (externalService && !reverseProxy) {
            url = url_ext;
        }

        if (service_type === "WMS" || service_type === "WFS" || service_type === "WMTS") {
            me.showLoadMask("servicedetails-window");

            if (url.slice(-1) === "&") {
                url = url.slice(0, -1);
            }

            var is_mrh = false;

            if (service_title.indexOf("MRH") > -1) {
                is_mrh = true;
            }

            Ext.Ajax.request({
                url: "backend/updateservicelayers",
                method: "POST",
                jsonData: {
                    url: url,
                    service_id: service_id,
                    service_type: service_type,
                    software: software,
                    version: version,
                    url_ext: url_ext,
                    is_mrh: is_mrh,
                    externalService: externalService
                },
                success: function (response) {
                    var responseText = Ext.decode(response.responseText);

                    if (responseText.success) {
                        var layer_list = responseText.layers;

                        var groupLayerStore = new Ext.data.ArrayStore({
                            fields: [
                                {name: "type"},
                                {name: "layer_name"},
                                {name: "title"},
                                {name: "service_id"},
                                {name: "service_type"},
                                {name: "namespace"},
                                {name: "scale_max"},
                                {name: "scale_min"},
                                {name: "output_format"}
                            ],
                            filters: [{
                                property: "type",
                                value: /groupLayer/
                            }]
                        });

                        groupLayerStore.loadData(layer_list);

                        var layerStore = new Ext.data.ArrayStore({
                            fields: [
                                {name: "type"},
                                {name: "layer_name"},
                                {name: "title"},
                                {name: "service_id"},
                                {name: "service_type"},
                                {name: "namespace"},
                                {name: "scale_max"},
                                {name: "scale_min"},
                                {name: "output_format"}
                            ],
                            filters: [{
                                property: "type",
                                value: /layer/
                            }]
                        });

                        layerStore.loadData(layer_list);

                        var layerSelectionWindow = Ext.getCmp("layerselection-window");

                        if (layerSelectionWindow) {
                            layerSelectionWindow.destroy();
                        }

                        layerSelectionWindow = Ext.create("servicedetails.ServiceDetails_LayerSelection");

                        if (groupLayerStore.getCount() > 0) {
                            Ext.getCmp("select-grouplayer-grid").setStore(groupLayerStore);
                        }
                        else {
                            Ext.getCmp("select-grouplayer-grid").hide();
                        }

                        Ext.getCmp("select-layer-grid").setStore(layerStore);
                        Ext.getCmp("select-layer-grid").getView().getSelectionModel().selectAll(true);

                        me.hideLoadMask();
                        layerSelectionWindow.show();
                    }
                    else {
                        me.hideLoadMask();
                        Ext.MessageBox.alert("Status", "Dienst konnte nicht abgerufen werden: " + responseText.error);
                    }
                },
                failure: function (response) {
                    console.log(response);
                    me.hideLoadMask();
                    Ext.MessageBox.alert("Status", "Dienst konnte nicht abgerufen werden!");
                }
            });
        }
        else {
            var layerEditor = Ext.getCmp("layereditor-window");

            if (layerEditor) {
                layerEditor.destroy();
            }

            layerEditor = Ext.create("servicedetails.layereditor.LayerEditor");

            layerEditor.show();
        }
    },

    onLayerSelectionDone: function () {

        var layerList = [],
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            service_name = Ext.getCmp("servicename").getSubmitValue(),
            service_type = Ext.getCmp("servicetype").getSubmitValue(),
            selectedGroupLayers = Ext.getCmp("select-grouplayer-grid").getView().getSelectionModel().getSelection(),
            selectedLayers = Ext.getCmp("select-layer-grid").getView().getSelectionModel().getSelection();

        Ext.each(selectedGroupLayers, function (item) {
            item.data.service_type = service_type;
            layerList.push(item.data);
        });

        Ext.each(selectedLayers, function (item) {
            item.data.service_type = service_type;
            layerList.push(item.data);
        });

        Ext.Ajax.request({
            url: "backend/addLayersToService",
            method: "POST",
            jsonData: {
                service_id: service_id,
                service_name: service_name,
                service_type: service_type,
                layer_list: layerList,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function (response) {
                var responseText = Ext.decode(response.responseText);

                Ext.getCmp("updatelayer").setDisabled(false);
                Ext.getCmp("layerdeleterequest").setDisabled(false);
                if (responseText.success) {
                    if (responseText.status === "layer_insert_complete") {
                        Ext.getStore("Layers").reload({
                            params: {service_id: service_id}
                        });
                        Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                        Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
                    }
                    else if (responseText.status === "no_layers_found") {
                        Ext.MessageBox.alert("Status", "keine Layer in den Capabilities enthalten!");
                    }
                    else if (responseText.status === "layer_update_needed") {
                        Ext.create("servicedetails.layermapper.LayerMapper").show();
                        var store = Ext.getStore("NewLayer");

                        store.loadData(responseText.new_layer);
                    }
                    else if (responseText.status === "layer_update_complete") {
                        Ext.MessageBox.alert("Status", responseText.msg);
                        Ext.getStore("Layers").reload({
                            params: {service_id: service_id}
                        });
                        DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(service_id);
                        Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                        Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
                    }
                }
                else {
                    Ext.MessageBox.alert("Fehler", "Der Dienst konnte nicht abgerufen werden!");
                }
                Ext.getCmp("layerselection-window").hide();
            },
            failure: function (response) {
                Ext.getCmp("layerselection-window").hide();
                Ext.getCmp("updatelayer").setDisabled(false);
                Ext.getCmp("layerdeleterequest").setDisabled(false);
                Ext.MessageBox.alert("Status", "Dienst konnte nicht abgerufen werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onLayerDeleteRequest: function () {
        var deleteLayerRequestWindow = Ext.getCmp("deletelayerrequest-window");

        if (deleteLayerRequestWindow) {
            deleteLayerRequestWindow.destroy();
        }

        deleteLayerRequestWindow = Ext.create("servicedetails.deletelayerrequest.DeleteLayerRequest");

        deleteLayerRequestWindow.show();
    },

    onLinkDataset: function () {
        var me = this,
            store = Ext.getStore("Datasets");

        store.removeAll();

        if (Ext.getCmp("metadata_uuid").getSubmitValue() !== "") {
            Ext.create("servicedetails.metadata.LinkLayerMD").show();

            me.showLoadMask("linklayermd-window");

            Ext.Ajax.request({
                url: "backend/getdatasetmetadata",
                method: "POST",
                jsonData: {
                    uuid: Ext.getCmp("metadata_uuid").getSubmitValue(),
                    source_csw: Ext.getCmp("servicesourcecswname").getSubmitValue()
                },
                success: function (response) {
                    var results = Ext.decode(response.responseText);

                    me.hideLoadMask();

                    if (!results.error) {
                        if (results.length > 0) {
                            store.add(results);
                        }
                        else {
                            Ext.MessageBox.alert("Status", "Keine Treffer im " + Ext.getCmp("servicesourcecswname").getSubmitValue());
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + Ext.getCmp("servicesourcecswname").getSubmitValue());
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    me.hideLoadMask();
                    Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + Ext.getCmp("servicesourcecswname").getSubmitValue());
                }
            });
        }
        else {
            Ext.create("servicedetails.metadata.LinkseparateLayerMD").show();
        }
    },

    onDeleteDSLink: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Ajax.request({
            url: "backend/deletelayerlink",
            method: "POST",
            jsonData: {
                layer_links_id: data.layer_links_id,
                service_id: Ext.getCmp("serviceid").getSubmitValue(),
                layer_id: Ext.getCmp("layerid").getSubmitValue(),
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
                DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(data.service_id);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
            }
        });
    },

    onInspireCapabilities: function () {
        var me = this,
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            service = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data,
            mb = Ext.MessageBox;

        if (!service.dsc_complete) {
            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("INSPIRE Capabilities", "Es sind nicht alle Layer gekoppelt, trotzdem fortfahren?", function (btn) {
                if (btn === "yes") {
                    me._uploadInspireCapabilities(service);
                }
            });
        }
        else {
            me._uploadInspireCapabilities(service);
        }
    },

    enableSaveButton: function () {
        if (!window.read_only) {
            Ext.getCmp("savelayerdetails").setDisabled(false);
        }
    },

    _uploadInspireCapabilities: function (service) {
        var me = this;

        me.showLoadMask("servicedetails-window");

        Ext.Ajax.request({
            url: "backend/createinspirecapabilities",
            method: "POST",
            jsonData: {
                service: service,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function (response) {
                me.hideLoadMask();
                if (response.responseText.indexOf("WMS_Capabilities") > -1) {
                    saveAs(new Blob([response.responseText], {type: "text/xml"}), "GetCapabilities130.xml");
                }
                else {
                    var results = Ext.decode(response.responseText);

                    if (results.error) {
                        Ext.MessageBox.alert("Fehler", "Der Dienst konnte nicht abgerufen werden!");
                    }
                    else {
                        Ext.MessageBox.alert("Erfolg", "Die Metadatenkonfiguration wurde erfolgreich hochgeladen!");
                    }
                }
            },
            failure: function (response) {
                me.hideLoadMask();
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
            }
        });
    },

    onExportLayerlist: function () {
        var me = this,
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            service = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data;

        me.showLoadMask("servicedetails-window");

        if (service.dsc_complete === true) {
            Ext.Ajax.request({
                url: "backend/getlinkedlayers?service_id=" + service_id,
                method: "GET",
                success: function (response) {
                    var layers = Ext.decode(response.responseText),
                        csv = "Layer ID;Name;Titel;Datensatz UUID;Datensatz RSIS;Datensatzname\r\n";

                    for (var i in layers) {
                        csv += layers[i].layer_id + ";" + layers[i].layer_name + ";" + layers[i].title + ";" + layers[i].md_id + ";" + layers[i].rs_id + ";" + layers[i].dataset_name + "\r\n";
                    }

                    saveAs(new Blob(["\ufeff", csv], {type: "text/csv;charset=UTF-8"}), "layer.csv");
                    me.hideLoadMask();
                },
                failure: function (response) {
                    me.hideLoadMask();
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
                }
            });
        }
        else {
            var csv = "Layer ID;Name;Titel\r\n";

            Ext.getStore("Layers").each(function (layer) {
                csv += layer.get("layer_id") + ";" + layer.get("layer_name") + ";" + layer.get("title") + "\r\n";
            });
            saveAs(new Blob(["\ufeff", csv], {type: "text/csv;charset=UTF-8"}), "layer.csv");
            me.hideLoadMask();
        }
    },

    onEditGFIConfig: function () {
        var gfiEditor = Ext.getCmp("gfieditor-window");

        if (gfiEditor) {
            gfiEditor.destroy();
        }

        gfiEditor = Ext.create("servicedetails.gfieditor.GFIEditor");
        gfiEditor.show();
    },

    onGfiAsNewWindow: function () {
        if (Ext.getCmp("layergfiasnewwindow").checked) {
            Ext.getCmp("layergfiwindowspecs").setHidden(false);
            this.enableSaveButton();
        }
        else {
            Ext.getCmp("layergfiwindowspecs").setHidden(true);
            Ext.getCmp("layergfiwindowspecs").setValue("");
        }
    },

    onClickMetadataLink: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        if (data.show_doc_url) {
            window.open(data.show_doc_url + data.md_id, "_blank");
        }
        else {
            Ext.Ajax.request({
                url: "backend/getmetadatavalues?md_id=" + data.md_id,
                method: "GET",
                success: function (response) {
                    var metadata = Ext.decode(response.responseText)[0];

                    Ext.create("servicedetails.metadata.LinkseparateLayerMD").show();
                    Ext.getCmp("linkseparatelayermd-tabpanel").setActiveTab("linkseparatelayermd-manualtab");

                    Ext.getCmp("manualinsertmdid").setValue(metadata.md_id);
                    Ext.getCmp("manualinserttitle").setValue(metadata.dataset_name);
                    Ext.getCmp("manualinsertdescription").setValue(metadata.description);
                    Ext.getCmp("manualinsertaccessconstraints").setValue(metadata.access_constraints);
                    Ext.getCmp("manualinsertfees").setValue(metadata.fees);
                    Ext.getCmp("manualinsertcatopendata").setValue(metadata.cat_opendata);
                    Ext.getCmp("manualinsertcatinspire").setValue(metadata.cat_inspire);
                    Ext.getCmp("manualinsertcatorg").setValue(metadata.cat_org);
                    Ext.getCmp("manualinsertkeywords").setValue(metadata.keywords);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
                }
            });
        }
    },

    onDisableGFI: function () {
        Ext.getCmp("editGFIConfig").setDisabled(Ext.getCmp("layergfidisable").getValue());
        this.enableSaveButton();
    },

    onRecreateJson: function (button) {
        var layer_id = "",
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            me = this;

        if (button.id === "layerjsonupdate") {
            layer_id = Ext.getCmp("layerid").getSubmitValue();
        }

        Ext.Ajax.request({
            url: "backend/updatelayerjson?layerid=" + layer_id + "&serviceid=" + service_id,
            method: "GET",
            success: function () {
                me.updateJsonField();
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Aufruf fehlgeschlagen!");
            }
        });
    },

    showLoadMask: function (component) {
        this.loadingsMask = new Ext.LoadMask({
            msg: "Bitte warten...",
            target: Ext.getCmp(component)
        });
        this.loadingsMask.show();
    },

    hideLoadMask: function () {
        this.loadingsMask.hide();
    },

    isJsonString: function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    }
});

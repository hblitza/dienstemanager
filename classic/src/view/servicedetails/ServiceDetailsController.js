Ext.define("DiensteManager.view.servicedetails.ServiceDetailsController", {
    id: "servicedetailscontroller",
    name: "ServiceDetailsController",
    extend: "Ext.app.ViewController",
    alias: "controller.dienst_details",

    onSearchMetadata: function (btn) {
        var source_csw_name = Ext.getCmp("servicesourcecswname").getSubmitValue(),
            title = Ext.getCmp("servicetitle").getSubmitValue();

        if (source_csw_name.length > 0) {
            btn.setDisabled(true);

            var loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("servicedetails-window")
            });

            loadingsMask.show();

            Ext.Ajax.request({
                url: "backend/getservicemetadata",
                method: "POST",
                jsonData: {
                    title: title,
                    source_csw: source_csw_name
                },
                success: function (response) {
                    var results = Ext.decode(response.responseText);

                    btn.setDisabled(false);

                    if (!results.error) {
                        if (results.length > 0) {
                            var store = Ext.getStore("ServiceMDSets"),
                                linkServiceWindow = Ext.getCmp("linkservicemd-window");

                            store.removeAll();
                            store.add(results);

                            if (!linkServiceWindow) {
                                Ext.create("servicedetails.metadata.linkservicemd").show();
                            }
                            else {
                                linkServiceWindow.show();
                            }
                        }
                        else {
                            Ext.MessageBox.alert("Status", "Keine Treffer im " + source_csw_name + " für " + title + ". Titel korrekt?");
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_name + " für " + title);
                    }

                    loadingsMask.hide();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_name + " für " + title);
                    btn.setDisabled(false);
                    loadingsMask.hide();
                }
            });
        }
    },

    onDeleteMetadataCoupling: function (btn) {
        Ext.getCmp("metadata_uuid").setValue("");
        Ext.getCmp("servicesourcecswname").setValue("");
        Ext.getCmp("metadata_uuid_link").setValue("Keine Metadaten verknüpft");
        btn.setDisabled(true);
        this.enableSaveButton();
    },

    onSaveServiceDetails: function () {
        if (window.read_only) {
            Ext.MessageBox.alert("Keine Berechtigung", "Sie haben nur lesenden Zugriff. Änderungen wurden nicht gespeichert!");
            Ext.getCmp("save_service_details_button").setDisabled(true);
        }
        else {
            Ext.getBody().mask();

            var me = this,
                service_id = Ext.getCmp("serviceid").getSubmitValue(),
                service_checked_status = Ext.getCmp("servicecheckedstatus").getValue(),
                last_edit_date = Ext.getCmp("servicelasteditdate").getSubmitValue(),
                title = Ext.getCmp("servicetitle").getSubmitValue(),
                description = Ext.getCmp("servicedescription").getSubmitValue(),
                fees = Ext.getCmp("servicefees").getSubmitValue(),
                access_constraints = Ext.getCmp("serviceaccessconstraints").getSubmitValue(),
                status = Ext.getCmp("servicestatus").getSubmitValue(),
                editor = Ext.getCmp("serviceeditor").getSubmitValue(),
                responsible_party = Ext.getCmp("serviceresponsibleparty").getSubmitValue(),
                service_type = Ext.getCmp("servicetype").getSubmitValue(),
                version = Ext.getCmp("serviceversion").getSubmitValue(),
                software = Ext.getCmp("servicesoftware").getSubmitValue(),
                server = Ext.getCmp("serviceserver").getSubmitValue(),
                folder = Ext.getCmp("servicefolder").getSubmitValue(),
                service_name = Ext.getCmp("servicename").getSubmitValue(),
                url_int = Ext.getCmp("serviceurlint").getSubmitValue(),
                url_ext = Ext.getCmp("serviceurlext").getSubmitValue(),
                url_sec = "",
                security_type = "",
                only_intranet = Ext.getCmp("serviceonlyintranet").getValue(),
                only_internet = Ext.getCmp("serviceonlyinternet").getValue(),
                datasource = Ext.getCmp("servicedatasource").getSubmitValue(),
                mapping_description = Ext.getCmp("servicemappingdescription").getSubmitValue(),
                cached = Ext.getCmp("servicecached").getValue(),
                inspire_relevant = Ext.getCmp("serviceinspirerelevant").getValue(),
                qa = Ext.getCmp("serviceqa").getValue(),
                service_md_id = "",
                config_metadata_catalog_id = null,
                show_doc_url = null,
                externalService = Ext.getCmp("externalServiceCheckBox").getValue();

            if (DiensteManager.Config.getMetadatenModul()) {
                service_md_id = Ext.getCmp("metadata_uuid").getSubmitValue();
                var selection = Ext.getCmp("servicesourcecswname").getSelection();

                if (selection) {
                    config_metadata_catalog_id = selection.data.config_metadata_catalog_id;
                    show_doc_url = selection.data.show_doc_url;
                }
            }

            if (DiensteManager.Config.getSecurityModul()) {
                url_sec = Ext.getCmp("serviceurlsec").getSubmitValue();
                security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();
                url_sec = url_sec.replace(new RegExp(" ", "g"), "_");
                Ext.getCmp("serviceurlsec").setValue(url_sec);
            }

            url_int = url_int.replace(new RegExp(" ", "g"), "_");
            url_ext = url_ext.replace(new RegExp(" ", "g"), "_");

            Ext.getCmp("serviceurlint").setValue(url_int);
            Ext.getCmp("serviceurlext").setValue(url_ext);

            if (service_checked_status === "" || service_checked_status === null || service_checked_status === undefined) {
                service_checked_status = "Ungeprüft";
            }

            if (status === "" || status === null || status === undefined) {
                status = "In Bearbeitung";
            }

            if (service_md_id !== "" && DiensteManager.Config.getMetadatenModul()) {
                Ext.getCmp("metadata_uuid_link").setValue("<a href=\"" + show_doc_url + service_md_id + "\" target=\"_blank\">Metadaten anzeigen</a>");
            }

            var valid = true;

            if (software === "deegree" && (server === "lgvfds01" || server === "lgvfds02" || server === "wsca0620" || server === "metaver")) {
                valid = false;
            }
            else if (software === "ESRI" && (server === "wfalgpa002" || server === "wfalgpa003" || server === "wfalgqa003" || server === "lgvfds03" || server === "wfalgqa004" || server === "wfalgta004")) {
                valid = false;
            }
            else if (software === "GWC" && (server === "lgvfds01" || server === "lgvfds02" || server === "wsca0620" || server === "metaver")) {
                valid = false;
            }

            if (software === "" || server === "" || folder === "" || service_name === "" || url_int === "" || responsible_party === "") {
                if (!externalService) {
                    valid = false;
                }
                else {
                    var reverseProxy = Ext.getCmp("reverseProxyCheckBox").getValue();

                    if (reverseProxy && url_int === "") {
                        valid = false;
                    }
                }
            }
            if (!valid) {
                Ext.MessageBox.alert("Achtung", "Bitte alle benötigten Felder (Software, Server, Ordner/Webapp, Dienstname/Endpoint, interne URL, verantwortliche Stelle) ausfüllen!");
                Ext.getBody().unmask();
            }
            else {
                if (isNaN(parseInt(service_id)) && valid) {
                    var isinstore = false;

                    Ext.getStore("ServiceList").each(function (service) {
                        if (service.get("title") === title && service.get("url_ext") === url_ext) {
                            isinstore = true;
                        }
                    });
                    if (!isinstore) {
                        Ext.Ajax.request({
                            url: "backend/addservice",
                            method: "POST",
                            jsonData: {
                                service_checked_status: service_checked_status,
                                last_edit_date: last_edit_date,
                                title: title,
                                description: description,
                                fees: fees,
                                access_constraints: access_constraints,
                                status: status,
                                editor: editor,
                                responsible_party: responsible_party,
                                service_type: service_type,
                                version: version,
                                software: software,
                                server: server,
                                folder: folder,
                                service_name: service_name,
                                url_int: url_int,
                                url_ext: url_ext,
                                url_sec: url_sec,
                                security_type: security_type,
                                only_intranet: only_intranet,
                                only_internet: only_internet,
                                datasource: datasource,
                                mapping_description: mapping_description,
                                cached: cached,
                                inspire_relevant: inspire_relevant,
                                service_md_id: service_md_id,
                                last_edited_by: window.auth_user,
                                config_metadata_catalog_id: config_metadata_catalog_id,
                                qa: qa,
                                external: externalService
                            },
                            success: function (response) {
                                var jsonobj = Ext.decode(response.responseText);

                                service_id = jsonobj[0].service_id;

                                Ext.getStore("ServiceList").reload();

                                Ext.getCmp("serviceid").setValue(service_id);
                                Ext.getCmp("servicelasteditedby").setValue(window.auth_user);

                                Ext.getCmp("updatelayer").setDisabled(false);
                                if (DiensteManager.Config.getMetadatenModul()) {
                                    Ext.getCmp("linkdatasetbutton").setDisabled(service_md_id === "" && !window.read_only);
                                }
                                me.saveKeywords(service_id);
                                me.saveContacts(service_id);

                                Ext.getBody().unmask();

                                if (DiensteManager.Config.getUseWebsockets()) {
                                    window.socket.emit("service_details_saved");
                                }
                            },
                            failure: function (response) {
                                console.log(Ext.decode(response.responseText));
                                Ext.Msg.alert("Fehler", "Dienst konnte nicht gespeichert werden!");
                            }
                        });
                    }
                    else {
                        Ext.MessageBox.alert("Status", "Dienst existiert bereits!");
                        Ext.getBody().unmask();
                    }
                }
                else if (valid) {
                    var service_db = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data,
                        update_esri_namespace = false;

                    if (DiensteManager.Config.getEsriUrlNamespace() && service_type === "WFS" && software === "ESRI") {
                        update_esri_namespace = true;
                    }

                    var save_date = moment().format("YYYY-MM-DD HH:mm");

                    Ext.Ajax.request({
                        url: "backend/updateservice",
                        method: "POST",
                        jsonData: {
                            service_id: parseInt(service_id),
                            service_checked_status: service_checked_status,
                            last_edit_date: save_date,
                            description: description,
                            title: title,
                            fees: fees,
                            access_constraints: access_constraints,
                            status: status,
                            old_status: service_db.status,
                            editor: editor,
                            responsible_party: responsible_party,
                            service_type: service_type,
                            version: version,
                            software: software,
                            server: server,
                            folder: folder,
                            service_name: service_name,
                            url_int: url_int,
                            url_ext: url_ext,
                            url_sec: url_sec,
                            security_type: security_type,
                            only_intranet: only_intranet,
                            only_internet: only_internet,
                            datasource: datasource,
                            mapping_description: mapping_description,
                            cached: cached,
                            inspire_relevant: inspire_relevant,
                            service_md_id: service_md_id,
                            last_edited_by: window.auth_user,
                            config_metadata_catalog_id: config_metadata_catalog_id,
                            qa: qa,
                            old_qa: service_db.qa,
                            esri_namespace_update: update_esri_namespace,
                            external: externalService
                        },
                        success: function (response) {
                            var text = Ext.decode(response.responseText).status;

                            if (text === "success") {

                                if (DiensteManager.Config.getSecurityModul()) {

                                    if (security_type === "Basic Auth") {
                                        me.getBasicAuthConfiguration().then(function (basicAuthConfiguration) {
                                            if (basicAuthConfiguration.endpoint_name !== title || basicAuthConfiguration.proxy_url !== url_int) {
                                                basicAuthConfiguration.url_sec = url_sec;
                                                me.updateBasicAuthConfiguration(basicAuthConfiguration).then(function () {
                                                    Ext.Msg.alert("Hinweis", "Basic Auth Konfiguration wurde ebenfalls geändert.", Ext.emptyFn);
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        var storeQuery = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true);

                                        if (storeQuery.items[0]) {
                                            var oldServiceData = storeQuery.items[0].data;

                                            if (oldServiceData.security_type === "Basic Auth") {
                                                me.deleteBasicAuthConfiguration();
                                            }
                                        }
                                    }
                                }

                                Ext.getStore("ServiceList").reload();

                                DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(service_id);

                                Ext.getCmp("servicelasteditdate").setValue(save_date);
                                Ext.getCmp("servicelasteditedby").setValue(window.auth_user);

                                if (status === "Produktiv") {
                                    if (qa) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#A8D44D 50%,#A8D44D 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background:#A8D44D;");
                                    }
                                }
                                else if (status === "In Bearbeitung") {
                                    if (qa) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FAD801 50%,#FAD801 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background:#FAD801;");
                                    }
                                }
                                else if (status === "Test") {
                                    if (qa) {
                                        Ext.getCmp("servicestatus").setFieldStyle("background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FF234C 50%,#FF234C 100%);");
                                    }
                                    else {
                                        Ext.getCmp("servicestatus").setFieldStyle("background:#FF234C;");
                                    }
                                }
                                Ext.getCmp("save_service_details_button").setDisabled(true);
                            }
                            else {
                                Ext.Msg.alert("Fehler", "Dienst konnte nicht gespeichert werden!");
                            }
                            Ext.getBody().unmask();

                            if (DiensteManager.Config.getUseWebsockets()) {
                                window.socket.emit("service_details_saved");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.Msg.alert("Fehler", "Dienst konnte nicht gespeichert werden!");
                            Ext.getBody().unmask();
                        }
                    });

                    if (DiensteManager.Config.getMetadatenModul()) {
                        Ext.getCmp("linkdatasetbutton").setDisabled(service_md_id === "" && !window.read_only);
                    }
                    me.saveKeywords(parseInt(service_id));
                    me.saveContacts(parseInt(service_id));
                }
                else if (!valid) {
                    Ext.MessageBox.alert("Status", "Software oder Server falsch!");
                    Ext.getBody().unmask();
                }
            }
        }
    },

    onSecurityTypeChange: function () {

        if (DiensteManager.Config.getSecurityModul()) {

            var url_secured,
                service_type = Ext.getCmp("servicetype").getSubmitValue(),
                security_type = Ext.getCmp("servicesecuritytype").getSubmitValue(),
                server = Ext.getCmp("serviceserver").getSubmitValue(),
                software = Ext.getCmp("servicesoftware").getSubmitValue(),
                folder = Ext.getCmp("servicefolder").getSubmitValue(),
                service_name = Ext.getCmp("servicename").getSubmitValue(),
                endpoint = "WMSServer";

            if (service_type === "WFS") {
                endpoint = "WFSServer";
            }

            if (security_type !== "keine") {
                if (security_type === "Basic Auth") {
                    Ext.getCmp("onOpenSetAllowedGroups").setHidden(true);
                    Ext.getCmp("basicAuthConfigurationButton").setHidden(false);
                    if (server !== "" && folder !== "" && service_name !== "") {
                        var urls;

                        if (software === "deegree" || software === "ESRI") {
                            urls = {
                                lgvfds04: DiensteManager.Config.getSecurityProxyUrlAuth() + "esri_prod/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                                lgvfds03: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_prod/" + folder + "/services/" + service_name,
                                wfalgqa003: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + folder + "/services/" + service_name,
                                wfalgqa004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + folder + "/services/" + service_name,
                                wfalgta004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_test/" + folder + "/services/" + service_name
                            };
                            url_secured = urls[server];
                        }
                        else if (software === "MapServer") {
                            urls = {
                                lgvfds03: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_prod/" + service_name,
                                wfalgqa003: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + service_name,
                                wfalgqa004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + service_name,
                                wfalgta004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_test/" + service_name
                            };
                            url_secured = urls[server];
                        }
                    }
                }
                else if (security_type === "AD SSO") {
                    var url_split = Ext.getCmp("serviceurlext").getSubmitValue().split("/");

                    url_secured = "/" + url_split[url_split.length - 1];
                    Ext.getCmp("basicAuthConfigurationButton").setHidden(true);
                    Ext.getCmp("onOpenSetAllowedGroups").setHidden(false);
                }
                if (Ext.getCmp("serviceurlsec").getSubmitValue() === "") {
                    Ext.getCmp("serviceurlsec").setValue(url_secured);
                }
                else {
                    if (Ext.getCmp("serviceurlsec").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlAuth()) === -1) {
                        Ext.getCmp("serviceurlsec").setValue(url_secured);
                    }
                    if (Ext.getCmp("serviceurlsec").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlSso()) === -1) {
                        Ext.getCmp("serviceurlsec").setValue(url_secured);
                    }
                }
                if (security_type === "AD SSO" && Ext.getCmp("serviceurlsec").getSubmitValue() === "" && Ext.getCmp("serviceurlext").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlSso()) === -1) {
                    Ext.getCmp("serviceurlext").setValue(url_secured);
                }
                else if (security_type === "Basic Auth" && (Ext.getCmp("serviceurlext").getSubmitValue() === "" || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlAuth()) > -1 || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlSso()) > -1)) {
                    Ext.getCmp("serviceurlext").setValue(DiensteManager.Config.getExterneUrlPrefix() + service_type + "_" + service_name);
                }
            }
            else {
                Ext.getCmp("onOpenSetAllowedGroups").setHidden(true);
                Ext.getCmp("basicAuthConfigurationButton").setHidden(true);
                Ext.getCmp("serviceurlsec").setValue("");
                if (Ext.getCmp("serviceurlext").getSubmitValue() === "" || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlAuth()) > -1 || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(DiensteManager.Config.getSecurityProxyUrlSso()) > -1) {
                    Ext.getCmp("serviceurlext").setValue(DiensteManager.Config.getExterneUrlPrefix() + service_type + "_" + service_name);
                }
            }
        }

        this.enableSaveButton();
    },

    onFieldValueChanged: function () {
        var service_type = Ext.getCmp("servicetype").getSubmitValue(),
            endpoint = "WMSServer";

        if (service_type === "WFS") {
            endpoint = "WFSServer";
        }

        var software = Ext.getCmp("servicesoftware").getSubmitValue(),
            server = Ext.getCmp("serviceserver").getSubmitValue(),
            folder = Ext.getCmp("servicefolder").getSubmitValue(),
            service_name = Ext.getCmp("servicename").getSubmitValue();

        if ((server !== "" && folder !== "" && service_name !== "") && (software === "deegree" || software === "ESRI")) {
            var urls = {
                lgvfds01: "http://lgvfds01.fhhnet.stadt.hamburg.de/arcgis/services/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                lgvfds02: "http://lgvfds02.fhhnet.stadt.hamburg.de/arcgis/services/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                lgvfds04: "http://lgvfds04.fhhnet.stadt.hamburg.de/arcgis/services/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                wfdsepa005: "http://wfdsepa005.dpaorinp.de/arcgis/services/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                wfdsepa006: "http://wfdsepa006.dpaorinp.de/arcgis/services/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                lgvfds03: "http://lgvfds03.fhhnet.stadt.hamburg.de/" + folder + "/services/" + service_name,
                wfalgqa004: "http://wfalgqa004.dpaorinp.de/" + folder + "/services/" + service_name,
                wfalgta004: "http://wfalgta004.dpaorinp.de/" + folder + "/services/" + service_name
            };

            if (urls[server] !== "") {
                Ext.getCmp("serviceurlint").setValue(urls[server]);
            }
            if (Ext.getCmp("serviceurlext").getSubmitValue() === "") {
                Ext.getCmp("serviceurlext").setValue(DiensteManager.Config.getExterneUrlPrefix() + service_type + "_" + service_name);
            }

            if (DiensteManager.Config.getSecurityModul()) {
                var security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();

                if (security_type === "Basic Auth") {
                    Ext.getCmp("onOpenSetAllowedGroups").setHidden(true);
                    Ext.getCmp("basicAuthConfigurationButton").setHidden(false);
                    if (server !== "" && folder !== "" && service_name !== "") {
                        var urls_sec,
                            url_secured;

                        if (software === "deegree" || software === "ESRI") {
                            urls_sec = {
                                lgvfds04: DiensteManager.Config.getSecurityProxyUrlAuth() + "esri_prod/" + folder + "/" + service_name + "/MapServer/" + endpoint,
                                lgvfds03: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_prod/" + folder + "/services/" + service_name,
                                wfalgqa004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + folder + "/services/" + service_name,
                                wfalgta004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_test/" + folder + "/services/" + service_name
                            };
                            url_secured = urls_sec[server];
                        }
                        else if (software === "MapServer") {
                            urls_sec = {
                                lgvfds03: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_prod/" + service_name,
                                wfalgqa004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_qs/" + service_name,
                                wfalgta004: DiensteManager.Config.getSecurityProxyUrlAuth() + "os_test/" + service_name
                            };
                            url_secured = urls_sec[server];
                        }
                        Ext.getCmp("serviceurlsec").setValue(url_secured);
                    }
                }
            }
        }
        else if ((server !== "" && folder !== "" && service_name !== "") && (software === "MapServer")) {
            var mapserver_urls = {
                lgvfds03: "http://lgvfds03.fhhnet.stadt.hamburg.de/" + service_name,
                wfalgqa004: "http://wfalgqa004.dpaorinp.de/" + service_name,
                wfalgta004: "http://wfalgta004.dpaorinp.de/" + service_name
            };

            if (mapserver_urls[server] !== "") {
                Ext.getCmp("serviceurlint").setValue(mapserver_urls[server]);
            }
            if (Ext.getCmp("serviceurlext").getSubmitValue() === "") {
                Ext.getCmp("serviceurlext").setValue(DiensteManager.Config.getExterneUrlPrefix() + service_type + "_" + service_name);
            }
        }
        else if ((server !== "" && folder !== "" && service_name !== "") && (software === "GWC")) {
            var gwc_urls = {
                lgvfds03: "http://lgvfds03.fhhnet.stadt.hamburg.de/geowebcache/service/wms",
                wfalgqa004: "http://wfalgqa004.dpaorinp.de/geowebcache/service/wms",
                wfalgta004: "http://wfalgta004.dpaorinp.de/geowebcache/service/wms"
            };

            if (gwc_urls[server] !== "") {
                Ext.getCmp("serviceurlint").setValue(gwc_urls[server]);
            }
            if (Ext.getCmp("serviceurlext").getSubmitValue() === "") {
                Ext.getCmp("serviceurlext").setValue(DiensteManager.Config.getExterneUrlPrefix() + service_type + "_Cache_NEU");
            }
        }

        var versioncombo = Ext.getCmp("serviceversion"),
            versionstore = DiensteManager.Config.getDienstVersionen()[service_type],
            layerBtn = Ext.getCmp("updatelayer");

        if (versionstore === null || versionstore === undefined) {
            versionstore = [];
        }

        versioncombo.bindStore(versionstore);

        if (service_type !== "WMS" && service_type !== "WFS" && service_type !== "WMTS") {
            layerBtn.setText("Layer editieren");
            layerBtn.setIconCls("right-icon white-icon pictos pictos-compose");
        }
        if (service_type === "WMS" || service_type === "WFS" || service_type === "WMTS") {
            if (layerBtn.text === "Layer editieren") {
                layerBtn.setText("Layer aktualisieren");
                layerBtn.setIconCls("right-icon white-icon pictos pictos-refresh");
            }
        }
        if (service_type === "ATOM") {
            layerBtn.setDisabled(true);
            versioncombo.setDisabled(true);
        }

        this.enableSaveButton();
    },

    enableSaveButton: function () {
        if (!window.read_only) {
            Ext.getCmp("save_service_details_button").setDisabled(false);
        }
    },

    onOnlyInternetChanged: function () {
        var only_internet = Ext.getCmp("serviceonlyinternet").getValue();

        if (only_internet) {
            Ext.getCmp("serviceonlyintranet").setValue(false);
        }

        this.enableSaveButton();
    },

    onOnlyIntranetChanged: function () {
        var only_intranet = Ext.getCmp("serviceonlyintranet").getValue();

        if (only_intranet) {
            Ext.getCmp("serviceonlyinternet").setValue(false);
        }

        this.enableSaveButton();
    },

    onReverseProxyCheckBoxChanged: function (checkbox, newValue) {
        if (checkbox.id === "reverseProxyCheckBox") {

            var servicesoftwareCombo = Ext.getCmp("servicesoftware");
            var serviceurlintTextField = Ext.getCmp("serviceurlint");

            if (newValue) {

                servicesoftwareCombo.setValue("ReverseProxy");

                serviceurlintTextField.setDisabled(false);
                serviceurlintTextField.setFieldLabel("Quell URL");
            }
            else {

                servicesoftwareCombo.setValue("Extern");

                serviceurlintTextField.setValue("");
                serviceurlintTextField.setDisabled(true);
                serviceurlintTextField.setFieldLabel("interne URL");
            }
        }

        this.enableSaveButton();
    },

    onExternalServiceChanged: function (checkbox, newValue) {

        if (checkbox.id === "externalServiceCheckBox") {

            var servicesoftwareCombo = Ext.getCmp("servicesoftware");
            var reverseProxyCheckBox = Ext.getCmp("reverseProxyCheckBox");
            var serviceserverCombo = Ext.getCmp("serviceserver");
            var servicefolderTextField = Ext.getCmp("servicefolder");
            var servicenameTextField = Ext.getCmp("servicename");
            var serviceurlintTextField = Ext.getCmp("serviceurlint");

            if (newValue) {
                // ReverseProxy
                reverseProxyCheckBox.show();
                if (servicesoftwareCombo.getSubmitValue() === "ReverseProxy") {
                    reverseProxyCheckBox.setRawValue(true);
                    serviceurlintTextField.setFieldLabel("Quell URL");
                }
                else {
                    // Software
                    servicesoftwareCombo.setValue("Extern");
                    // interne URL
                    serviceurlintTextField.setValue("");
                    serviceurlintTextField.setDisabled(true);
                }
                servicesoftwareCombo.setDisabled(true);
                // Server
                serviceserverCombo.setDisabled(true);
                serviceserverCombo.clearValue();
                serviceserverCombo.allowBlank = true;
                serviceserverCombo.validateValue(serviceserverCombo.getValue());
                // Ordner / Webapp
                servicefolderTextField.setValue("");
                servicefolderTextField.setDisabled(true);
                // Dienstname / Endpoint
                servicenameTextField.setValue("");
                servicenameTextField.setDisabled(true);
                // Absicherung
                if (DiensteManager.Config.getSecurityModul()) {
                    Ext.getCmp("servicesecuritytype").setDisabled(true);
                    Ext.getCmp("serviceurlsec").setValue("");
                    Ext.getCmp("serviceurlsec").setDisabled(true);
                }
            }
            else {
                var service_id = Ext.getCmp("serviceid").getSubmitValue();
                var service = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data;

                // ReverseProxy
                reverseProxyCheckBox.hide();
                // Software
                servicesoftwareCombo.setDisabled(false);
                servicesoftwareCombo.setValue(service.software);
                servicesoftwareCombo.validateValue(servicesoftwareCombo.getValue());
                // Server
                serviceserverCombo.setDisabled(false);
                serviceserverCombo.allowBlank = false;
                serviceserverCombo.setValue(service.server);
                serviceserverCombo.validateValue(serviceserverCombo.getValue());
                // Ordner / Webapp
                servicefolderTextField.setDisabled(false);
                servicefolderTextField.setValue(service.folder);
                // Dienstname / Endpoint
                servicenameTextField.setDisabled(false);
                servicenameTextField.setValue(service.service_name);
                // interne URL
                serviceurlintTextField.setDisabled(false);
                serviceurlintTextField.setValue(service.url_int);
                // Absicherung
                if (DiensteManager.Config.getSecurityModul()) {
                    Ext.getCmp("servicesecuritytype").setDisabled(false);
                    Ext.getCmp("serviceurlsec").setDisabled(false);
                }
            }
        }

        this.enableSaveButton();
    },

    saveKeywords: function (service_id) {
        var keyword_array = Ext.getCmp("servicekeywords").getValueRecords(),
            keywords = [];

        for (var i = 0; i < keyword_array.length; i++) {
            keywords.push(keyword_array[i].data.text);
        }

        Ext.Ajax.request({
            url: "backend/updatekeywords",
            method: "POST",
            jsonData: {
                service_id: service_id,
                keywords: keywords
            },
            success: function () {
                Ext.getStore("Keywords").reload();
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    saveContacts: function (service_id) {
        var contacts_array = Ext.getCmp("servicecontacts").getValueRecords(),
            contacts = [];

        for (var i = 0; i < contacts_array.length; i++) {
            contacts.push(contacts_array[i].data);
        }

        Ext.Ajax.request({
            url: "backend/updatecontactlinks",
            method: "POST",
            jsonData: {
                service_id: service_id,
                contacts: contacts
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onSearchFreigabe: function () {
        var service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.getStore("LinkedFreigaben").load({
            params: {service_id: service_id},
            callback: function () {
                var store = Ext.getStore("FreigabenList");

                store.removeFilter("searchFilter");

                store.reload();

                Ext.getCmp("search_input").storeId = "FreigabenList";

                var linkFreigabenWindow = Ext.getCmp("linkfreigaben-window");

                if (!linkFreigabenWindow) {
                    linkFreigabenWindow = Ext.create("servicedetails.freigaben.linkfreigaben");
                    linkFreigabenWindow.show();
                }
                else {
                    linkFreigabenWindow.show();
                }
            }
        });
    },

    onDeleteFreigabeLink: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            freigaben_link_id = data.freigaben_link_id,
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/deletefreigabelink",
            method: "POST",
            jsonData: {
                freigaben_link_id: freigaben_link_id,
                service_id: service_id,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onOpenSetAllowedGroups: function () {
        var allowed_groups_store = Ext.getStore("AllowedADGroups"),
            ad_groups_store = Ext.getStore("ADGroups"),
            allowedGroupsChooserWindow = Ext.getCmp("allowedgroupschooser-window"),
            service = Ext.getStore("ServiceList").query("service_id", Ext.getCmp("serviceid").getSubmitValue(), false, false, true).items[0].data;

        allowed_groups_store.removeAll();
        ad_groups_store.removeAll();

        if (service.allowed_groups !== "" && service.allowed_groups) {
            var allowed_groups_json = JSON.parse(service.allowed_groups);

            for (var i = 0; i < allowed_groups_json.length; i++) {
                allowed_groups_store.add({name: allowed_groups_json[i]});
            }
        }

        if (!allowedGroupsChooserWindow) {
            allowedGroupsChooserWindow = Ext.create("allowedgroupschooser.AllowedGroupsChooser");
        }
        allowedGroupsChooserWindow.show();
    },

    onBasicAuthConfiguration: function () {
        if (Ext.getStore("Layers").getCount() > 0) {
            var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
                service_id = Ext.getCmp("serviceid").getSubmitValue(),
                endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();

            controller.getBasicAuthConfiguration(service_id, endpoint_name).then(function () {
                var basicauthconfigurationWindow = Ext.getCmp("basicauthconfiguration-window");

                if (!basicauthconfigurationWindow) {
                    basicauthconfigurationWindow = Ext.create("basicauthconfiguration.BasicAuthConfigurationWindow");
                }
                basicauthconfigurationWindow.show();

                var securityProxyConfigurationStore = Ext.data.StoreManager.lookup("SecurityProxyConfiguration"),
                    endpointNameIndex = securityProxyConfigurationStore.findExact("endpoint_name", Ext.getCmp("servicetitle").getSubmitValue());

                if (endpointNameIndex === -1 && securityProxyConfigurationStore.count() > 0) {
                    Ext.MessageBox.alert("Fehler", "Kein WMS Layer bzw. WFS Feature für die Rolle im SecureAdmin eingetragen.");
                }
            });
        }
        else {
            Ext.Msg.alert("Basic Auth Konfiguration", "Keine Layer für die Basic Auth Konfiguration vorhanden. Layerliste aktualisieren!", Ext.emptyFn);
        }
    },

    getBasicAuthConfiguration: function () {
        var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();

        return controller.getBasicAuthConfiguration(service_id, endpoint_name);
    },

    updateBasicAuthConfiguration: function (params) {
        var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            endpoint_name = Ext.getCmp("servicetitle").getSubmitValue(),
            proxy_url = Ext.getCmp("serviceurlint").getSubmitValue();

        return controller.updateBasicAuthEndpoint(service_id, endpoint_name, proxy_url, params);
    },

    deleteBasicAuthConfiguration: function () {
        var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();

        controller.getBasicAuthConfiguration(service_id, endpoint_name).then(function (securityProxyConfiguration) {
            if (securityProxyConfiguration.hasOwnProperty("endpoint_name")) {
                controller.deleteConfiguration(securityProxyConfiguration).then(function () {
                    Ext.Msg.alert("Hinweis", "Basic Auth Konfiguration wurde gelöscht.", Ext.emptyFn);
                }).catch(function (error) {
                    if (error.status !== 0) {
                        console.log(error);
                    }
                });
                controller.removeBasicAuthConfigurationFromStore(securityProxyConfiguration.endpoint_name);
            }
        });
    }

});

Ext.define("DiensteManager.view.servicedetails.comments.CommentEditor", {
    extend: "Ext.window.Window",
    id: "commenteditor-window",
    alias: "servicedetails.comments.commenteditor",
    height: 400,
    width: 730,
    title: "Kommentar bearbeiten",

    controller: "commenteditor",

    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                id: "commenteditorform",
                items: [{
                    xtype: "htmleditor",
                    id: "commenteditortext",
                    grow: true,
                    labelWidth: 0,
                    height: 350,
                    width: 730,
                    autoscroll: true
                }]
            }
        ];

        this.buttons = [
            {
                text: "Speichern",
                margin: "0 7 0 0",
                listeners: {
                    click: "onSaveComment"
                }
            }
        ];
        this.callParent(arguments);
    }
});

Ext.define("DiensteManager.view.servicedetails.comments.CommentEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.commenteditor",

    onSaveComment: function () {
        var grid = Ext.getCmp("comments-grid"),
            text = Ext.getCmp("commenteditortext").getValue(),
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0];

            Ext.Ajax.request({
                url: "backend/updatecomment",
                method: "POST",
                jsonData: {
                    comment_id: row.data.comment_id,
                    service_id: service_id,
                    author: window.auth_user,
                    post_date: moment().format("YYYY-MM-DD"),
                    text: text
                },
                success: function () {
                    Ext.getStore("Comments").reload({
                        params: {service_id: service_id}
                    });
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else {
            Ext.Ajax.request({
                url: "backend/addcomment",
                method: "POST",
                jsonData: {
                    service_id: service_id,
                    author: window.auth_user,
                    post_date: moment().format("YYYY-MM-DD"),
                    text: text
                },
                success: function () {
                    Ext.getStore("Comments").reload({
                        params: {service_id: service_id}
                    });
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        Ext.getCmp("commenteditor-window").close();
    }
});

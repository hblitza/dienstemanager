Ext.define("DiensteManager.view.servicedetails.ServiceDetails_JiraController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_jiracontroller",

    onDeleteIssue: function (grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex),
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/deleteServiceIssue",
            method: "POST",
            jsonData: {
                id: record.data.id,
                service_id: service_id
            },
            success: function (response) {
                if (response.responseText === "") {
                    Ext.Msg.alert("Jira Ticket", "Fehler: Jira Ticket bereits hinzugefügt?", Ext.emptyFn);
                }
                else {
                    Ext.Msg.alert("Jira Ticket", "Das Jira Ticket wurde erfolgreich entfernt.", Ext.emptyFn);
                    Ext.getStore("JiraTickets").reload({
                        params: {service_id: service_id}
                    });
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onKeyPress: function (field, event) {
        if (event.getKey() === event.ENTER) {
            this.onSearchIssue();
        }
    },

    onSearchIssue: function () {
        var issueKey = Ext.getCmp("issueSearchField").getSubmitValue().toLowerCase(),
            me = this,
            loadingsMask = new Ext.LoadMask({target: Ext.getCmp("servicedetails-window"), msg: "Bitte warten..."});

        loadingsMask.show();

        Ext.Ajax.request({
            url: "backend/searchJiraIssues",
            jsonData: {
                key: issueKey
            },
            success: function (response) {
                var issues = Ext.decode(response.responseText),
                    jiraTicketWindow = Ext.getCmp("jiraticket-window");

                if (issues.error) {
                    loadingsMask.hide();
                    Ext.MessageBox.alert("Fehler", "Es konnten keine JIRA Tickets geladen werden!");
                }
                else {

                    if (jiraTicketWindow) {
                        jiraTicketWindow.destroy();
                    }

                    jiraTicketWindow = Ext.create("jira.JiraTicketWindow");

                    var grid = Ext.getCmp("jiraticket-grid"),
                        jiraticketStore = grid.getStore();

                    jiraticketStore.removeAll();

                    if (issueKey === "") {
                        issues.forEach(function (issue) {
                            me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                        });
                    }
                    else {
                        issues.forEach(function (issue) {
                            if (issue.key.toLowerCase().includes(issueKey) || issue.fields.creator.displayName.toLowerCase().includes(issueKey) || issue.fields.summary.toLowerCase().includes(issueKey)) {
                                me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                            }
                            else {
                                if (issue.fields.assignee) {
                                    if (issue.fields.assignee.displayName.toLowerCase().includes(issueKey)) {
                                        me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                                    }
                                }
                            }
                        });
                    }

                    loadingsMask.hide();

                    if (jiraticketStore.getCount() > 0) {
                        jiraTicketWindow.show();
                    }
                    else {
                        Ext.Msg.alert("Jira Ticket", "Keine passenden Jira Tickets für die Suchanfrage " + Ext.getCmp("issueSearchField").getSubmitValue() + " gefunden.", Ext.emptyFn);
                    }
                }
            },
            failure: function (response) {
                loadingsMask.hide();
                console.log(response);
            }
        });
    },

    onOpenJiraTicketWindow: function () {
        var createJiraTicketWindow = Ext.getCmp("createjiraticket-window");

        if (createJiraTicketWindow) {
            createJiraTicketWindow.destroy();
        }

        createJiraTicketWindow = Ext.create("jira.CreateJiraTicketWindow").show();
    },

    _addIssueToStore: function (jiraticketStore, issue, project) {
        if (issue.fields.assignee) {
            jiraticketStore.add({id: issue.id, key: issue.key, creator: issue.fields.creator.displayName, summary: issue.fields.summary, assignee: issue.fields.assignee.displayName, project: project});
        }
        else {
            jiraticketStore.add({id: issue.id, key: issue.key, creator: issue.fields.creator.displayName, summary: issue.fields.summary, assignee: issue.fields.assignee, project: project});
        }
    }
});

Ext.define("DiensteManager.view.servicedetails.ServiceDetails_LayerSelection", {
    extend: "Ext.window.Window",
    xtype: "layerselection-window",
    id: "layerselection-window",
    alias: "servicedetails.ServiceDetails_LayerSelection",
    controller: "servicedetails_layer",
    title: "Layer Auswahl",
    height: 500,
    width: 500,
    scrollable: true,

    layout: {
        type: "vbox",
        align: "stretch"
    },

    buttons: [{
        text: "Übernehmen",
        handler: "onLayerSelectionDone"
    }],

    items: [
        {
            xtype: "grid",
            id: "select-grouplayer-grid",
            columns: [{
                text: "Gruppen-Layer-Name",
                dataIndex: "layer_name",
                flex: 0.5,
                align: "left"
            }, {
                text: "Gruppen-Layer-Titel",
                dataIndex: "title",
                flex: 0.5,
                align: "left"
            }],
            selModel: {
                selType: "checkboxmodel"
            }
        }, {
            xtype: "grid",
            id: "select-layer-grid",
            columns: [{
                text: "Layer-Name",
                dataIndex: "layer_name",
                flex: 0.5,
                align: "left"
            }, {
                text: "Layer-Titel",
                dataIndex: "title",
                flex: 0.5,
                align: "left"
            }],
            selModel: {
                selType: "checkboxmodel"
            }
        }
    ]
});

Ext.define("DiensteManager.view.servicedetails.allowedgroupschooser.AllowedGroupsChooser", {
    extend: "Ext.window.Window",
    id: "allowedgroupschooser-window",
    alias: "allowedgroupschooser.AllowedGroupsChooser",
    height: 475,
    width: 727,

    title: "Gruppen im AD Suchen und hinzufügen",

    controller: "allowedgroupschooser",

    initComponent: function () {

        this.items = [
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                labelWidth: 0,
                width: "100%",
                items: [
                    {
                        xtype: "textfield",
                        id: "adgroup_search_field",
                        margin: "5 20 5 5",
                        width: 200,
                        tooltip: "Nach Gruppen im AD suchen"
                    }, {
                        xtype: "button",
                        id: "search_adgroup",
                        margin: "5 0 5 0",
                        tooltip: "Suche nach Ad Gruppen starten",
                        iconCls: "right-icon white-icon pictos pictos-search",
                        listeners: {
                            click: "onSearchADGroups"
                        }
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                labelWidth: 0,
                width: "100%",
                items: [
                    {
                        xtype: "grid",
                        store: "ADGroups",
                        id: "adgroups-grid",
                        height: 320,
                        width: 355,
                        border: true,
                        margin: "0 0 0 5",
                        autoScroll: true,
                        columns: [
                            {
                                header: "Suchergebnisse",
                                dataIndex: "name",
                                align: "left",
                                flex: 1
                            }, {
                                xtype: "actioncolumn",
                                header: "+",
                                name: "allowgroup",
                                tooltip: "Die Gruppe wird in die Liste der erlaubten Gruppen hinzugefügt",
                                align: "center",
                                handler: "onAddToAllowedGroups",
                                hidden: window.read_only,
                                width: 27,
                                items: [
                                    {
                                        iconCls: "right-icon pictos pictos-add"
                                    }
                                ]
                            }
                        ],
                        columnLines: true
                    },
                    {
                        xtype: "grid",
                        store: "AllowedADGroups",
                        id: "allowedadgroups-grid",
                        height: 320,
                        width: 355,
                        border: true,
                        margin: "0 0 0 5",
                        autoScroll: true,
                        columns: [
                            {
                                header: "berechtigte Gruppen",
                                dataIndex: "name",
                                align: "left",
                                flex: 1
                            }, {
                                xtype: "actioncolumn",
                                header: "-",
                                name: "disallowgroup",
                                tooltip: "Die Gruppe wird aus der Liste der erlaubten Gruppen gelöscht",
                                align: "center",
                                handler: "onDeleteAllowedGroup",
                                hidden: window.read_only,
                                width: 27,
                                items: [
                                    {
                                        iconCls: "right-icon pictos pictos-minus2"
                                    }
                                ]
                            }
                        ],
                        columnLines: true
                    }
                ]
            }
        ];
        this.buttons = [{
            text: "Speichern",
            tooltip: "Die Liste der erlaubten AD Gruppen wird gespeichert.",
            listeners: {
                click: "onSaveAllowedGroups"
            }
        }];

        this.callParent(arguments);
    }
});

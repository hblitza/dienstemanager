Ext.define("DiensteManager.view.servicedetails.layereditor.LayerEditor", {
    extend: "Ext.window.Window",
    id: "layereditor-window",
    alias: "servicedetails.layereditor.LayerEditor",
    height: 450,
    width: 700,
    title: "Layer editieren",

    requires: [
        "Ext.selection.CellModel"
    ],

    controller: "layereditor",

    listeners: {
        "close": "onClosingWindow"
    },

    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "Layers",
                id: "layereditor-grid",
                height: 405,
                autoScroll: true,
                columnLines: true,
                plugins: {
                    ptype: "cellediting",
                    clicksToEdit: 1
                },
                selModel: {
                    type: "cellmodel"
                },
                viewConfig: {
                    enableTextSelection: true
                },
                columns: [{
                    header: "Datei-Name / typeName",
                    dataIndex: "layer_name",
                    align: "left",
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                }, {
                    header: "Titel",
                    dataIndex: "title",
                    align: "left",
                    flex: 1,
                    editor: {
                        allowBlank: false
                    }
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteRow",
                    items: [{
                        icon: "resources/images/drop-no.png"
                    }]
                }],
                tbar: [{
                    text: "Zeile hinzufügen",
                    tooltip: "Fügt eine leere Zeile am Ende der Tabelle ein",
                    handler: "onAddRow"
                }],
                dockedItems: [{
                    xtype: "toolbar",
                    dock: "bottom",
                    items: [{
                        id: "savelayer",
                        text: "Speichern",
                        margin: "0 0 0 580",
                        width: 100,
                        handler: "onSaveLayer"
                    }]
                }]
            }
        ];

        this.callParent(arguments);
    }
});

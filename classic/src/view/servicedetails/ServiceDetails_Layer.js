Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Layer", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-layer",
    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.tab.*",
        "Ext.layout.container.Border",
        "Ext.panel.Panel",
        "Ext.container.Container",
        "Ext.toolbar.Toolbar"
    ],
    maxWidth: 1570,
    width: Ext.Element.getViewportWidth(),
    maxHeight: 680,
    height: Ext.Element.getViewportHeight() - 90,
    controller: "servicedetails_layer",

    initComponent: function () {
        var dockedItems = [],
            width_inner = Ext.Element.getViewportWidth() / 2,
            layer_details_height = Ext.Element.getViewportHeight() - 180,
            layer_details_max_height = 625,
            form_height = Ext.Element.getViewportHeight(),
            grid_height = 620;

        if (Ext.Element.getViewportWidth() < 1280) {
            width_inner = Ext.Element.getViewportWidth() - 25;
            layer_details_height = Ext.Element.getViewportHeight() - 180;
            layer_details_max_height = Ext.Element.getViewportHeight() - 180;
            form_height = Ext.Element.getViewportHeight() - 90;
            grid_height = 300;
        }
        else if (Ext.Element.getViewportHeight() < 650) {
            layer_details_height = Ext.Element.getViewportHeight() - 140;
            layer_details_max_height = Ext.Element.getViewportHeight() - 140;
            form_height = Ext.Element.getViewportHeight() - 90;
            grid_height = Ext.Element.getViewportHeight() - 140;
        }
        else if (Ext.Element.getViewportWidth() > 1570) {
            width_inner = 785;
        }

        var layerdetails_items = [{
            xtype: "addin_layerdetails",
            width: width_inner,
            maxHeight: layer_details_max_height,
            height: layer_details_height
        }];

        if (DiensteManager.Config.getMetadatenModul()) {
            layerdetails_items.push({
                xtype: "addin_linkedmetadata",
                width: width_inner,
                maxHeight: 625,
                height: Ext.Element.getViewportHeight() - 180
            });

            dockedItems.push({
                id: "linkdatasetbutton",
                iconCls: "right-icon white-icon pictos pictos-attachment",
                text: "Metadaten koppeln",
                width: 150,
                margin: "0 10 0 1",
                disabled: false,
                handler: "onLinkDataset"
            });
        }

        if (DiensteManager.Config.getGekoppelteportaleModul()) {
            layerdetails_items.push({
                xtype: "addin_linkedportals",
                width: width_inner,
                maxHeight: 625,
                height: Ext.Element.getViewportHeight() - 180
            });
        }

        layerdetails_items.push({
            xtype: "addin_jsonpanel",
            width: width_inner,
            maxHeight: 625,
            height: Ext.Element.getViewportHeight() - 180
        });

        dockedItems.push({
            xtype: "splitbutton",
            id: "layertestrequest",
            margin: "0 10 0 0",
            width: 150,
            text: "Testrequest",
            iconCls: "right-icon white-icon pictos pictos-play2",
            disabled: true,
            menu: []
        });

        dockedItems.push({
            xtype: "button",
            id: "layerdeleterequest",
            margin: "0 10 0 0",
            width: 150,
            text: "Löschvermerk",
            iconCls: "right-icon white-icon pictos pictos-delete",
            disabled: window.read_only,
            listeners: {
                click: "onLayerDeleteRequest"
            }
        });

        this.items = [
            {
                xtype: "form",
                layout: "column",
                width: Ext.Element.getViewportWidth(),
                maxWidth: 1570,
                height: form_height,
                autoScroll: true,
                defaults: {
                    xtype: "container",
                    width: width_inner
                },
                items: [
                    {
                        xtype: "layerselection-window"
                    },
                    {
                        items: [
                            {
                                xtype: "fieldcontainer",
                                layout: "hbox",
                                style: "border-bottom: solid #d0d0d0 1px",
                                margin: "0 0 0 0",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "button",
                                        id: "updatelayer",
                                        margin: "10 10 5 10",
                                        width: 150,
                                        tooltip: "Die Capabilities des Dienstes werden abgefragt und die Liste der Layer aktualisiert",
                                        text: "Layer aktualisieren",
                                        iconCls: "right-icon white-icon pictos pictos-refresh",
                                        listeners: {
                                            click: "onUpdateLayerList"
                                        }
                                    },
                                    {
                                        xtype: "button",
                                        id: "getINSPIRECaps",
                                        margin: "10 10 5 0",
                                        width: 150,
                                        text: "Capabilities ausleiten",
                                        tooltip: "Die Capabilities werden um die Informationen aus der Daten-Dienste-Kopplung ergänzt.",
                                        disabled: true,
                                        listeners: {
                                            click: "onInspireCapabilities"
                                        }
                                    },
                                    {
                                        xtype: "button",
                                        id: "layerListToExcel",
                                        width: 150,
                                        margin: "10 10 5 0",
                                        tooltip: "Die Layerliste wird als CSV-Datei ausgegeben",
                                        text: "CSV Download",
                                        listeners: {
                                            click: "onExportLayerlist"
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: "gridpanel",
                                store: "Layers",
                                name: "layersGrid",
                                autoScroll: true,
                                width: width_inner,
                                height: grid_height,
                                id: "layers-grid",
                                viewConfig: {
                                    enableTextSelection: true
                                },
                                columnLines: true,
                                selModel: "rowmodel",
                                listeners: {
                                    cellclick: "onLayerClick",
                                    select: "onSelectLayer"
                                },
                                columns: [
                                    {
                                        dataIndex: "layer_delete_request",
                                        text: "",
                                        width: 37,
                                        align: "center",
                                        renderer: function (value) {
                                            if (value === true) {
                                                return "<img src=\"resources/images/gelb.png\" />";
                                            }
                                            else {
                                                return null;
                                            }
                                        }
                                    },
                                    {
                                        header: "Layer-Name",
                                        dataIndex: "layer_name",
                                        align: "left",
                                        flex: 1,
                                        minWidth: 350
                                    }, {
                                        header: "Layer-Titel",
                                        dataIndex: "title",
                                        align: "left",
                                        flex: 1
                                    }
                                ],
                                dockedItems: [{
                                    xtype: "toolbar",
                                    dock: "top",
                                    items: dockedItems
                                }]
                            }
                        ]
                    },
                    {
                        items: [
                            {
                                xtype: "tabpanel",
                                activeTab: 0,
                                name: "layerdetails",
                                id: "layer-details",
                                items: layerdetails_items,
                                listeners: {
                                    "tabchange": "updateJsonField"
                                }
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});

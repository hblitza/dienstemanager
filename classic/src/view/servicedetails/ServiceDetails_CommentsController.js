Ext.define("DiensteManager.view.servicedetails.ServiceDetails_CommentsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_comments",

    onAddComment: function () {
        var grid = Ext.getCmp("comments-grid"),
            commentEditorWindow = Ext.getCmp("commenteditor-window");

        grid.getSelectionModel().deselectAll();

        if (commentEditorWindow) {
            commentEditorWindow.destroy();
        }

        commentEditorWindow = Ext.create("servicedetails.comments.commenteditor");

        commentEditorWindow.show();
    },

    onDeleteComment: function () {
        var grid = Ext.getCmp("comments-grid"),
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0];

            Ext.Ajax.request({
                url: "backend/deletecomment",
                method: "POST",
                jsonData: {
                    comment_id: row.data.comment_id
                },
                success: function () {
                    Ext.getStore("Comments").reload({
                        params: {service_id: service_id}
                    });
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onEditComment: function () {
        var grid = Ext.getCmp("comments-grid");

        if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0],
                commentEditorWindow = Ext.getCmp("commenteditor-window");

            if (commentEditorWindow) {
                commentEditorWindow.destroy();
            }

            commentEditorWindow = Ext.create("servicedetails.comments.commenteditor");

            commentEditorWindow.show();

            Ext.getCmp("commenteditortext").setValue(row.data.text);
        }
    }
});

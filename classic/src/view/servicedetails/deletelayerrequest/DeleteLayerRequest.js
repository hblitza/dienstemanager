Ext.define("DiensteManager.view.servicedetails.deletelayerrequest.DeleteLayerRequest", {
    extend: "Ext.window.Window",
    id: "deletelayerrequest-window",
    alias: "servicedetails.deletelayerrequest.DeleteLayerRequest",
    height: 300,
    width: 400,
    title: "Layer zum löschen vormerken",
    resizable: false,
    requires: [
        "Ext.selection.CellModel"
    ],
    controller: "deletelayerrequest",
    listeners: {
        beforeRender: "beforeRender"
    },
    items: [
        {
            xtype: "panel",
            id: "deletelayerrequest-panel",
            bodyPadding: 10,
            height: 250,
            autoScroll: true,
            items: [
                {
                    xtype: "component",
                    html: "Bemerkung eingeben (optional):"
                },
                {
                    xtype: "textareafield",
                    grow: false,
                    id: "deletelayerrequest-comment",
                    height: 150,
                    width: "100%"
                }
            ],
            buttons: [
                {
                    text: "Speichern",
                    tooltip: "Es wird ein Vermerk zum Löschen des Layers erstellt und eine Benachrichtigung verschickt.",
                    margin: "0 5 0 10",
                    id: "deletelayerrequest-save",
                    listeners: {
                        click: "onSaveDeleteLayerRequest"
                    }
                },
                {
                    text: "Vermerk löschen",
                    tooltip: "Der Vermerk wird gelöscht.",
                    margin: "0 5 0 10",
                    id: "deletelayerrequest-remove",
                    disabled: true,
                    listeners: {
                        click: "onRemoveDeleteLayerRequest"
                    }
                }
            ]
        }
    ]
});

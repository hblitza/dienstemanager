Ext.define("DiensteManager.view.servicedetails.layermapper.LayerMapperController", {
    extend: "Ext.app.ViewController",
    alias: "controller.layermapper",

    showLayerButtons: function () {
        var newlayergrid = Ext.getCmp("newlayergrid"),
            selectedNewLayergridLength = newlayergrid.getSelectionModel().getSelection().length,
            oldlayergrid = Ext.getCmp("oldlayergrid"),
            selectedOldLayergridLength = oldlayergrid.getSelectionModel().getSelection().length;

        if (selectedNewLayergridLength === 1 && selectedOldLayergridLength === 1) {
            Ext.getCmp("changedLayerUpdateButton").setDisabled(false);
            Ext.getCmp("changedLayerAddButton").setDisabled(true);
        }
        if (selectedNewLayergridLength === 1 && selectedOldLayergridLength === 0) {
            Ext.getCmp("changedLayerAddButton").setDisabled(false);
            Ext.getCmp("changedLayerUpdateButton").setDisabled(true);
        }
        if (selectedNewLayergridLength > 1 || selectedOldLayergridLength > 1) {
            Ext.getCmp("changedLayerAddButton").setDisabled(true);
            Ext.getCmp("changedLayerUpdateButton").setDisabled(true);
        }
        if (selectedNewLayergridLength === 0) {
            Ext.getCmp("changedLayerAddButton").setDisabled(true);
            Ext.getCmp("changedLayerUpdateButton").setDisabled(true);
        }
    },

    onUpdateLayer: function () {
        var service_id = Ext.getCmp("serviceid").getSubmitValue(),
            Layergrid = Ext.getCmp("oldlayergrid"),
            selectedLayergrid = Layergrid.getSelectionModel().getSelection()[0].data,
            layer_id = selectedLayergrid.layer_id,
            ChangedLayergrid = Ext.getCmp("newlayergrid"),
            selectedChangedLayergrid = ChangedLayergrid.getSelectionModel().getSelection()[0].data;

        DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").updateLayer(service_id, layer_id, selectedChangedLayergrid, false);
    },

    onAddLayer: function () {
        var ChangedLayergrid = Ext.getCmp("newlayergrid"),
            service_type = Ext.getCmp("servicetype").getSubmitValue(),
            changedLayergridSelection = ChangedLayergrid.getSelectionModel().getSelection()[0],
            selectedChangedLayergrid = changedLayergridSelection.data,
            loadingsMask = new Ext.LoadMask(Ext.getCmp("layermapper-window"), {msg: "Bitte warten..."});

        loadingsMask.show();

        Ext.Ajax.request({
            url: "backend/addlayer",
            method: "POST",
            jsonData: {
                service_id: selectedChangedLayergrid.service_id,
                service_type: service_type,
                layer_name: selectedChangedLayergrid.layer_name,
                title: selectedChangedLayergrid.title,
                namespace: selectedChangedLayergrid.namespace,
                scale_min: "0",
                scale_max: "2500000",
                output_format: "image/png",
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function () {
                Ext.getStore("NewLayer").remove(changedLayergridSelection);
                Ext.getStore("Layers").reload({
                    params: {service_id: selectedChangedLayergrid.service_id},
                    callback: function () {
                        if (DiensteManager.Config.getSecurityModul()) {
                            var security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();

                            if (security_type === "Basic Auth") {
                                var basicAuthConfigurationCntrl = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration");
                                var endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();

                                service_type = Ext.getCmp("servicetype").getSubmitValue();
                                basicAuthConfigurationCntrl.updateEndpointLayers(endpoint_name, selectedChangedLayergrid.service_id, service_type);
                            }
                        }
                    }
                });
                DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(selectedChangedLayergrid.service_id);
                loadingsMask.hide();
                Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
            },
            failure: function (response) {
                loadingsMask.hide();
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeleteLayer: function (grid, cell) {
        var data = grid.store.getAt(cell).data,
            portalStore = Ext.getStore("Portals").load({
                params: {layer_id: data.layer_id}
            });

        portalStore.on("load", function () {
            if (portalStore.getCount() > 0) {
                var portals = "Folgende Portale sind betroffen:<br/><br/>";

                portalStore.each(function (record) {
                    portals += record.data.title + "<br/>";
                });
                Ext.MessageBox.alert("Löschen nicht möglich!", portals);
            }
            else {
                var mb = Ext.MessageBox;

                mb.buttonText.yes = "ja";
                mb.buttonText.no = "nein";

                mb.confirm("Löschen", "Layer wirklich löschen? Hinweis: Die LayerID geht verloren!", function (btn) {
                    if (btn === "yes") {
                        Ext.Ajax.request({
                            url: "backend/deletelayerfinal",
                            method: "POST",
                            jsonData: {
                                layer_id: data.layer_id,
                                service_id: data.service_id,
                                title: data.title,
                                status: Ext.getCmp("servicestatus").getSubmitValue(),
                                last_edited_by: window.auth_user,
                                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                            },
                            success: function () {
                                Ext.getStore("Layers").reload({
                                    params: {service_id: data.service_id},
                                    callback: function () {
                                        if (DiensteManager.Config.getSecurityModul()) {
                                            var security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();

                                            if (security_type === "Basic Auth") {
                                                var basicAuthConfigurationCntrl = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration");
                                                var endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();
                                                var service_type = Ext.getCmp("servicetype").getSubmitValue();

                                                basicAuthConfigurationCntrl.updateEndpointLayers(endpoint_name, data.service_id, service_type);
                                            }
                                        }
                                    }
                                });
                                Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                                Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
                            },
                            failure: function (response) {
                                console.log(Ext.decode(response.responseText));
                            }
                        });
                    }
                });
            }
        }, this, {single: true});
    }
});

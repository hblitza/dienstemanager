Ext.define("DiensteManager.view.servicedetails.layermapper.LayerMapper", {
    extend: "Ext.window.Window",
    id: "layermapper-window",
    alias: "servicedetails.layermapper.LayerMapper",
    maxHeight: 650,
    height: Ext.Element.getViewportHeight(),
    maxWidth: 1200,
    width: Ext.Element.getViewportWidth(),

    controller: "layermapper",

    title: "Layer aktualisieren",
    layout: {
        type: "column",
        columns: 2,
        tableAttrs: {
            style: {
                width: "100%"
            }
        }
    },

    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                margin: "5 5 5 5",
                colspan: 1,
                columnWidth: 0.5,
                id: "oldlayergrid",
                store: "Layers",
                height: 550,
                name: "layersGrid",
                columnLines: true,
                listeners: {
                    selectionchange: "showLayerButtons"
                },
                selModel: {
                    type: "checkboxmodel",
                    showHeaderCheckbox: false,
                    mode: "SIMPLE"
                },
                columns: [
                    {
                        header: "Layer-ID",
                        dataIndex: "layer_id",
                        sortable: true,
                        align: "center",
                        flex: 10 / 100
                    },
                    {
                        header: "Name",
                        dataIndex: "layer_name",
                        sortable: true,
                        align: "left",
                        flex: 25 / 100
                    }, {
                        header: "Titel",
                        dataIndex: "title",
                        sortable: true,
                        align: "left",
                        flex: 65 / 100
                    }, {
                        xtype: "actioncolumn",
                        header: "Löschen",
                        name: "deletelayer",
                        tooltip: "Der Layer wird unwiederruflich gelöscht!",
                        align: "center",
                        handler: "onDeleteLayer",
                        flex: 20 / 100,
                        items: [
                            {
                                icon: "resources/images/drop-no.png"
                            }
                        ]
                    }
                ]
            }, {
                xtype: "grid",
                margin: "5 5 5 5",
                colspan: 1,
                columnWidth: 0.5,
                id: "newlayergrid",
                height: 550,
                store: "NewLayer",
                columnLines: true,
                listeners: {
                    selectionchange: "showLayerButtons"
                },
                selModel: {
                    type: "checkboxmodel",
                    showHeaderCheckbox: false,
                    mode: "SIMPLE"
                },
                columns: [
                    {
                        id: "changedlayername",
                        header: "Name (neu)",
                        flex: 25 / 100,
                        sortable: true,
                        align: "left",
                        dataIndex: "layer_name"
                    },
                    {
                        id: "changedlayertitle",
                        header: "Titel (neu)",
                        flex: 75 / 100,
                        sortable: true,
                        align: "left",
                        dataIndex: "title"
                    }
                ],
                dockedItems: [{
                    xtype: "toolbar",
                    dock: "bottom",
                    items: [
                        {
                            itemId: "changedLayerUpdateButton",
                            id: "changedLayerUpdateButton",
                            text: "Layer updaten",
                            tooltip: "Der ausgewählte Layer wird mit den Daten des neuen Layers aktualisiert. Die Layer-ID bleibt erhalten!",
                            disabled: true,
                            handler: "onUpdateLayer"
                        }, {
                            itemId: "changedLayerAddButton",
                            id: "changedLayerAddButton",
                            text: "Layer hinzufügen",
                            tooltip: "Der gewählte Layer wird hinzugefüt. Es wird eine neue Layer-ID vergeben.",
                            disabled: true,
                            handler: "onAddLayer"
                        }
                    ]
                }]
            }
        ];
        this.buttons = [{
            text: "Schließen",
            handler: function () {
                Ext.getCmp("layermapper-window").close();
            }
        }];

        this.callParent(arguments);
    }
});

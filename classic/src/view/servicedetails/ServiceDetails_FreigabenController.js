Ext.define("DiensteManager.view.servicedetails.ServiceDetails_FreigabenController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_freigaben",

    onSearchFreigabe: function () {
        var service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.getStore("LinkedFreigaben").load({
            params: {service_id: service_id},
            callback: function () {
                var store = Ext.getStore("FreigabenListe");

                store.removeFilter("searchFilter");

                store.reload();

                var linkFreigabenWindow = Ext.getCmp("linkfreigaben-window");

                if (!linkFreigabenWindow) {
                    linkFreigabenWindow = Ext.create("servicedetails.freigaben.linkfreigaben");
                    linkFreigabenWindow.show();
                }
                else {
                    linkFreigabenWindow.show();
                }
            }
        });
    },

    onDeleteFreigabeLink: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            freigabenlink_id = data.freigabenlink_id;

        Ext.Ajax.request({
            url: "backend/deletefreigabelink",
            method: "POST",
            jsonData: {
                freigabenlink_id: freigabenlink_id
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    }
});

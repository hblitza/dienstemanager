Ext.define("DiensteManager.view.servicedetails.addins.addin_layerdetails", {
    extend: "Ext.form.Panel",
    requires: [
        "Ext.form.*"
    ],
    xtype: "addin_layerdetails",
    title: "weitere Angaben",
    id: "layer-details-form",
    bodyPadding: "5 5 5 5",
    maxHeight: 625,
    autoScroll: true,
    defaultType: "textfield",
    defaults: {
        listeners: {
            change: "enableSaveButton"
        }
    },
    items: [
        {
            fieldLabel: "Layer-ID",
            id: "layerid",
            allowBlank: false,
            readOnly: true,
            tooltip: "ID des Layers",
            labelWidth: 150,
            width: 360
        }, {
            fieldLabel: "Layer-Name",
            id: "layername",
            allowBlank: false,
            readOnly: true,
            tooltip: "Name des Layers",
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Layer-Titel",
            id: "layertitle",
            allowBlank: false,
            readOnly: true,
            tooltip: "Titel des Layers",
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Zusätzliche Kategorien",
            id: "layercategories",
            allowBlank: true,
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Alternativer Titel",
            id: "layeralttitle",
            allowBlank: true,
            labelWidth: 150,
            autoEl: {
                tag: "div",
                "data-qtip": "Wird im Themenbaum anstatt des Layertitels aus den Capabilties angezeigt"
            },
            width: "100%"
        }, {
            fieldLabel: "WFS Namespace",
            id: "layernamespace",
            allowBlank: true,
            readOnly: true,
            labelWidth: 150,
            width: "100%",
            hidden: true
        }, {
            fieldLabel: "WFS Datetime Column",
            id: "layerdatetimecolumn",
            allowBlank: true,
            labelWidth: 150,
            width: "100%",
            hidden: true
        }, {
            xtype: "fieldset",
            title: "GFI Konfiguration",
            defaultType: "textfield",
            defaults: {
                anchor: "100%",
                listeners: {
                    change: "enableSaveButton"
                }
            },
            width: "100%",
            items: [
                {
                    xtype: "combo",
                    fieldLabel: "Format",
                    store: ["", "text/html"],
                    id: "layergfiformat",
                    allowBlank: true,
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Wenn das Feld nicht ausgefüllt wird, wird text/xml verwendet"
                    },
                    labelWidth: 134
                }, {
                    xtype: "numberfield",
                    fieldLabel: "Feature Count",
                    id: "layerfeaturecount",
                    hideTrigger: true,
                    labelWidth: 134,
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Anzahl der maximal abfragbaren Feature bei einem Klick"
                    }
                }, {
                    fieldLabel: "Theme",
                    id: "layergfitheme",
                    hideTrigger: true,
                    labelWidth: 134,
                    maxWidth: 740
                }, {
                    xtype: "fieldset",
                    title: "Theme Parameter (JSON)",
                    padding: "0 5 0 5",
                    cls: "prio-fieldset",
                    collapsible: true,
                    collapsed: true,
                    items: [{
                        xtype: "textareafield",
                        grow: false,
                        id: "layergfithemeparams",
                        height: 80,
                        width: "100%"
                    }],
                    defaults: {
                        listeners: {
                            change: "enableSaveButton"
                        }
                    }
                }, {
                    xtype: "checkboxfield",
                    id: "layergficomplex",
                    fieldLabel: "Komplex",
                    boxLabel: "",
                    labelWidth: 134
                }, {
                    xtype: "checkboxfield",
                    id: "layergfiasnewwindow",
                    fieldLabel: "Als Fenster öffnen",
                    boxLabel: "",
                    labelWidth: 134,
                    listeners: {
                        change: "onGfiAsNewWindow"
                    }
                }, {
                    fieldLabel: "GFI Fenster Parameter",
                    id: "layergfiwindowspecs",
                    hidden: true,
                    hideTrigger: true,
                    labelWidth: 134,
                    maxWidth: 740
                }, {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    items: [
                        {
                            xtype: "checkboxfield",
                            id: "layergfidisable",
                            boxLabel: "GFI deaktivieren",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Wenn angehakt, wird die GFI Konfiguration auf 'ignore' gesetzt"
                            },
                            labelWidth: 100,
                            margin: "0 0 0 138",
                            listeners: {
                                change: "onDisableGFI"
                            }
                        },
                        {
                            xtype: "button",
                            id: "editGFIConfig",
                            text: "Attribute zuordnen",
                            margin: "0 0 0 30",
                            width: 200,
                            listeners: {
                                click: "onEditGFIConfig"
                            }
                        },
                        {
                            xtype: "checkboxfield",
                            id: "layergfibeautifykeys",
                            boxLabel: "GFI verschönern",
                            labelWidth: 100,
                            margin: "0 0 0 30",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Wenn angehakt: Anfangsbuchstabe groß, egal wie in der Attribute Konfiguration angegeben"
                            },
                            listeners: {
                                change: "enableSaveButton"
                            }
                        }
                    ]
                }
            ]
        }, {
            fieldLabel: "Layer-Attribution",
            id: "layerattribution",
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "combo",
            fieldLabel: "Ausgabeformat",
            store: ["XML", "GeoJSON", "image/png", "image/jpeg"],
            id: "layeroutputformat",
            allowBlank: false,
            autoEl: {
                tag: "div",
                "data-qtip": "Standardrückgabeformat bei WFS getFeatureRequests"
            },
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "Gutter (Px)",
            id: "layergutter",
            hidden: true,
            minValue: 0,
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "Transparenz (%)",
            id: "layertransparency",
            minValue: 0,
            maxValue: 100,
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "Tilesize (Px)",
            id: "layertilesize",
            hidden: true,
            minValue: 0,
            step: 128,
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "Min-Scale",
            id: "layerminscale",
            hideTrigger: true,
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "Max-Scale",
            id: "layermaxscale",
            hideTrigger: true,
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "numberfield",
            fieldLabel: "hit Tolerance (Px)",
            id: "layerhittolerance",
            hidden: true,
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Legenden-URL Internet",
            id: "layerlegendurlinternet",
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Legenden-URL Intranet",
            id: "layerlegendurlintranet",
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "combo",
            fieldLabel: "Legenden-Art",
            store: ["statisch", "dynamisch"],
            id: "layerlegendtype",
            labelWidth: 150,
            width: "100%"
        }, {
            fieldLabel: "Legenden-Pfad",
            id: "layerlegendpath",
            labelWidth: 150,
            width: "100%"
        }, {
            xtype: "checkboxfield",
            id: "layersingletile",
            hidden: true,
            fieldLabel: "Singletile",
            boxLabel: "",
            labelWidth: 150
        }, {
            xtype: "checkboxfield",
            id: "layertransparent",
            hidden: true,
            fieldLabel: "Transparent",
            boxLabel: "",
            labelWidth: 150
        }, {
            xtype: "checkboxfield",
            id: "layerurlisvisible",
            fieldLabel: "Service URL sichtbar",
            boxLabel: "",
            labelWidth: 150
        },
        {
            xtype: "checkboxfield",
            id: "layernotsupportedfor3d",
            fieldLabel: "Kein 3D Support",
            boxLabel: "",
            labelWidth: 150,
            hidden: true,
            autoEl: {
                tag: "div",
                "data-qtip": "Häkchen setzen, wenn Layer nicht in 3D Anwendungen benutzbar ist."
            }
        },
        {
            xtype: "fieldset",
            title: "3D Einstellungen",
            id: "layer3dsettings",
            hidden: true,
            defaults: {
                listeners: {
                    change: "enableSaveButton"
                }
            },
            items: [
                {
                    xtype: "checkboxfield",
                    id: "layerrequestvertexnormals",
                    fieldLabel: "requestVertexNormals",
                    boxLabel: "",
                    labelWidth: 150,
                    hidden: true
                }, {
                    xtype: "numberfield",
                    fieldLabel: "maxScreenSpaceError",
                    id: "layermaximumscreenspaceerror",
                    minValue: 0,
                    labelWidth: 150,
                    width: "100%",
                    hidden: true
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Oblique Einstellungen",
            id: "layerobliquesettings",
            hidden: true,
            defaults: {
                listeners: {
                    change: "enableSaveButton"
                }
            },
            items: [
                {
                    xtype: "numberfield",
                    id: "layerhidelevels",
                    fieldLabel: "hideLevels",
                    minValue: 0,
                    labelWidth: 150,
                    width: "100%"
                }, {
                    xtype: "numberfield",
                    id: "layerminzoom",
                    minValue: 0,
                    fieldLabel: "minZoom",
                    labelWidth: 150,
                    width: "100%"
                }, {
                    xtype: "numberfield",
                    id: "layerresolution",
                    fieldLabel: "resolution",
                    minValue: 0,
                    labelWidth: 150,
                    width: "100%"
                }, {
                    xtype: "textfield",
                    id: "layerprojection",
                    fieldLabel: "projection",
                    labelWidth: 150,
                    width: "100%"
                }, {
                    xtype: "textfield",
                    id: "layerterrainurl",
                    fieldLabel: "terrainUrl",
                    labelWidth: 150,
                    width: "100%"
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "SensorThings Einstellungen",
            id: "layersensorthingssettings",
            hidden: true,
            defaults: {
                listeners: {
                    change: "enableSaveButton"
                }
            },
            items: [
                {
                    xtype: "combo",
                    fieldLabel: "URL Param. - Root",
                    store: ["Datastreams", "Things"],
                    id: "layerURLParamsRootEl",
                    allowBlank: false,
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "URL Param. - Filter",
                    id: "layerURLParamsFilter",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "URL Param. - Expand",
                    id: "layerURLParamsExpand",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "EPSG",
                    id: "layerEPSG",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Zu ersetzende LayerIDs",
                    id: "layerRelatedWMSLayerIDs",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Style ID",
                    id: "layerStyleID",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    id: "layerClusterDistance",
                    fieldLabel: "ClusterDistance (Px)",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    id: "layerMouseHoverField",
                    fieldLabel: "MouseHoverField",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textareafield",
                    id: "layerStaTestUrl",
                    fieldLabel: "Test URL",
                    labelWidth: 150,
                    width: "100%",
                    height: 100,
                    grow: true
                },
                {
                    xtype: "checkboxfield",
                    id: "layerLoadThingsOnlyInCurrentExtent",
                    fieldLabel: "Nur Objeckte in aktuellem Ausschnitt laden",
                    boxLabel: "",
                    labelWidth: 150
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Vector Tiles Einstellungen",
            id: "layervectortilessettings",
            hidden: true,
            defaults: {
                listeners: {
                    change: "enableSaveButton"
                }
            },
            items: [
                {
                    xtype: "textfield",
                    fieldLabel: "Extent",
                    id: "layerextent",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Origin",
                    id: "layerorigin",
                    labelWidth: 150,
                    width: "100%"
                },
                {
                    xtype: "textareafield",
                    fieldLabel: "Resolutions",
                    id: "layerresolutions",
                    labelWidth: 150,
                    width: "100%",
                    height: 100,
                    grow: true
                },
                {
                    xtype: "textareafield",
                    fieldLabel: "VT Styles",
                    id: "layervtstyles",
                    labelWidth: 150,
                    width: "100%",
                    height: 100,
                    grow: true
                },
                {
                    xtype: "checkboxfield",
                    fieldLabel: "Visibility",
                    id: "layervisibility",
                    boxLabel: "",
                    labelWidth: 150
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        defaultButtonUI: "default",
        items: [
            "->",
            {
                id: "savelayerdetails",
                text: "Speichern",
                disabled: true,
                listeners: {
                    click: "onSaveLayerDetails"
                }
            }
        ]
    }]
});

Ext.define("DiensteManager.view.servicedetails.addins.addin_metadaten", {
    extend: "Ext.form.FieldContainer",

    requires: [
        "Ext.form.*",
        "Ext.layout.container.HBox"
    ],

    xtype: "addin_metadaten",
    layout: "hbox",
    margin: "0 0 0 0",
    labelClsExtra: "metadata-id-field",

    initComponent: function () {
        this.items = [
            {
                xtype: "textfield",
                maxWidth: 375,
                width: "100%",
                id: "metadata_uuid",
                hidden: true
            },
            {
                xtype: "combo",
                fieldLabel: "Quellkatalog",
                store: "ConfigMetadataCatalogs",
                displayField: "name",
                allowBlank: false,
                id: "servicesourcecswname",
                editable: false,
                margin: "0 10 0 0",
                labelWidth: 100,
                width: 260
            },
            {
                xtype: "button",
                id: "searchMetadata",
                disabled: window.read_only,
                margin: "0 0 0 0",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Abfrage des Metadatenkatalogs via CSW, es wird nach dem eingegebenen Dienst-Titel oder der eingegebenen Metadaten-ID gesucht"
                },
                text: "suchen",
                listeners: {
                    click: "onSearchMetadata"
                }
            }, {
                xtype: "displayfield",
                id: "metadata_uuid_link",
                value: "Keine Metadaten verknüpft",
                margin: "0 10 0 10"
            },
            {
                xtype: "button",
                id: "deleteMetadataCoupling",
                iconCls: "right-icon white-icon pictos pictos-forbidden",
                margin: "0 0 0 0",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Löscht die Verknüpfung auf den Dienstmetadatensatz."
                },
                text: "löschen",
                hidden: window.read_only,
                disabled: true,
                listeners: {
                    click: "onDeleteMetadataCoupling"
                }
            }
        ];
        this.callParent(arguments);
    }
});

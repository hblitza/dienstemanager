Ext.define("DiensteManager.view.servicedetails.addins.addin_jsonpanel", {
    extend: "Ext.panel.Panel",

    requires: [
        "Ext.form.*"
    ],

    xtype: "addin_jsonpanel",

    title: "JSON",
    id: "layer_json_preview",
    items: [
        {
            xtype: "fieldcontainer",
            layout: "hbox",
            items: [
                {
                    xtype: "button",
                    id: "layerjsonupdate",
                    text: "JSON neu erzeugen",
                    margin: "5 5 0 5",
                    width: 230,
                    disabled: true,
                    listeners: {
                        click: "onRecreateJson"
                    }
                },
                {
                    xtype: "button",
                    id: "layerjsonupdateall",
                    text: "JSON neu erzeugen (ganzer Dienst)",
                    margin: "5 5 0 5",
                    width: 230,
                    disabled: true,
                    listeners: {
                        click: "onRecreateJson"
                    }
                }
            ]
        },
        {
            xtype: "textareafield",
            id: "layer_json_field",
            grow: true,
            name: "layer_json_field",
            maxHeight: 575,
            height: Ext.Element.getViewportHeight() - 235,
            width: "100%",
            autoscroll: true
        }
    ]
});

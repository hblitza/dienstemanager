Ext.define("DiensteManager.view.layer.LayerListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.layer_list",

    onLayerDblClick: function (view, td, cellIndex, record) {
        var data = record.data,
            service = Ext.getStore("ServiceList").query("service_id", data.service_id, false, false, true).items[0].data;

        DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").openServiceDetailsWindow(service, data.layer_id);
    },

    onShowLayerClick: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            layer_id = data.layer_id,
            previewURL = DiensteManager.Config.getMapPreview() + "?Map/layerids=" + DiensteManager.Config.getMapPreviewBaseLayerId() + "," + layer_id;

        window.open(previewURL, "_blank");
    },

    onTestInternClick: function (grid, rowIndex) {
        var selectionModel = grid.getSelectionModel();

        grid.getSelectionModel().select(rowIndex);

        if (selectionModel.hasSelection()) {
            var layerData = selectionModel.getSelection()[0];

            if (layerData.data.service_type === "WMS") {
                this.openGetMapTestLinkWindow(layerData, "getMap - intern");
            }
            else if (layerData.data.service_type === "WFS") {
                this.openGetFeatureTestLinkWindow(layerData, "getFeature - intern");
            }
        }
    },

    onTestExternClick: function (grid, rowIndex) {
        var selectionModel = grid.getSelectionModel();

        grid.getSelectionModel().select(rowIndex);

        if (selectionModel.hasSelection()) {
            var layerData = selectionModel.getSelection()[0];

            if (layerData.data.service_type === "WMS") {
                this.openGetMapTestLinkWindow(layerData, "getMap - extern");
            }
            else if (layerData.data.service_type === "WFS") {
                this.openGetFeatureTestLinkWindow(layerData, "getFeature - extern");
            }
        }
    },

    openGetMapTestLinkWindow: function (layerData, req) {
        var getMapTestLinkWindow = Ext.create({
            xtype: "ServiceDetails_Layer_getMap",
            viewModel: {
                data: {
                    layerData: layerData,
                    req: req
                }
            }
        });
        var combo = Ext.getCmp("bbox_getMap");

        combo.select(combo.getStore().getAt(0));
        getMapTestLinkWindow.show();
    },

    openGetFeatureTestLinkWindow: function (layerData, req) {
        var selectedLayer = layerData.data,
            serviceStore = Ext.data.StoreManager.lookup("ServiceList"),
            serviceData,
            i = -1;

        if (selectedLayer.service_title) {
            i = serviceStore.findExact("title", selectedLayer.service_title);
        }
        else if (selectedLayer.service_id) {
            i = serviceStore.findExact("service_id", selectedLayer.service_id);
        }
        if (i !== -1) {
            serviceData = serviceStore.getAt(i).data;
        }

        var maxFeatures = "maxFeatures";

        if (serviceData.version === "2.0.0") {
            maxFeatures = "count";
        }

        var url = "";

        if (req === "getFeature - intern") {
            url = serviceData.url_int + "?SERVICE=WFS&VERSION=" + serviceData.version + "&REQUEST=GetFeature&" + maxFeatures + "=1&typename=" + selectedLayer.layer_name;
        }
        else if (req === "getFeature - extern") {
            url = serviceData.url_ext + "?SERVICE=WFS&VERSION=" + serviceData.version + "&REQUEST=GetFeature&" + maxFeatures + "=1&typename=" + selectedLayer.layer_name;
        }

        var mb = Ext.MessageBox;

        mb.buttonText.yes = "Request ausführen";
        mb.buttonText.no = "Abbrechen";

        mb.confirm("Testrequest", "<input id=\"testRequestInput\" value=\"" + url + "\">", function (btn) {
            if (btn === "yes") {
                window.open(url, "_blank");
            }
            else if (btn === "no") {
                mb.close();
            }
        });
    }
});

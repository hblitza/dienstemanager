Ext.define("DiensteManager.view.serviceimport.ServiceImportWindow", {
    extend: "Ext.window.Window",
    id: "serviceimport-window",
    alias: "serviceimport.ServiceImportWindow",
    controller: "serviceimport",
    padding: "5 0 0 0", // (top, right, bottom, left)
    layout: "fit",
    width: 800,
    height: 300,
    modal: true,
    title: "Dienst Importieren",
    listeners: {
        close: "onWindowClose"
    },
    tbar: [{
        xtype: "combo",
        fieldLabel: "ext. Dienstemanager-Instanz:",
        store: "ConfigDMInstances",
        displayField: "name",
        allowBlank: false,
        id: "serviceSourceCombo",
        editable: false,
        margin: "0 10 0 0",
        labelWidth: 200,
        width: 400,
        listeners: {
            "select": "onNewHostSelected"
        }
    }, {
        xtype: "textfield",
        id: "serviceSearchField",
        width: 200,
        tooltip: "Nach externem Dienst suchen",
        enableKeyEvents: true,
        listeners: {
            "keypress": "onKeyPress"
        }
    }, {
        text: "Dienst suchen",
        handler: "onSearchServiceClicked"
    }],
    items: [
        {
            xtype: "grid",
            id: "found-services-grid",
            autoScroll: true,
            selectable: {
                columns: false,
                extensible: true
            },
            columnLines: true,
            columns: [{
                text: "Service ID",
                dataIndex: "service_id",
                width: 100,
                align: "left"
            }, {
                text: "Titel",
                dataIndex: "title",
                flex: 0.5,
                align: "left"
            }, {
                text: "Dienstname",
                dataIndex: "service_name",
                flex: 0.5,
                align: "left"
            }, {
                text: "Status",
                dataIndex: "status",
                minWidth: 20,
                align: "left"
            }, {
                text: "Bearbeiter",
                dataIndex: "editor",
                minWidth: 50,
                align: "left"
            }],
            store: Ext.create("Ext.data.Store", {
            })
        }
    ],
    buttons: [{
        text: "Ausgewählten Dienst importieren",
        tooltip: "Der ausgewählte Dienst wird importiert.",
        listeners: {
            click: "onImportSelectedService"
        }
    }]
});

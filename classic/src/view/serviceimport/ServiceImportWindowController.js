Ext.define("DiensteManager.view.serviceimport.ServiceImportWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.serviceimport",

    onKeyPress: function (field, event) {
        if (event.getKey() === event.ENTER) {
            this.onSearchServiceClicked();
        }
    },

    onWindowClose: function () {
        var grid = Ext.getCmp("found-services-grid"),
            foundServicesStore = grid.getStore();

        foundServicesStore.removeAll();
    },

    onNewHostSelected: function (combo, record) {
        Ext.Ajax.request({
            url: "backend/resetexternaldminstanceconnection",
            jsonData: {
                host: record.data.host
            }
        });
    },

    onSearchServiceClicked: function () {
        var searchStr = Ext.getCmp("serviceSearchField").getSubmitValue().toLowerCase(),
            serviceSourceCombo = Ext.getCmp("serviceSourceCombo"),
            serviceSourceName = serviceSourceCombo.getSubmitValue(),
            me = this;

        if (serviceSourceName !== "") {
            if (searchStr !== "") {
                me.searchServices(serviceSourceName, searchStr);
            }
            else {
                Ext.Msg.alert("Suche", "Suchfeld muss ausgefüllt werden!", Ext.emptyFn);
            }
        }
        else {
            Ext.Msg.alert("Dienst Import", "Externe Dienstemanager-Instanz auswählen!", Ext.emptyFn);
        }
    },

    searchServices: function (serviceSourceName, searchStr) {
        var me = this,
            serviceSourceCombo = Ext.getCmp("serviceSourceCombo"),
            index = serviceSourceCombo.getStore().findExact("name", serviceSourceName),
            record = serviceSourceCombo.getStore().getAt(index);

        const connection = {
            host: record.data.host,
            port: record.data.port,
            database: record.data.database,
            user: record.data.db_user,
            password: record.data.pw,
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000
        };

        Ext.Ajax.request({
            url: "backend/searchServicesInExternalDMInstance",
            jsonData: {
                searchStr: searchStr,
                connection: connection
            },
            success: function (response) {
                if (response.responseText) {
                    me.addFoundServicesToGrid(response);
                }
                else {
                    Ext.Msg.alert("Suche fehlgeschlagen", "Suche fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Suche fehlgeschlagen", "Suche fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    addFoundServicesToGrid: function (response) {
        var res = Ext.decode(response.responseText),
            grid = Ext.getCmp("found-services-grid"),
            foundServicesStore = grid.getStore();

        foundServicesStore.removeAll();

        if (Array.isArray(res)) {
            if (res.length > 0) {
                res.forEach(function (service) {
                    foundServicesStore.add(service);
                });
            }
            else {
                Ext.Msg.alert("Dienstsuche", "Kein Dienst gefunden", Ext.emptyFn);
            }
        }
        else {
            Ext.Msg.alert("Dienstsuche", "Kein Dienst gefunden", Ext.emptyFn);
        }
    },

    onImportSelectedService: function () {
        var grid = Ext.getCmp("found-services-grid"),
            selectedService = grid.getSelectionModel().getSelection(),
            me = this;

        selectedService.forEach(function (service) {

            var serviceIndex = Ext.getStore("ServiceList").findExact("service_id", service.data.service_id);
            var deletedServiceIndex = Ext.getStore("DeletedServiceList").findExact("service_id", service.data.service_id);

            if (serviceIndex === -1 && deletedServiceIndex === -1) {
                me.importService(service, false);
            }
            else {
                var record = null;
                var mb = Ext.MessageBox;

                if (serviceIndex !== -1) {
                    record = Ext.getStore("ServiceList").getAt(serviceIndex);
                    mb.buttonText.yes = "Neue Dienst ID erzeugen";
                    mb.buttonText.no = "Dienst aktualisieren";
                    mb.confirm("Dienst Importieren", "Ein Dienst mit der ID " + service.data.service_id + " existiert bereits für " + record.data.title + ". Neue IDs für Dienst und Layer erzeugen oder den bestehenden Dienst aktualisieren?", function (btn) {
                        if (btn === "yes") {
                            me.importService(service, true);
                        }
                        if (btn === "no") {
                            me.updateService(service);
                        }
                    });
                }
                if (deletedServiceIndex !== -1) {
                    record = Ext.getStore("DeletedServiceList").getAt(deletedServiceIndex);
                    mb.buttonText.yes = "Neue Dienst ID erzeugen";
                    mb.buttonText.no = "Dienst aktualisieren";
                    mb.confirm("Dienst Importieren", "Ein als gelöscht markierter Dienst mit der ID " + service.data.service_id + " existiert bereits für " + record.data.title + ". Neue IDs für Dienst und Layer erzeugen oder den bestehenden Dienst aktualisieren?", function (btn) {
                        if (btn === "yes") {
                            me.importService(service, true);
                        }
                        if (btn === "no") {
                            me.updateService(service);
                        }
                    });
                }
            }
        });
    },

    updateService: function (service) {
        var me = this;

        Ext.getBody().mask();
        Ext.getCmp("serviceimport-window").mask();
        Ext.Ajax.request({
            url: "backend/updatewithimportedservice",
            jsonData: {
                service_id: service.data.service_id,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function (response) {
                var jsonobj = Ext.decode(response.responseText);

                if (jsonobj.error) {
                    Ext.Msg.alert("Aktualisierung fehlgeschlagen", "Aktualisierung des Dienstes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                    Ext.getBody().unmask();
                    Ext.getCmp("serviceimport-window").unmask();
                }
                else {
                    me.showServiceImportSuccess(jsonobj, service);
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Aktualisierung fehlgeschlagen", "Aktualisierung des Dienstes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
                Ext.getBody().unmask();
                Ext.getCmp("serviceimport-window").unmask();
            }
        });
    },

    importService: function (service, createNewIds) {
        var me = this;

        Ext.getBody().mask();
        Ext.getCmp("serviceimport-window").mask();
        Ext.Ajax.request({
            url: "backend/importservice",
            jsonData: {
                service_id: service.data.service_id,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm"),
                createNewIds: createNewIds
            },
            success: function (response) {
                var jsonobj = Ext.decode(response.responseText);

                if (jsonobj.error) {
                    Ext.Msg.alert("Import fehlgeschlagen", "Import des Dienstes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                    Ext.getBody().unmask();
                    Ext.getCmp("serviceimport-window").unmask();
                }
                else {
                    me.showServiceImportSuccess(jsonobj, service);
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Import fehlgeschlagen", "Import des Dienstes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
                Ext.getBody().unmask();
                Ext.getCmp("serviceimport-window").unmask();
            }
        });
    },

    showServiceImportSuccess: function (res, service) {
        if (res.hasOwnProperty("service_id")) {
            Ext.getStore("ServiceList").reload();
            Ext.Msg.alert("Import erfolgreich", "Import des Dienstes " + service.data.title + " (ID: " + service.data.service_id + ") erfolgreich abgeschlossen!", Ext.emptyFn);

            var serviceImportWindow = Ext.getCmp("serviceimport-window");

            if (!serviceImportWindow) {
                serviceImportWindow = Ext.create("serviceimport.ServiceImportWindow");
            }
            Ext.getCmp("serviceimport-window").unmask();
            serviceImportWindow.close();
        }
        else {
            Ext.Msg.alert("Import fehlgeschlagen", "Import fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
        }
        Ext.getBody().unmask();
    }
});

Ext.define("DiensteManager.view.statistics.Statistics", {
    extend: "Ext.panel.Panel",
    controller: "statistics",
    id: "statistics-view",
    name: "statistics",
    autoScroll: true,
    height: Ext.Element.getViewportHeight() - 70,
    items: [{
        xtype: "tabpanel",
        id: "statisticsTabPanel",
        activeTab: 0,
        defaults: {
            layout: "anchor"
        },
        items: [{
            xtype: "container",
            title: "Anzahl",
            layout: {
                type: "hbox",
                align: "stretch"
            },
            items: [{
                xtype: "panel",
                width: "33%",
                height: Ext.Element.getViewportHeight() - 150,
                items: [{
                    xtype: "polar",
                    id: "serviceStatisticsChart",
                    width: "100%",
                    height: Ext.Element.getViewportHeight() - 190,
                    store: "ServiceStats",
                    insetPadding: 30,
                    innerPadding: 20,
                    theme: "green",
                    legend: {
                        type: "sprite",
                        docked: "bottom"
                    },
                    interactions: ["rotate", "itemhighlight"],
                    series: [{
                        type: "pie",
                        id: "serviceStatisticsPie",
                        animation: {
                            easing: "easeOut",
                            duration: 500
                        },
                        angleField: "count",
                        clockwise: false,
                        highlight: {
                            margin: 20
                        },
                        label: {
                            field: "service_type",
                            display: "rotate",
                            fontSize: 12,
                            renderer: function (item) {
                                var store = Ext.getStore("ServiceStats"),
                                    storeItem = store.findRecord("service_type", item, 0, false, false, true);

                                return storeItem.get("service_type") + " - " + storeItem.get("count");
                            }
                        },
                        style: {
                            strokeStyle: "white",
                            lineWidth: 1
                        },
                        tooltip: {
                            trackMouse: true,
                            renderer: "onserviceStatisticsTooltipRender"
                        }
                    }]
                }],
                dockedItems: [{
                    xtype: "toolbar",
                    id: "ServiceStatsToolbar",
                    dock: "top",
                    items: [{
                        xtype: "combo",
                        store: ["Alle Dienste", "Interne prod. Dienste", "Nur produktive Dienste", "Nur externe Dienste"],
                        value: "Alle Dienste",
                        id: "chooseProdComboService",
                        name: "chooseprodcomboservice",
                        listeners: {
                            change: "onFilterServiceStatistics"
                        }
                    }, {
                        id: "saveServiceStats",
                        text: "Grafik speichern",
                        handler: "onSaveServiceStatisticsPie"
                    }]
                }]
            }, {
                xtype: "panel",
                width: "34%",
                height: Ext.Element.getViewportHeight() - 150,
                items: [{
                    xtype: "polar",
                    id: "softwareStatisticsChart",
                    width: "100%",
                    height: Ext.Element.getViewportHeight() - 190,
                    store: "SoftwareStats",
                    insetPadding: 30,
                    innerPadding: 20,
                    theme: "sky",
                    legend: {
                        type: "sprite",
                        docked: "bottom"
                    },
                    interactions: ["rotate", "itemhighlight"],
                    series: [{
                        type: "pie",
                        id: "softwareStatisticsPie",
                        animation: {
                            easing: "easeOut",
                            duration: 500
                        },
                        angleField: "count",
                        clockwise: false,
                        highlight: {
                            margin: 20
                        },
                        label: {
                            field: "software",
                            display: "rotate",
                            fontSize: 12,
                            renderer: function (item) {
                                var store = Ext.getStore("SoftwareStats"),
                                    storeItem = store.findRecord("software", item, 0, false, false, true);

                                return storeItem.get("software") + " - " + storeItem.get("count");
                            }
                        },
                        style: {
                            strokeStyle: "white",
                            lineWidth: 1
                        },
                        tooltip: {
                            trackMouse: true,
                            renderer: "onSoftwareStatisticsTooltipRender"
                        }
                    }]
                }],
                dockedItems: [{
                    xtype: "toolbar",
                    id: "SoftwareStatsToolbar",
                    dock: "top",

                    items: [{
                        xtype: "combo",
                        store: ["Alle Dienste", "Interne prod. Dienste", "Nur produktive Dienste", "Nur externe Dienste"],
                        value: "Alle Dienste",
                        id: "chooseProdComboSoftware",
                        name: "chooseprodcombosoftware",
                        listeners: {
                            change: "onFilterSoftwareStatistics"
                        }
                    }, {
                        id: "saveSoftwareStats",
                        text: "Grafik speichern",
                        handler: "onSaveSoftwareStatisticsPie"
                    }]
                }]
            }, {
                xtype: "panel",
                width: "33%",
                height: Ext.Element.getViewportHeight() - 150,
                items: [{
                    xtype: "polar",
                    id: "layerStatisticsChart",
                    width: "100%",
                    height: Ext.Element.getViewportHeight() - 190,
                    store: "LayerStats",
                    insetPadding: 30,
                    innerPadding: 20,
                    theme: "red",
                    legend: {
                        type: "sprite",
                        docked: "bottom"
                    },
                    interactions: ["rotate", "itemhighlight"],
                    series: [{
                        type: "pie",
                        title: "Anzahl Layer",
                        id: "layerStatisticsPie",
                        animation: {
                            easing: "easeOut",
                            duration: 500
                        },
                        angleField: "count",
                        clockwise: false,
                        highlight: {
                            margin: 20
                        },
                        label: {
                            field: "service_type",
                            display: "rotate",
                            fontSize: 12,
                            renderer: function (item) {
                                var store = Ext.getStore("LayerStats"),
                                    storeItem = store.findRecord("service_type", item, 0, false, false, true);

                                return storeItem.get("service_type") + " - " + storeItem.get("count");
                            }
                        },
                        style: {
                            strokeStyle: "white",
                            lineWidth: 1
                        },
                        tooltip: {
                            trackMouse: true,
                            renderer: "onlayerStatisticsTooltipRender"
                        }
                    }]
                }],
                dockedItems: [{
                    xtype: "toolbar",
                    id: "LayerStatsToolbar",
                    dock: "top",
                    items: [{
                        xtype: "combo",
                        store: ["Alle Dienste", "Interne prod. Dienste", "Nur produktive Dienste", "Nur externe Dienste"],
                        value: "Alle Dienste",
                        id: "chooseProdComboLayer",
                        name: "chooseprodcombolayer",
                        listeners: {
                            change: "onFilterlayerStatistics"
                        }
                    }, {
                        id: "saveLayerStats",
                        text: "Grafik speichern",
                        handler: "onSavelayerStatisticsPie"
                    }]
                }]
            }]
        }]
    }],
    listeners: {
        "show": "viewIsShown"
    }
});

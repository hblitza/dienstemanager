Ext.define("DiensteManager.view.widget.SearchField", {
    extend: "Ext.form.field.Text",

    xtype: "dmsearchfield",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*"
    ],

    alias: "widget.dmsearchfield",

    triggers: {
        clear: {
            cls: Ext.baseCSSPrefix + "form-clear-trigger",
            handler: function () {
                this.onTrigger1Click();
            }
        },
        search: {
            cls: Ext.baseCSSPrefix + "form-search-trigger",
            handler: function () {
                this.onTrigger2Click();
            }
        }
    },

    hasSearch: false,
    paramName: "query",

    initComponent: function () {
        var me = this;

        me.callParent(arguments);
        me.on("specialkey", function (f, e) {
            if (e.getKey() === e.ENTER) {
                me.onTrigger2Click();
            }
        });

        me.on("change", function () {
            me.onTrigger2Click();
        });
    },

    afterRender: function () {
        this.callParent();
        this.triggerCell.item(0).setDisplayed(false);
    },

    onTrigger1Click: function () {
        var me = this;

        if (me.hasSearch) {
            me.setValue("");
            me.store.removeFilter("searchFilter");
            me.hasSearch = false;
            me.triggerCell.item(0).setDisplayed(false);
            me.updateLayout();
        }
    },

    onTrigger2Click: function () {
        var me = this;

        me.store = Ext.getStore(me.storeId);

        var searchValueMinLength = 0;

        if (me.storeId === "LayerListe") {
            searchValueMinLength = 0;
        }

        me.searchValue = me.getValue().toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        if (me.searchValue.length > searchValueMinLength) {
            var customFilter = new Ext.util.Filter({
                id: "searchFilter",
                filterFn: function (item) {
                    if (item.get("dataset_name") !== undefined && item.get("dataset_shortname") !== undefined) {
                        var dataset_name = item.get("dataset_name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
                        var dataset_shortname = item.get("dataset_shortname").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return dataset_name.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 ||
                            dataset_shortname.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                    else if (item.get("title") !== undefined && item.get("url") !== undefined) {
                        var title = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return title.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                    else if (item.get("title") !== undefined && item.get("service_name") !== undefined) {
                        var title = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue"),
                            service_name = item.get("service_name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue"),
                            url_int = item.get("url_int").toLowerCase(),
                            url_ext = item.get("url_ext").toLowerCase(),
                            service_id = item.get("service_id");

                        return service_name.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 ||
                            title.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 || url_int.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 ||
                            url_ext.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 || service_id === parseInt(me.searchValue);
                    }
                    else if (item.get("layer_id") !== undefined && item.get("name") !== undefined && item.get("title") !== undefined && item.get("service_title") !== undefined) {
                        var title = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue"),
                            name = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue"),
                            layer_id = parseInt(item.get("layer_id")),
                            service_title = item.get("service_title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return layer_id === parseInt(me.searchValue) ||
                            name.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 ||
                            title.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1 ||
                            service_title.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                    else if (item.get("description") !== undefined) {
                        var description = item.get("description").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return description.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                    else if (item.get("surname") !== undefined) {
                        var given_name = item.get("given_name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue"),
                            surname = item.get("surname").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return given_name.indexOf(me.searchValue.toLowerCase()) > -1 || surname.indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                    else if (item.get("datenbezeichnung") !== undefined) {
                        var datenbezeichnung = item.get("datenbezeichnung").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

                        return datenbezeichnung.toLowerCase().indexOf(me.searchValue.toLowerCase()) > -1;
                    }
                }
            });

            me.store.addFilter(customFilter);

            me.hasSearch = true;
            me.triggerCell.item(0).setDisplayed(true);
            me.updateLayout();
        }
        else if (me.searchValue.length === 0) {
            me.onTrigger1Click();
        }
    }
});

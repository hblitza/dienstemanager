Ext.define("DiensteManager.view.editor.ChooseEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.chooseeditor",

    onChooseEditorOk: function () {
        if (Ext.getCmp("chooseeditor_combo").getSubmitValue() !== "") {
            window.auth_user = Ext.getCmp("chooseeditor_combo").getSubmitValue();
            Ext.getCmp("chooseeditor-window").hide();
        }
    },

    beforeRender: function () {
        var editors = DiensteManager.Config.getBearbeiter(),
            combo = Ext.getCmp("chooseeditor_combo");

        combo.setStore(editors);
        combo.select(combo.getStore().getAt(0));
    }
});

Ext.define("DiensteManager.view.datasets.DatasetList", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    id: "datasets_list",

    store: "DatasetList",

    viewConfig: {
        enableTextSelection: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    columns: [
        {
            header: "Datensatzname",
            dataIndex: "dataset_name",
            align: "left",
            flex: 1
        }, {
            header: "Kurzname",
            dataIndex: "dataset_shortname",
            align: "left",
            flex: 1
        }, {
            header: "Metdaten ID",
            dataIndex: "md_id",
            flex: 1,
            align: "center",
            renderer: function (value, metadata, record) {
                return "<a href=\"" + record.get("show_doc_url") + value + "\" target=\"_blank\">" + value + "</a>";
            }
        }, {
            header: "Ressource ID",
            dataIndex: "rs_id",
            hidden: true,
            align: "center",
            flex: 1
        }, {
            header: "OpenData Kategorie",
            dataIndex: "cat_opendata",
            align: "left",
            flex: 1
        }, {
            header: "INSPIRE Kategorie",
            dataIndex: "cat_inspire",
            align: "left",
            flex: 1
        }, {
            header: "HambTG Kategorie",
            dataIndex: "cat_hmbtg",
            align: "left",
            flex: 1
        }, {
            header: "BBOX",
            dataIndex: "bbox",
            align: "left",
            hidden: true,
            flex: 1
        }, {
            header: "Lizenz",
            dataIndex: "fees",
            align: "left",
            flex: 1
        }
    ]
});

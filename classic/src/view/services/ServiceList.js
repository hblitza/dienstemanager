Ext.define("DiensteManager.view.services.ServiceList", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.button.Cycle"
    ],

    id: "service_list",

    store: "ServiceList",

    controller: "service_list",

    listeners: {
        celldblclick: "onServiceDblClick"
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,
    height: Ext.Element.getViewportHeight() - 70,
    loadMask: true,

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    initComponent: function () {
        this.tbar = [];

        if (DiensteManager.Config.getMetadatenModul()) {
            this.tbar.push({
                xtype: "checkbox",
                boxLabel: "Metadaten",
                margin: "0 20px 0 5px",
                handler: "onHasMetadataClick"
            });
        }

        this.tbar.push({
            prependText: "Geprüft: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "geprueft_alle",
                    itemId: "geprueft_alle",
                    checked: true
                },
                {
                    text: "Geprüft",
                    type: "geprueft_geprueft",
                    itemId: "geprueft_geprueft"
                },
                {
                    text: "Ungeprüft",
                    type: "geprueft_ungeprueft",
                    itemId: "geprueft_ungeprueft"
                },
                {
                    text: "Hinweis",
                    type: "geprueft_hinweis",
                    itemId: "geprueft_hinweis"
                },
                {
                    text: "Fehler",
                    type: "geprueft_fehler",
                    itemId: "geprueft_fehler"
                }]
            }
        },
        {
            prependText: "Typ: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: DiensteManager.Config.dienstTypFilterListeGenerieren()
            }
        },
        {
            prependText: "Software: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: DiensteManager.Config.softwareFilterListeGenerieren()
            }
        },
        {
            prependText: "Stelle: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: DiensteManager.Config.responsiblePartyFilterListeGenerieren()
            }
        },
        {
            prependText: "Status: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "status_alle",
                    itemId: "status_alle",
                    checked: true
                },{
                    text: "Produktiv",
                    type: "status_produktiv",
                    itemId: "status_produktiv"
                },{
                    text: "In Bearbeitung",
                    type: "status_inbeabrbeitung",
                    itemId: "status_inbeabrbeitung"
                },{
                    text: "Test",
                    type: "status_test",
                    itemId: "status_test"
                }]
            }
        }
        );

        var koppl_vollst_hidden = true;

        if (DiensteManager.Config.getMetadatenModul()) {
            this.tbar.push({
                prependText: "Gekoppelt: ",
                xtype: "cycle",
                showText: true,
                width: 150,
                margin: "0 5px 0 5px",
                textAlign: "left",
                listeners: {
                    change: "onFilter"
                },
                menu: {
                    items: [{
                        text: "Alle",
                        type: "kopplung_alle",
                        itemId: "kopplung_alle",
                        checked: true
                    },{
                        text: "Ja",
                        type: "kopplung_ja",
                        itemId: "kopplung_ja"
                    },{
                        text: "Nein",
                        type: "kopplung_nein",
                        itemId: "kopplung_nein"
                    }]
                }
            });
            koppl_vollst_hidden = false;
        }

        this.columns = [
            {
                dataIndex: "service_checked_status",
                text: "Geprüft",
                width: 80,
                align: "center",
                renderer: function (value) {
                    if (value === "Fehler") {
                        return "<img src=\"resources/images/rot.png\" />";
                    }
                    else if (value === "Geprüft") {
                        return "<img src=\"resources/images/gruen.png\" />";
                    }
                    else if (value === "Hinweis") {
                        return "<img src=\"resources/images/gelb.png\" />";
                    }
                    else if (value === "Ungeprüft") {
                        return "<img style=\"height: 16px; width:16px\" src=\"resources/images/icon-question.png\" />";
                    }
                }
            },
            {
                dataIndex: "service_type",
                text: "Typ",
                align: "center",
                width: 85
            },
            {
                dataIndex: "software",
                text: "Software",
                align: "center",
                width: 95
            },
            {
                dataIndex: "title",
                text: "Titel",
                flex: 2,
                minWidth: 200,
                align: "left",
                renderer: function (value) {
                    return "<b>" + value + "</b>";
                }
            },
            {
                dataIndex: "responsible_party",
                flex: 0.5,
                text: "Verantwortliche Stelle",
                align: "left"
            },
            {
                dataIndex: "service_name",
                flex: 2,
                text: "Dienstname / Endpoint",
                align: "left",
                hidden: true
            },
            {
                text: "Ordner / Webapp",
                dataIndex: "folder",
                flex: 2,
                align: "left",
                hidden: true
            },
            {
                xtype: "datecolumn",
                text: "Aktualisiert am",
                dataIndex: "last_edit_date",
                format: "Y-m-d",
                align: "center",
                width: 130
            },
            {
                text: "Bearbeiter",
                dataIndex: "editor",
                align: "left",
                width: 145,
                hidden: true
            },
            {
                text: "Status",
                dataIndex: "status",
                width: 120,
                align: "center",
                renderer: function (value, meta, record) {
                    var qa = record.get("qa");

                    if (value === "Produktiv") {
                        if (qa) {
                            meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#A8D44D 50%,#A8D44D 100%);";

                        }
                        else {
                            meta.style = "background-color:#A8D44D;";
                        }
                    }
                    else if (value === "In Bearbeitung") {
                        if (qa) {
                            meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FAD801 50%,#FAD801 100%);";

                        }
                        else {
                            meta.style = "background-color:#FAD801;";
                        }
                    }
                    else if (value === "Test") {
                        if (qa) {
                            meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FF234C 50%,#FF234C 100%);";

                        }
                        else {
                            meta.style = "background-color:#FF234C;";
                        }
                    }
                    return value;
                }
            },
            {
                text: "Kopplung",
                dataIndex: "dsc_complete",
                width: 80,
                align: "center",
                hidden: koppl_vollst_hidden,
                renderer: function (value) {
                    if (value === false) {
                        return "<img src=\"resources/images/rot.png\" />";
                    }
                    else if (value === true) {
                        return "<img src=\"resources/images/gruen.png\" />";
                    }
                }
            },
            {
                text: "INSPIRE-identifiziert?",
                dataIndex: "inspire",
                width: 150,
                align: "center",
                hidden: true,
                renderer: function (value) {
                    if (value === false) {
                        return "<img src=\"resources/images/rot.png\" />";
                    }
                    else if (value === true) {
                        return "<img src=\"resources/images/gruen.png\" />";
                    }
                }
            },
            {
                xtype: "actioncolumn",
                header: "Aktionen",
                name: "aktionen",
                align: "center",
                sortable: false,
                menuDisabled: true,
                width: 110,
                renderer: function (value, metadata, record) {
                    if (record.get("service_type") === "WMS") {
                        this.items[1].icon = "resources/images/info.png";
                        this.items[1].tooltip = "Aufrufen der Capabilities";
                        this.items[1].handler = "onGetCapabilitiesClick";
                        this.items[2].icon = "resources/images/map.png";
                        this.items[2].tooltip = "Alle Layer des Dienstes werden im Vorschauportal geladen";
                        this.items[2].handler = "onMapDownloadClick";
                    }
                    else if (record.get("service_type") === "WMTS") {
                        this.items[1].icon = "resources/images/info.png";
                        this.items[1].tooltip = "Aufrufen der Capabilities";
                        this.items[1].handler = "onGetCapabilitiesClick";
                        this.items[2].icon = "resources/images/not_available.png";
                        this.items[2].tooltip = "Keine Funktion verfügbar";
                    }
                    else if (record.get("service_type") === "WFS") {
                        this.items[1].icon = "resources/images/info.png";
                        this.items[1].tooltip = "Aufrufen der Capabilities";
                        this.items[1].handler = "onGetCapabilitiesClick";
                        this.items[2].icon = "resources/images/download.png";
                        this.items[2].tooltip = "Erzeugen eines GML-Zips";
                        this.items[2].handler = "onMapDownloadClick";
                    }
                    else if (record.get("service_type") === "WPS") {
                        this.items[1].icon = "resources/images/info.png";
                        this.items[1].tooltip = "Aufrufen der Capabilities";
                        this.items[1].handler = "onGetCapabilitiesClick";
                        this.items[2].icon = "resources/images/not_available.png";
                        this.items[2].tooltip = "Keine Funktion verfügbar";
                    }
                    else if (record.get("service_type") === "ATOM") {
                        this.items[1].icon = "resources/images/not_available.png";
                        this.items[1].tooltip = "Keine Funktion verfügbar";
                        this.items[2].icon = "resources/images/rss.png";
                        this.items[2].tooltip = "Atom Feed wird in neuem Tab aufgerufen";
                    }
                    else if (record.get("service_type") === "SensorThings") {
                        this.items[1].icon = "resources/images/info.png";
                        this.items[1].tooltip = "Aufrufen der API";
                        this.items[1].handler = "onGetCapabilitiesClick";
                        this.items[2].icon = "resources/images/map.png";
                        this.items[2].tooltip = "Alle Layer des Dienstes werden im Vorschauportal geladen";
                        this.items[2].handler = "onMapDownloadClick";
                    }
                    else {
                        this.items[1].icon = "resources/images/not_available.png";
                        this.items[1].tooltip = "Keine Funktion verfügbar";
                        this.items[2].icon = "resources/images/not_available.png";
                        this.items[2].tooltip = "Keine Funktion verfügbar";
                    }
                    if (DiensteManager.Config.getEndpointer()) {
                        if (record.get("software") === "deegree") {
                            this.items[0].icon = "resources/images/settings.png";
                            this.items[0].tooltip = "Der Dienst wird im Endpointer geöffnet";
                        }
                        else {
                            this.items[0].icon = "resources/images/not_available.png";
                            this.items[0].tooltip = "Keine Funktion verfügbar";
                        }
                    }
                    else {
                        this.items[0].icon = "resources/images/not_available.png";
                        this.items[0].tooltip = "Keine Funktion verfügbar";
                    }
                },
                items: [
                    {
                        icon: "resources/images/settings.png",
                        tooltip: "Der Dienst wird im Endpointer geöffnet",
                        handler: "onOpenEndpointer"
                    },
                    {
                        icon: "resources/images/info.png",
                        tooltip: "Aufrufen der Capabilities",
                        handler: "onGetCapabilitiesClick"
                    },{
                        icon: "resources/images/map.png",
                        tooltip: "Alle Layer des Dienstes werden im Vorschauportal geladen",
                        handler: "onMapDownloadClick"
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});

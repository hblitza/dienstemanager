Ext.define("DiensteManager.view.services.DeletedServiceListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.deleted_service_list",

    requires: [
        "Ext.list.Tree",
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.window.Window"
    ],

    onFilter: function (btn, menuitem) {
        var type = "",
            filterItem = "";

        if (menuitem.type.indexOf("geprueft") > -1) {
            type = "geprueft";
            filterItem = "service_checked_status";
        }

        if (menuitem.type.indexOf("typ") > -1) {
            type = "typ";
            filterItem = "service_type";
        }

        if (menuitem.type.indexOf("software") > -1) {
            type = "software";
            filterItem = "software";
        }

        if (menuitem.type.indexOf("responsible_party") > -1) {
            type = "responsible_party";
            filterItem = "responsible_party";
        }

        if (menuitem.type.indexOf("kopplung") > -1) {
            type = "kopplung";
            filterItem = "dskopplung_vollst";
            if (menuitem.text === "Nein") {
                menuitem.text = false;
            }
            if (menuitem.text === "Ja") {
                menuitem.text = true;
            }
        }

        if (menuitem.type.indexOf("status") > -1) {
            type = "typ";
            filterItem = "status";
        }

        if (menuitem.type.indexOf("alle") > -1) {
            Ext.getStore("DeletedServiceList").removeFilter("filter_" + type);
        }
        else {
            Ext.getStore("DeletedServiceList").addFilter(new Ext.util.Filter({
                id: "filter_" + type,
                filterFn: function (item) {
                    return item.get(filterItem) === menuitem.text;
                }
            }));
        }

    }
});

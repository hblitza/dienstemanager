Ext.define("DiensteManager.view.services.ServiceListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.service_list",

    requires: [
        "Ext.list.Tree",
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.window.Window"
    ],

    onServiceDblClick: function (view, td, cellIndex, record) {
        this.selected_data = record.data;
        var service = record.data;

        DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").openServiceDetailsWindow(service);
    },

    onHasMetadataClick: function (cb) {

        var checked = cb.getValue();

        if (checked) {
            Ext.getStore("ServiceList").addFilter(new Ext.util.Filter({
                id: "filterImMetadatenkatalog",
                filterFn: function (item) {
                    return item.get("service_md_id") !== "";
                }
            }));

        }
        else {
            Ext.getStore("ServiceList").removeFilter("filterImMetadatenkatalog");
        }
    },

    onFilter: function (btn, menuitem) {
        var type = "",
            filterItem = "";

        if (menuitem.type.indexOf("geprueft") > -1) {
            type = "geprueft";
            filterItem = "service_checked_status";
        }

        if (menuitem.type.indexOf("typ") > -1) {
            type = "typ";
            filterItem = "service_type";
        }

        if (menuitem.type.indexOf("software") > -1) {
            type = "software";
            filterItem = "software";
        }

        if (menuitem.type.indexOf("responsible_party") > -1) {
            type = "responsible_party";
            filterItem = "responsible_party";
        }

        if (menuitem.type.indexOf("kopplung") > -1) {
            type = "kopplung";
            filterItem = "dsc_complete";
            if (menuitem.text === "Nein") {
                menuitem.text = false;
            }
            if (menuitem.text === "Ja") {
                menuitem.text = true;
            }
        }

        if (menuitem.type.indexOf("status") > -1) {
            type = "status";
            filterItem = "status";
        }

        if (menuitem.type.indexOf("alle") > -1) {
            Ext.getStore("ServiceList").removeFilter("filter_" + type);
        }
        else {
            Ext.getStore("ServiceList").addFilter(new Ext.util.Filter({
                id: "filter_" + type,
                filterFn: function (item) {
                    return item.get(filterItem) === menuitem.text;
                }
            }));
        }

    },

    onGetCapabilitiesClick: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            service_type = data.service_type,
            version = data.version;

        if (service_type === "WMS" || service_type === "WFS" || service_type === "WMTS" || service_type === "WPS") {
            var url_int = data.url_int + "?Service=" + service_type + "&Version=" + version + "&Request=GetCapabilities",
                url_ext = data.url_ext + "?Service=" + service_type + "&Version=" + version + "&Request=GetCapabilities",
                mb = Ext.MessageBox;

            mb.buttonText.yes = "Intern";
            mb.buttonText.no = "Extern";

            mb.confirm("GetCapabilities", "interne- oder externe-URL aufrufen?", function (btn) {
                if (btn === "yes") {
                    window.open(url_int, "_blank");
                }
                else if (btn === "no") {
                    if (data.url_ext !== "") {
                        window.open(url_ext, "_blank");
                    }
                }
            });
        }
        else if (service_type === "SensorThings") {
            var url = data.url_ext.replace(/\/$/, "") + "/v" + version;

            window.open(url, "_blank");
        }
    },

    onMapDownloadClick: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            service_type = data.service_type,
            service_name = data.service_name,
            version = data.version,
            url = data.url_int;

        if (service_type === "WMS" || service_type === "SensorThings") {
            var MB = Ext.MessageBox,
                visibilityList = "true,",
                transparencyList = "0,";

            Ext.getStore("Layers").load({
                params: {service_id: data.service_id},
                callback: function (records) {
                    if (records.length > 0) {
                        var titleList = [];

                        for (var i = 0; i < records.length; i++) {
                            titleList.push(records[i].data.title.toUpperCase().replace("Ö", "O").replace("Ü", "U").replace("Ä", "A") + "*#*" + records[i].data.layer_id + "*#*");
                            visibilityList += "true,";
                            transparencyList += records[i].data.transparency + ",";
                        }

                        titleList = titleList.sort().reverse().toString().split("*#*");

                        for (var j = 0; j < titleList.length; j++) {
                            titleList.splice(j + 0, 1);
                        }

                        MB.buttonText.yes = "Einfach";
                        MB.buttonText.no = "Komplex";

                        MB.confirm("Vorschauportal", "Einfachen oder komplexen Themenbaum aufrufen?", function (btn) {
                            if (btn === "yes") {
                                var previewURL = DiensteManager.Config.getMapPreview() + "?Map/layerids=" + DiensteManager.Config.getMapPreviewBaseLayerId() + "," + titleList.toString() + "&visibility=" + visibilityList.substring(0, visibilityList.length - 1) + "&transparency=" + transparencyList.substring(0, transparencyList.length - 1);

                                window.open(previewURL, "_blank");
                            }
                            else if (btn === "no") {
                                var previewURL_complex = DiensteManager.Config.getMapPreview() + "?Map/layerids=" + DiensteManager.Config.getMapPreviewBaseLayerId() + "," + titleList.toString() + "&visibility=" + visibilityList.substring(0, visibilityList.length - 1) + "&transparency=" + transparencyList.substring(0, transparencyList.length - 1) + "&config=" + DiensteManager.Config.getMapPreviewComplexAppend();

                                window.open(previewURL_complex, "_blank");
                            }
                        });
                    }
                    else {
                        MB.alert("Achtung", "Der Dienst enthält keine Layer die im Vorschauportal angezeigt werden könnten!");
                    }
                }
            });
        }
        else if (service_type === "WFS") {
            Ext.getStore("Layers").load({
                params: {service_id: data.service_id}
            });

            var downloaderwindow = Ext.getCmp("downloader-window");

            if (downloaderwindow) {
                downloaderwindow.showDownloader(url, service_name, version);
            }
            else {
                Ext.create("services.Downloader").showDownloader(url, service_name, version);
            }
        }
        else if (service_type === "ATOM") {
            var url_ext = data.url_ext;

            window.open(url_ext, "_blank");
        }
    },

    onOpenEndpointer: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            software = data.software,
            workspace = data.folder,
            endpoint = data.service_name,
            server = data.server;

        if (software === "deegree" && DiensteManager.Config.getEndpointer()) {
            var endpointerURL = DiensteManager.Config.getEndpointer() + "#workspace-view_-_" + server + "_-_" + workspace + "_-_" + endpoint;

            window.open(endpointerURL, "_blank");
        }
    }
});

Ext.define("DiensteManager.view.services.DeletedServiceList", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*",
        "Ext.button.Cycle"
    ],

    id: "deleted_service_list",

    headerBorders: false,
    rowLines: false,
    autoScroll: true,
    height: Ext.Element.getViewportHeight() - 70,
    loadMask: true,

    controller: "deleted_service_list",

    store: "DeletedServiceList",

    viewConfig: {
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    tbar: [
        {
            prependText: "Geprüft: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "geprueft_alle",
                    itemId: "geprueft_alle",
                    checked: true
                }, {
                    text: "Geprüft",
                    type: "geprueft_geprueft",
                    itemId: "geprueft_geprueft"
                }, {
                    text: "Ungeprüft",
                    type: "geprueft_ungeprueft",
                    itemId: "geprueft_ungeprueft"
                }, {
                    text: "Hinweis",
                    type: "geprueft_hinweis",
                    itemId: "geprueft_hinweis"
                }, {
                    text: "Fehler",
                    type: "geprueft_fehler",
                    itemId: "geprueft_fehler"
                }]
            }
        },
        {
            prependText: "Typ: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "typ_alle",
                    itemId: "typ_alle",
                    checked: true
                }, {
                    text: "WMS",
                    type: "typ_wms",
                    itemId: "typ_wms"
                }, {
                    text: "WFS",
                    type: "typ_wfs",
                    itemId: "typ_wfs"
                },
                {
                    text: "ATOM",
                    type: "typ_atom",
                    itemId: "typ_atom"
                },
                {
                    text: "custom",
                    type: "typ_custom",
                    itemId: "typ_custom"
                },
                {
                    text: "WMS-geschützt",
                    type: "typ_wms_geschuetzt",
                    itemId: "typ_wms_geschuetzt"
                }]
            }
        },
        {
            prependText: "Software: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "software_alle",
                    itemId: "software_alle",
                    checked: true
                }, {
                    text: "deegree",
                    type: "software_deegree",
                    itemId: "software_deegree"
                }, {
                    text: "ESRI",
                    type: "software_esri",
                    itemId: "software_esri"
                }, {
                    text: "GWC",
                    type: "software_gwc",
                    itemId: "software_gwc"
                }, {
                    text: "Proxy",
                    type: "software_proxy",
                    itemId: "software_proxy"
                }, {
                    text: "INGRID",
                    type: "software_ingrid",
                    itemId: "software_ingrid"
                }, {
                    text: "3dcitydb",
                    type: "software_3dcitydb",
                    itemId: "software_3dcitydb"
                }]
            }
        },
        {
            prependText: "Status: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "status_alle",
                    itemId: "status_alle",
                    checked: true
                }, {
                    text: "Produktiv",
                    type: "status_produktiv",
                    itemId: "status_produktiv"
                }, {
                    text: "In Bearbeitung",
                    type: "status_inbeabrbeitung",
                    itemId: "status_inbeabrbeitung"
                }, {
                    text: "Test",
                    type: "status_test",
                    itemId: "status_test"
                }]
            }
        },
        {
            prependText: "Stelle: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "responsible_party_alle",
                    itemId: "responsible_party_alle",
                    checked: true
                }, {
                    text: "UDP Webdienste",
                    type: "responsible_party_udp_webdienste",
                    itemId: "responsible_party_udp_webdienste"
                }, {
                    text: "UDP Infrastruktur",
                    type: "responsible_party_udp_infrastruktur",
                    itemId: "responsible_party_udp_infrastruktur"
                }, {
                    text: "UDP Planen und Bauen",
                    type: "responsible_party_udp_planen_und_bauen",
                    itemId: "responsible_party_udp_planen_und_bauen"
                }, {
                    text: "UDH Echtzeitdaten",
                    type: "responsible_party_udh_echtzeitdaten",
                    itemId: "responsible_party_udh_echtzeitdaten"
                }, {
                    text: "GDI-MRH",
                    type: "responsible_party_gdi-mrh",
                    itemId: "responsible_party_gdi-mrh"
                }]
            }
        },
        {
            prependText: "Gekoppelt: ",
            xtype: "cycle",
            showText: true,
            width: 150,
            margin: "0 5px 0 5px",
            textAlign: "left",
            listeners: {
                change: "onFilter"
            },
            menu: {
                items: [{
                    text: "Alle",
                    type: "kopplung_alle",
                    itemId: "kopplung_alle",
                    checked: true
                }, {
                    text: "Ja",
                    type: "kopplung_ja",
                    itemId: "kopplung_ja"
                }, {
                    text: "Nein",
                    type: "kopplung_nein",
                    itemId: "kopplung_nein"
                }]
            }
        }
    ],

    columns: [
        {
            dataIndex: "service_checked_status",
            text: "Geprüft",
            width: 80,
            align: "center",
            renderer: function (value) {
                if (value === "Fehler") {
                    return "<img src=\"resources/images/rot.png\" />";
                }
                else if (value === "Geprüft") {
                    return "<img src=\"resources/images/gruen.png\" />";
                }
                else if (value === "Hinweis") {
                    return "<img src=\"resources/images/gelb.png\" />";
                }
                else if (value === "Ungeprüft") {
                    return "<img style=\"height: 16px; width:16px\" src=\"resources/images/icon-question.png\" />";
                }
            }
        },
        {
            dataIndex: "service_type",
            text: "Typ",
            align: "center",
            width: 80
        },
        {
            dataIndex: "software",
            text: "Software",
            align: "center",
            width: 80
        },
        {
            dataIndex: "title",
            text: "Titel",
            align: "left",
            flex: 2,
            renderer: function (value) {
                return "<b>" + value + "</b>";
            }
        },
        {
            dataIndex: "responsible_party",
            flex: 0.5,
            text: "Verantwortliche Stelle",
            align: "left"
        },
        {
            dataIndex: "service_name",
            flex: 2,
            text: "Dienstname / Endpoint",
            align: "left",
            hidden: true
        },
        {
            text: "Ordner / Webapp",
            dataIndex: "folder",
            flex: 2,
            align: "left",
            hidden: true
        },
        {
            xtype: "datecolumn",
            text: "Aktualisiert am",
            dataIndex: "last_edit_date",
            format: "Y-m-d",
            align: "center",
            width: 120
        },
        {
            text: "Bearbeiter",
            dataIndex: "editor",
            align: "left",
            width: 140,
            hidden: true
        },
        {
            text: "Status",
            dataIndex: "status",
            width: 120,
            align: "center",
            renderer: function (value, meta, record) {
                var qa = record.get("qa");

                if (value === "Produktiv") {
                    if (qa) {
                        meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#A8D44D 50%,#A8D44D 100%);";

                    }
                    else {
                        meta.style = "background-color:#A8D44D;";
                    }
                }
                else if (value === "In Bearbeitung") {
                    if (qa) {
                        meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FAD801 50%,#FAD801 100%);";

                    }
                    else {
                        meta.style = "background-color:#FAD801;";
                    }
                }
                else if (value === "Test") {
                    if (qa) {
                        meta.style = "background: linear-gradient(to right, #FA5882 0%,#FA5882 50%,#FF234C 50%,#FF234C 100%);";

                    }
                    else {
                        meta.style = "background-color:#FF234C;";
                    }
                }
                return value;
            }
        },
        {
            text: "Kopplung vollst.",
            dataIndex: "dskopplung_vollst",
            width: 120,
            align: "center",
            renderer: function (value) {
                if (value === false) {
                    return "<img src=\"resources/images/rot.png\" />";
                }
                else if (value === true) {
                    return "<img src=\"resources/images/gruen.png\" />";
                }
            }
        },
        {
            text: "INSPIRE-identifiziert?",
            dataIndex: "inspire",
            width: 150,
            align: "center",
            hidden: true,
            renderer: function (value) {
                if (value === false) {
                    return "<img src=\"resources/images/rot.png\" />";
                }
                else if (value === true) {
                    return "<img src=\"resources/images/gruen.png\" />";
                }
            }
        }
    ]
});

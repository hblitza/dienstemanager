INSERT INTO deeuser.saii_user (
    user_id,
    username,
    password,
    email
)
VALUES (
    nextval('deeuser.saii_user_seq'),
    ${username},
    ${password},
    ${email}
) 
RETURNING user_id;
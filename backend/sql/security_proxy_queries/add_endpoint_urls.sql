BEGIN TRANSACTION;
INSERT INTO  deeuser.saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${endpoint_url}, 
    nextval('deeuser.saii_endpoint_url_seq')
);
INSERT INTO  deeuser.saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${url_int}, 
    nextval('deeuser.saii_endpoint_url_seq')
)
RETURNING deeuser.saii_endpoint_url.endpoint_id;
END TRANSACTION;
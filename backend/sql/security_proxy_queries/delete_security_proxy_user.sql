DELETE FROM deeuser.saii_user_group WHERE user_id = ${user_id};
DELETE FROM deeuser.saii_user WHERE user_id = ${user_id};

DELETE FROM deeuser.saii_group_role WHERE group_id = ${group_id};
DELETE FROM deeuser.saii_group WHERE group_id = ${group_id};

DELETE FROM deeuser.saii_role_object WHERE role_id = ${role_id};
DELETE FROM deeuser.saii_role WHERE role_id = ${role_id};

UPDATE layers_type_ows SET
    gutter = ${gutter},
    singletile = ${singletile},
    legend_type = ${legend_type},
    legend_path = ${legend_path},
    featurecount = ${featurecount},
    hittolerance = $(hittolerance),
    gfi_format = ${gfi_format},
    gfi_complex = ${gfi_complex},
    namespace = ${namespace},
    datetime_column = ${datetime_column},
    notsupportedfor3d = ${notsupportedfor3d}
WHERE layer_id = ${layer_id};
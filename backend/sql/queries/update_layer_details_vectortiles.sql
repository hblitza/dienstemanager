UPDATE layers_type_vectortiles SET
    extent = ${extent},
    origin = ${origin},
    resolutions = ${resolutions},
    visibility = ${visibility},
    vtstyles = ${vtstyles}
WHERE layer_id = ${layer_id};
INSERT INTO layers (
    layer_name, 
    service_id, 
    title,
    scale_min, 
    scale_max, 
    output_format
)
VALUES (
    ${layer_name}, 
    ${service_id}::integer, 
    ${title},
    ${scale_min}, 
    ${scale_max}, 
    ${output_format}
)
RETURNING layer_id;

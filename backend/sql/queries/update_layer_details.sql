 UPDATE layers SET
    title_alt = ${title_alt},
    layerattribution = ${layerattribution},
    additional_categories = ${additional_categories},
    tilesize = ${tilesize},
    legend_url_internet = ${legend_url_internet},
    legend_url_intranet = ${legend_url_intranet},
    transparent = ${transparent},
    transparency = ${transparency},
    output_format = ${output_format},
    scale_min = ${scale_min},
    scale_max = ${scale_max},
    gfi_theme = ${gfi_theme},
    gfi_theme_params = ${gfi_theme_params},
    gfi_config = ${gfi_config},
    gfi_asnewwindow = ${gfi_asnewwindow},
    gfi_windowspecs = ${gfi_windowspecs},
    service_url_is_visible = ${service_url_is_visible},
    gfi_beautifykeys = ${gfi_beautifykeys}
WHERE layer_id = ${layer_id}
RETURNING layer_id;

DELETE FROM layer_json WHERE layer_id = ${layer_id};
INSERT INTO layer_json(layer_id, service_id, only_intranet, only_internet, status, json_intranet, json_internet, json_intranet_es, json_internet_es)
    VALUES (${layer_id}, ${service_id}, ${only_intranet}, ${only_internet}, ${status}, ${json_intranet}::json, ${json_internet}::json, ${json_intranet_es}::json, ${json_internet_es}::json);
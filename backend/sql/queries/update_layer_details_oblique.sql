UPDATE layers_type_oblique SET
    resolution = ${resolution},
    terrainurl = ${terrainurl},
    projection = ${projection},
    minzoom = ${minzoom},
    hidelevels = ${hidelevels}
WHERE layer_id = ${layer_id};
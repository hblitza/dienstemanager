UPDATE services SET
    update_type=${update_type},
    update_number=${update_number},
    update_unit=${update_unit},
    update_date=${update_date},
    update_process_type=${update_process_type},
    update_process_name=${update_process_name},
    update_process_location=${update_process_location},
    update_process_description=${update_process_description}
 WHERE service_id = ${service_id};

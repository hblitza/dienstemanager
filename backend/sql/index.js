var QueryFile = require("pg-promise").QueryFile,
    path = require("path");

function sql (file) {
    var fullPath = path.join(__dirname, file);

    return new QueryFile(fullPath, {minify: true});
}

module.exports = {
    datasets: {
        add: sql("queries/add_dataset.sql"),
        update: sql("queries/update_dataset.sql"),
        upsert: sql("queries/upsert_dataset.sql")
    },
    layers: {
        add: sql("queries/add_layer.sql"),
        add_ows: sql("queries/add_layer_ows.sql"),
        add_3d: sql("queries/add_layer_3d.sql"),
        add_oblique: sql("queries/add_layer_oblique.sql"),
        add_sensorthings: sql("queries/add_layer_sensorthings.sql"),
        add_vectortiles: sql("queries/add_layer_vectortiles.sql"),
        delete: sql("queries/delete_layer.sql"),
        delete_final: sql("queries/delete_layer_final.sql"),
        delete_json: sql("queries/delete_layer_json.sql"),
        update_details: sql("queries/update_layer_details.sql"),
        update_ows_details: sql("queries/update_layer_details_ows.sql"),
        update_3d_details: sql("queries/update_layer_details_3d.sql"),
        update_oblique_details: sql("queries/update_layer_details_oblique.sql"),
        update_sensorthings_details: sql("queries/update_layer_details_sensorthings.sql"),
        update_vectortiles_details: sql("queries/update_layer_details_vectortiles.sql"),
        update: sql("queries/update_layer.sql"),
        update_json: sql("queries/update_layer_json.sql"),
        update_layerlink: sql("queries/update_layerlink.sql")
    },
    services: {
        update: sql("queries/update_service.sql"),
        update_metadata: sql("queries/update_service_metadata.sql"),
        dataupdate: sql("queries/update_servicedataupdate.sql"),
        add: sql("queries/add_service.sql"),
        delete: sql("queries/delete_service.sql"),
        delete_final: sql("queries/delete_service_final.sql"),
        rollback_delete: sql("queries/rollback_deleted_service.sql")
    },
    contacts: {
        delete: sql("queries/delete_contact.sql"),
        add: sql("queries/add_contact.sql")
    },
    servicetestresults: {
        add: sql("queries/add_servicetestresult.sql"),
        check: sql("queries/check_servicetestresult.sql")
    },
    changelog: {
        add: sql("queries/add_changelog.sql")
    },
    securityproxy: {
        addEndpoint: sql("security_proxy_queries/add_endpoint.sql"),
        addEndpointURLs: sql("security_proxy_queries/add_endpoint_urls.sql"),
        addSecurityProxyUser: sql("security_proxy_queries/add_saii_user.sql"),
        addSecurityProxyGroup: sql("security_proxy_queries/add_saii_group.sql"),
        addSecurityProxyRole: sql("security_proxy_queries/add_saii_role.sql"),
        assignUserGroupRole: sql("security_proxy_queries/assign_user_group_role.sql"),
        deleteSecurityProxyUser: sql("security_proxy_queries/delete_security_proxy_user.sql"),
        deleteEndpoint: sql("security_proxy_queries/delete_endpoint.sql"),
        getUserGroupRole: sql("security_proxy_queries/get_user_group_role.sql"),
        getObjects: sql("security_proxy_queries/get_objects.sql"),
        updateEndpoint: sql("security_proxy_queries/update_endpoint.sql")
    }
};

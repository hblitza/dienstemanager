const config = require("../config.js"),
    debug = require("debug")("DM:checkuser"),
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

async function checkuser (req, res, next) {

    function intersectionObjects (a, b) {
        const results = [];

        for (let i = 0; i < a.length; i++) {
            var existsInB = false;

            for (let j = 0; j < b.length; j++) {
                if (a[i].name === b[j].name) {
                    existsInB = true;
                    break;
                }
            }

            if (existsInB) {
                results.push(a[i]);
            }
        }

        return results;
    }

    if (req.url === "/app") {
        if (config.ldap) {
            var ad = require("./ad_async_caller"),
                accountname;

            if (env !== "dev") {
                accountname = req.headers.remoteuser.split("\\")[1];
            }
            else {
                accountname = config.dev_ad_user;
            }

            var user_query = await ad.findUser(accountname),
                username = user_query.givenName + " " + user_query.sn,
                grouplist = await ad.getGroupMembershipForUser(accountname),
                matches = intersectionObjects(config.ldap.allowed_groups, grouplist),
                read_only = true;

            for (var i = 0; i < matches.length; i++) {
                if (!matches[i].read_only) {
                    read_only = false;
                }
            }

            if (matches.length !== 0) {
                if (config.use_websockets) {
                    res.render("accessgranted", {username: username, readonly: read_only});
                }
                else {
                    res.render("accessgranted_no_ws", {username: username, readonly: read_only});
                }
            }
            else {
                if (config.log_level === "debug") {
                    debug("Keine Gruppenübereinstimmung für User " + username + " (" + accountname + ") gefunden.");
                }
                res.render("norights", {
                    username: username
                });
            }
        }
        else {
            if (config.use_websockets) {
                res.render("accessgranted", {username: "", readonly: false});
            }
            else {
                res.render("accessgranted_no_ws", {username: "", readonly: false});
            }
        }
    }
    else {
        return next();
    }
}

module.exports = checkuser;

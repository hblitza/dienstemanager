var config = require("./../config.js"),
    nodemailer = require("nodemailer"),
    debug = require("debug")("DM:mailer"),
    mailer;

mailer = {
    sendMail: function (mail_to, subject, text) {
        if (config.mail && mail_to) {
            var transporter = nodemailer.createTransport({
                    host: config.mail.smtp_host,
                    port: config.mail.smtp_port,
                    secure: false,
                    auth: null,
                    tls: {
                        rejectUnauthorized: false
                    }
                }),
                mailOptions = {
                    from: config.mail.from,
                    to: mail_to,
                    subject: subject,
                    text: text
                };

            transporter.sendMail(mailOptions, (error) => {
                if (error) {
                    debug("Send Mail", error);
                }
            });
        }
    }
};

module.exports = mailer;

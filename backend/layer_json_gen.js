var database = require("./database"),
    layer_json_gen,
    config = require("./../config.js"),
    debug = require("debug")("DM:layer_json_gen"),
    moment = require("moment"),
    elasticsearch_connector = require("./elasticsearch_connector"),
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

layer_json_gen = {
    prepare: function (service_id, layer_id, md_id, res) {
        var me = this;

        // alle relevanten Daten für alle layer eines speziellen Dienstes holen
        if (service_id !== undefined && layer_id === undefined && md_id === undefined) {
            database.getJsonDataService(service_id).then(
                function (service_data) {
                    if (service_data[0].service_type === "WMS" || service_data[0].service_type === "WFS" || service_data[0].service_type === "WMTS" || service_data[0].service_type === "GeoJSON" || service_data[0].service_type === "Elastic" || service_data[0].service_type === "Terrain3D" || service_data[0].service_type === "TileSet3D" || service_data[0].service_type === "SensorThings" || service_data[0].service_type === "Oblique" || service_data[0].service_type === "VectorTiles") {
                        database.getJsonDataServiceLayer(service_id).then(
                            function (layer_data) {
                                me._service_insert_rec(service_data[0], layer_data, 0, true, res);
                            }
                        );
                    }
                }
            );
        }
        // alle relevanten Daten für einen speziellen Layer holen
        else if (service_id !== undefined && layer_id !== undefined && md_id === undefined) {
            database.getJsonDataService(service_id).then(
                function (service_data) {
                    if (service_data[0].service_type === "WMS" || service_data[0].service_type === "WFS" || service_data[0].service_type === "WMTS" || service_data[0].service_type === "GeoJSON" || service_data[0].service_type === "Elastic" || service_data[0].service_type === "Terrain3D" || service_data[0].service_type === "TileSet3D" || service_data[0].service_type === "SensorThings" || service_data[0].service_type === "Oblique" || service_data[0].service_type === "VectorTiles") {
                        database.getJsonDataLayer(layer_id).then(
                            function (layer_data) {
                                if (layer_data.length > 0) {
                                    database.getJsonDataLayerDataset(layer_data[0].layer_id).then(
                                        function (dataset_data) {
                                            me.insert_json(service_data[0], layer_data[0], dataset_data, true, res);
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );
        }
        // alle relevanten Daten für alle Layer, die mit einem bestimmten Metadatensatz gekoppelt sind holen
        else if (service_id === undefined && layer_id === undefined && md_id !== undefined) {
            database.getLinkedDatasetLayers(md_id).then(
                function (layer_data) {
                    me._layer_insert_rec(layer_data, 0, true);
                }
            );
        }
        // alle Layer JSON Objekte löschen und neu schreiben
        else if (service_id === undefined && layer_id === undefined && md_id === undefined) {
            database.deleteUnusedLayerJSON().then(
                function () {
                    database.getLayers().then(
                        function (layer_data) {
                            me._layer_insert_rec(layer_data, 0, true, res);
                        }
                    );
                }
            );
        }
    },

    _service_insert_rec: function (service_data, layer_data, index, es_update, res) {
        var me = this;

        if (index < layer_data.length) {
            database.getJsonDataLayerDataset(layer_data[index].layer_id).then(
                function (dataset_data) {
                    setTimeout(function () {
                        me.insert_json(service_data, layer_data[index], dataset_data, es_update);
                        var index_new = index + 1;

                        me._service_insert_rec(service_data, layer_data, index_new, es_update, res);
                    }, 50);
                }
            );
        }
        else {
            if (res !== undefined) {
                return res.json({success: true});
            }
        }
    },

    _layer_insert_rec: function (layer_data, index, es_update, res) {
        var me = this;

        if (index < layer_data.length) {
            database.getJsonDataLayerDataset(layer_data[index].layer_id).then(
                function (dataset_data) {
                    database.getJsonDataService(layer_data[index].service_id).then(
                        function (service_data) {
                            setTimeout(function () {
                                if (service_data.length > 0) {
                                    if (service_data[0].service_type === "WMS" || service_data[0].service_type === "WFS" || service_data[0].service_type === "WMTS" || service_data[0].service_type === "GeoJSON" || service_data[0].service_type === "Elastic" || service_data[0].service_type === "Terrain3D" || service_data[0].service_type === "TileSet3D" || service_data[0].service_type === "SensorThings" || service_data[0].service_type === "Oblique" || service_data[0].service_type === "VectorTiles") {
                                        me.insert_json(service_data[0], layer_data[index], dataset_data, es_update);
                                    }
                                }
                                var index_new = index + 1;

                                me._layer_insert_rec(layer_data, index_new, es_update, res);
                            }, 50);
                        }
                    );
                }
            );
        }
        else {
            debug("Layer JSON gen completed");

            database.updateScheduledTaskStatus(["generate_layer_json", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle Layer-JSON-Objekte wurden in der Datenbank neu generiert.", "Erfolgreich"]);

            if (res !== undefined) {
                return res.json({success: true});
            }
        }
    },

    insert_json: function (service_data, layer_data, dataset_data, es_update, res) {
        var output_format = layer_data.output_format,
            gfi_config = "showAll",
            scale_min = layer_data.scale_min,
            scale_max = layer_data.scale_max,
            layerattribution = layer_data.layerattribution,
            legend_url_intranet = layer_data.legend_url_intranet,
            legend_url_internet = layer_data.legend_url_internet,
            hittolerance = layer_data.hittolerance,
            gfi_format = layer_data.gfi_format,
            cache = false,
            namespace = "nicht vorhanden",
            title = layer_data.title_alt !== "" && layer_data.title_alt !== null && layer_data.title_alt !== undefined ? layer_data.title_alt : layer_data.title,
            // Quick Fix: bei Proxy Dienstes sol immer die Proxy URL verwendet werden, auch in der Internet JSON. Ansonsten gibt es Probleme bei fehlenden CORS Settings
            // url_intranet = service_data.software === "Proxy" ? service_data.url_int : service_data.url_ext,
            // url_internet = service_data.software === "ReverseProxy" ? service_data.url_int : service_data.url_ext;
            url_intranet = service_data.url_ext,
            url_internet = service_data.url_ext;

        if (service_data.service_type === "GeoJSON") {
            url_intranet = service_data.url_int + "/" + layer_data.layer_name;
            url_internet = service_data.url_ext + "/" + layer_data.layer_name;
        }

        if (layer_data.output_format === "" || layer_data.output_format === null || layer_data.output_format === undefined) {
            if (service_data.service_type === "WMS" || service_data.service_type === "Oblique") {
                output_format = "image/png";
            }
            else {
                output_format = "XML";
            }
        }
        if (layer_data.gfi_config === null || layer_data.gfi_config === undefined) {
            gfi_config = "showAll";
        }
        else if (layer_data.gfi_config !== null && layer_data.gfi_config !== undefined && this.isJsonString(layer_data.gfi_config)) {
            gfi_config = JSON.parse(layer_data.gfi_config);
        }
        else if (layer_data.gfi_config === "ignore") {
            gfi_config = "ignore";

        }
        if (scale_min === null || scale_min === undefined || scale_min === "") {
            scale_min = "0";
        }
        if (scale_max === null || scale_max === undefined || scale_max === "") {
            scale_max = "2500000";
        }
        if (layerattribution === "" || layerattribution === null || layerattribution === undefined) {
            layerattribution = "nicht vorhanden";
        }
        if (legend_url_intranet === null || legend_url_intranet === undefined) {
            legend_url_intranet = "";
        }
        if (legend_url_internet === null || legend_url_internet === undefined) {
            legend_url_internet = "";
        }
        if (hittolerance === null || hittolerance === undefined && service_data.service_type === "WFS") {
            hittolerance = "";
        }
        if (layer_data.gfi_theme_params !== "" && layer_data.gfi_theme_params !== null && layer_data.gfi_theme_params !== undefined && this.isJsonString(layer_data.gfi_theme_params)) {
            layer_data.gfi_theme = {
                name: layer_data.gfi_theme,
                params: JSON.parse(layer_data.gfi_theme_params)
            };
            if (layer_data.gfi_theme.name === "default" && !layer_data.gfi_beautifykeys) {
                layer_data.gfi_theme.params.beautifyKeys = layer_data.gfi_beautifykeys;
            }
        }
        if (!layer_data.gfi_theme_params && layer_data.gfi_theme) {
            if (layer_data.gfi_theme === "default" && !layer_data.gfi_beautifykeys) {
                layer_data.gfi_theme = {
                    name: layer_data.gfi_theme,
                    params: {
                        beautifyKeys: layer_data.gfi_beautifykeys
                    }
                };
            }
        }
        if (gfi_format === "") {
            gfi_format = "text/xml";
        }
        if (service_data.software === "GWC") {
            cache = true;
        }
        if (service_data.service_type === "WFS") {
            if (layer_data.layer_name.indexOf(":") > 0) {
                layer_data.layer_name = layer_data.layer_name.split(":")[1];
            }
        }
        if (title !== null && title !== undefined) {
            title = title.replace(/_/g, " ");
        }
        else {
            title = "";
        }
        if (layer_data.namespace !== null && layer_data.namespace !== undefined && layer_data.namespace !== "") {
            namespace = layer_data.namespace;
        }
        if (layer_data.service_url_is_visible === null) {
            layer_data.service_url_is_visible = false;
        }
        if (layer_data.rootel === "") {
            layer_data.rootel = "Datastreams";
        }
        if (layer_data.related_wms_layers) {
            layer_data.related_wms_layers = layer_data.related_wms_layers.split(",");
        }
        else {
            layer_data.related_wms_layers = [];
        }
        if (layer_data.mouse_hover_field) {
            if (layer_data.mouse_hover_field.split(",").length > 1) {
                layer_data.mouse_hover_field = layer_data.mouse_hover_field.split(",");
            }
        }

        var json_data = {
                layer_id: layer_data.layer_id,
                title: title,
                url: url_intranet,
                service_type: service_data.service_type,
                layer_name: layer_data.layer_name,
                output_format: output_format,
                version: service_data.version,
                singletile: layer_data.singletile === null ? false : layer_data.singletile,
                transparent: layer_data.transparent,
                transparency: layer_data.transparency,
                service_url_is_visible: layer_data.service_url_is_visible,
                tilesize: layer_data.tilesize,
                gutter: layer_data.gutter,
                namespace: namespace,
                scale_min: scale_min,
                scale_max: scale_max,
                gfi_format: gfi_format,
                gfi_config: gfi_config,
                gfi_theme: layer_data.gfi_theme,
                gfi_complex: layer_data.gfi_complex,
                gfi_asnewwindow: layer_data.gfi_asnewwindow,
                gfi_windowspecs: layer_data.gfi_windowspecs,
                layerattribution: layerattribution,
                legend_url: legend_url_intranet,
                hittolerance: hittolerance,
                cache: cache,
                featurecount: layer_data.featurecount,
                request_vertex_normals: layer_data.request_vertex_normals,
                maximum_screen_space_error: layer_data.maximum_screen_space_error,
                rootel: layer_data.rootel,
                filter: layer_data.filter,
                expand: layer_data.expand,
                related_wms_layers: layer_data.related_wms_layers,
                style_id: layer_data.style_id,
                cluster_distance: layer_data.cluster_distance,
                load_things_only_in_current_extent: layer_data.load_things_only_in_current_extent,
                mouse_hover_field: layer_data.mouse_hover_field,
                epsg: layer_data.epsg,
                hidelevels: layer_data.hidelevels,
                minzoom: layer_data.minzoom,
                projection: layer_data.projection,
                terrainurl: layer_data.terrainurl,
                resolution: layer_data.resolution,
                extent: layer_data.extent,
                origin: layer_data.origin,
                resolutions: layer_data.resolutions,
                visibility: layer_data.visibility,
                vtstyles: layer_data.vtstyles,
                notsupportedfor3d: layer_data.notsupportedfor3d

            },
            json_intranet_data = JSON.parse(JSON.stringify(json_data)),
            json_internet_data = JSON.parse(JSON.stringify(json_data)),
            json_intranet_data_es = JSON.parse(JSON.stringify(json_data)),
            json_internet_data_es = JSON.parse(JSON.stringify(json_data));

        json_intranet_data.datasets = this.generateDatasetJsonString(dataset_data, false);
        json_internet_data.url = url_internet;
        json_internet_data.datasets = this.generateDatasetJsonStringInternet(dataset_data, false);
        json_internet_data.legend_url = legend_url_internet;
        json_intranet_data_es.datasets = this.generateDatasetJsonString(dataset_data, true);
        json_internet_data_es.url = url_internet;
        json_internet_data_es.datasets = this.generateDatasetJsonStringInternet(dataset_data, true);

        var params = {
            layer_id: layer_data.layer_id,
            service_id: service_data.service_id,
            only_intranet: service_data.only_intranet,
            only_internet: service_data.only_internet,
            status: service_data.status,
            json_intranet: this.jsonTemplates(service_data.service_type, json_intranet_data).replace(/&quot;/g, "\""),
            json_internet: this.jsonTemplates(service_data.service_type, json_internet_data).replace(/&quot;/g, "\""),
            json_intranet_es: this.jsonTemplates(service_data.service_type, json_intranet_data_es).replace(/&quot;/g, "\""),
            json_internet_es: this.jsonTemplates(service_data.service_type, json_internet_data_es).replace(/&quot;/g, "\"")
        };

        database.updateLayerJSON(params).then(function () {
            if (res !== undefined) {
                return res.json({success: true});
            }
        });

        if (es_update && config.es_params !== undefined && config.es_params !== null && env !== "dev") {
            elasticsearch_connector.updateIndex(service_data, layer_data, params);
        }
    },

    generateDatasetJsonString: function (datasets, es_param) {
        var datasets_json = [];
        var elastic = es_param;

        for (var i = 0; i < datasets.length; i++) {
            var kategorie_opendata = [],
                kategorie_organisation = "",
                kategorie_inspire;

            if (datasets[i].cat_opendata.length > 0) {
                kategorie_opendata = datasets[i].cat_opendata.split("|");
            }
            if (datasets[i].cat_org !== null && datasets[i].cat_org !== undefined) {
                kategorie_organisation = datasets[i].cat_org;
            }
            if (datasets[i].cat_inspire.length === 0) {
                kategorie_inspire = ["kein INSPIRE-Thema"];
            }
            else {
                kategorie_inspire = datasets[i].cat_inspire.split("|");
            }

            if (!elastic) {
                datasets_json.push(
                    {
                        md_id: datasets[i].md_id,
                        csw_url: datasets[i].source_csw,
                        show_doc_url: datasets[i].show_doc_url,
                        rs_id: datasets[i].rs_id,
                        md_name: datasets[i].dataset_name,
                        bbox: datasets[i].bbox,
                        kategorie_opendata: kategorie_opendata,
                        kategorie_inspire: kategorie_inspire,
                        kategorie_organisation: kategorie_organisation
                    }
                );
            }
            else {
                datasets_json.push(
                    {
                        md_id: datasets[i].md_id,
                        csw_url: datasets[i].source_csw,
                        show_doc_url: datasets[i].show_doc_url,
                        rs_id: datasets[i].rs_id,
                        md_name: datasets[i].dataset_name,
                        keywords: datasets[i].keywords,
                        description: datasets[i].description,
                        bbox: datasets[i].bbox,
                        kategorie_opendata: kategorie_opendata,
                        kategorie_inspire: kategorie_inspire,
                        kategorie_organisation: kategorie_organisation
                    }
                );
            }
        }
        return datasets_json;
    },

    generateDatasetJsonStringInternet: function (datasets, es_param) {

        var datasets_json = [];
        var elastic = es_param;

        for (var i = 0; i < datasets.length; i++) {
            var kategorie_opendata = [],
                source_csw = datasets[i].source_csw,
                show_doc_url = datasets[i].show_doc_url,
                kategorie_organisation = "",
                kategorie_inspire;

            if (datasets[i].cat_opendata.length > 0) {
                kategorie_opendata = datasets[i].cat_opendata.split("|");
            }
            if (datasets[i].cat_org !== null && datasets[i].cat_org !== undefined) {
                kategorie_organisation = datasets[i].cat_org;
            }
            if (datasets[i].cat_inspire.length === 0) {
                kategorie_inspire = ["kein INSPIRE-Thema"];
            }
            else {
                kategorie_inspire = datasets[i].cat_inspire.split("|");
            }

            if (datasets[i].source_csw === "https://hmdk.metaver.de/csw") {
                source_csw = "https://metaver.de/csw";
                show_doc_url = "https://metaver.de/trefferanzeige?cmd=doShowDocument&docuuid=";
            }

            if (!elastic) {
                datasets_json.push(
                    {
                        md_id: datasets[i].md_id,
                        csw_url: source_csw,
                        show_doc_url: show_doc_url,
                        rs_id: datasets[i].rs_id,
                        md_name: datasets[i].dataset_name,
                        bbox: datasets[i].bbox,
                        kategorie_opendata: kategorie_opendata,
                        kategorie_inspire: kategorie_inspire,
                        kategorie_organisation: kategorie_organisation
                    }
                );
            }
            else {
                datasets_json.push(
                    {
                        md_id: datasets[i].md_id,
                        csw_url: source_csw,
                        show_doc_url: show_doc_url,
                        rs_id: datasets[i].rs_id,
                        md_name: datasets[i].dataset_name,
                        keywords: datasets[i].keywords,
                        description: datasets[i].description,
                        bbox: datasets[i].bbox,
                        kategorie_opendata: kategorie_opendata,
                        kategorie_inspire: kategorie_inspire,
                        kategorie_organisation: kategorie_organisation
                    }
                );
            }

        }

        return datasets_json;
    },

    jsonTemplates: function (type, params) {
        var reg = /(?<=}).*?,/g,
            vtStyles,
            extent,
            origin,
            resolutions;

        if (params.vtstyles) {
            vtStyles = params.vtstyles.split(reg).map(vtStyles => JSON.parse(vtStyles));
        }

        if (params.extent) {
            extent = params.extent.split(",").map(Number);
        }

        if (params.origin) {
            origin = params.origin.split(",").map(Number);
        }

        if (params.resolutions) {
            resolutions = params.resolutions.split(",").map(Number);
        }

        var templates = {
                WMS: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    layers: params.layer_name,
                    format: params.output_format,
                    version: params.version,
                    singleTile: params.singletile,
                    transparent: params.transparent,
                    transparency: params.transparency,
                    urlIsVisible: params.service_url_is_visible,
                    tilesize: params.tilesize,
                    gutter: params.gutter,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    infoFormat: params.gfi_format,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    gfiComplex: params.gfi_complex,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    cache: params.cache,
                    featureCount: params.featurecount,
                    datasets: params.datasets,
                    notSupportedIn3D: params.notsupportedfor3d
                },
                WMTS: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    layers: params.layer_name,
                    format: params.output_format,
                    version: params.version,
                    singleTile: params.singletile,
                    transparent: params.transparent,
                    transparency: params.transparency,
                    urlIsVisible: params.service_url_is_visible,
                    tilesize: params.tilesize,
                    gutter: params.gutter,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    infoFormat: params.gfi_format,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    gfiComplex: params.gfi_complex,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    cache: params.cache,
                    featureCount: params.featurecount,
                    datasets: params.datasets
                },
                WFS: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    featureType: params.layer_name,
                    outputFormat: params.output_format,
                    version: params.version,
                    featureNS: params.namespace,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    gfiComplex: params.gfi_complex,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    hitTolerance: params.hittolerance,
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                GeoJSON: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    format: params.output_format,
                    version: params.version,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    gfiComplex: params.gfi_complex,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                Elastic: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typeName: params.layer_name,
                    typ: params.service_type,
                    version: params.version,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    gfiComplex: params.gfi_complex,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                Terrain3D: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    cesiumTerrainProviderOptions: {
                        requestVertexNormals: params.request_vertex_normals
                    },
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                TileSet3D: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    gfiAttributes: params.gfi_config,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    cesium3DTilesetOptions: {
                        maximumScreenSpaceError: params.maximum_screen_space_error
                    },
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                SensorThings: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    version: params.version,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    gfiAttributes: params.gfi_config,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    datasets: params.datasets,
                    urlParameter: {
                        root: params.rootel,
                        filter: params.filter,
                        expand: params.expand
                    },
                    related_wms_layers: params.related_wms_layers,
                    epsg: params.epsg,
                    gfiTheme: params.gfi_theme,
                    styleId: params.style_id,
                    clusterDistance: params.cluster_distance,
                    loadThingsOnlyInCurrentExtent: params.load_things_only_in_current_extent,
                    mouseHoverField: params.mouse_hover_field,
                    urlIsVisible: params.service_url_is_visible
                },
                Oblique: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    resolution: params.resolution,
                    hideLevels: params.hidelevels,
                    minZoom: params.minzoom,
                    terrainUrl: params.terrainurl,
                    projection: params.projection,
                    gfiAttributes: params.gfi_config,
                    layerAttribution: params.layerattribution,
                    legendURL: params.legend_url,
                    datasets: params.datasets,
                    urlIsVisible: params.service_url_is_visible
                },
                VectorTiles: {
                    id: String(params.layer_id),
                    name: params.title,
                    url: params.url,
                    typ: params.service_type,
                    epsg: params.epsg,
                    extent: extent,
                    origin: origin,
                    resolutions: resolutions,
                    tileSize: params.tilesize,
                    transparency: params.transparency,
                    visibility: params.visibility,
                    minScale: params.scale_min,
                    maxScale: params.scale_max,
                    legendURL: params.legend_url,
                    gfiAttributes: params.gfi_config,
                    gfiTheme: params.gfi_theme,
                    vtStyles: vtStyles
                }
            },
            template_data = templates[type];

        if (params.gfi_asnewwindow) {
            template_data.gfiAsNewWindow = {
                name: "_blank",
                specs: params.gfi_windowspecs
            };
        }

        if (!params.gfi_complex) {
            delete template_data.gfiComplex;
        }
        if (!params.gfi_format || params.gfi_format === "" || params.gfi_config === "ignore") {
            delete template_data.infoFormat;
        }
        if (!params.cluster_distance) {
            delete template_data.clusterDistance;
        }
        if (!params.related_wms_layers || params.related_wms_layers.length === 0) {
            delete template_data.related_wms_layers;
        }
        if (!params.mouse_hover_field || params.mouse_hover_field.length === 0) {
            delete template_data.mouseHoverField;
        }
        if (!params.style_id || params.style_id === "") {
            delete template_data.styleId;
        }

        return JSON.stringify(template_data);
    },

    isJsonString: function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    }
};
module.exports = layer_json_gen;

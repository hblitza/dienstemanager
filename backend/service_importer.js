var pgpromise = require("pg-promise")(/* options */),
    debug = require("debug")("DM:service_importer");

var db = null;
var connection = null;

const service_importer = {

    dbConnect: function (connection_params) {
        if (db === null) {
            db = pgpromise(connection_params);
            connection = connection_params;
        }
        else {
            if (connection_params.hasOwnProperty("host") && connection_params.hasOwnProperty("database")) {
                if (connection.host !== connection_params.host || connection.database !== connection_params.database) {
                    db = pgpromise(connection_params);
                    connection = connection_params;
                }
            }
        }
    },

    resetConnection: function (host) {
        if (db !== null && connection !== null) {
            if (connection.host !== host) {
                db.$pool.end();
                db = null;
                connection = null;
                return true;
            }
        }
        else if (db === null && connection !== null) {
            connection = null;
            return true;
        }
        return false;
    },

    query: function (query, params) {
        var result = db.any(query, params).then(data => {
            return data;
        }).catch(error => {
            debug(error, query);
        });

        return result;
    },

    searchService: function (params) {
        var query = "";

        this.dbConnect(params.connection);

        if (!isNaN(params.searchStr) && Number.isInteger(parseFloat(params.searchStr))) {
            var searchServiceID = parseFloat(params.searchStr);

            query = "SELECT * FROM service_list WHERE service_id=$1";
            return this.query(query, [searchServiceID]);
        }
        else {
            query = "SELECT * FROM service_list WHERE title ILIKE '%$1:value%'";
            return this.query(query, [params.searchStr]);
        }
    },

    getServiceData: function (service_id) {
        var result = db.task("get-service-data", async t => {
            var service = await t.one("SELECT * FROM services WHERE service_id=$1", [service_id]),
                md_ids = [];

            service.allowed_groups = JSON.stringify(service.allowed_groups);

            // Get all service data
            service.comments = await t.any("SELECT * FROM comments WHERE service_id = $1", [service_id]);
            service.changelog = await t.any("SELECT * FROM changelog WHERE service_id = $1", [service_id]);
            service.contact_links = await t.any("SELECT * FROM contact_links WHERE service_id = $1", [service_id]);
            service.keywords = await t.any("SELECT * FROM keywords WHERE service_id = $1", [service_id]);
            // Get all service layers
            service.layers = await t.any("SELECT * FROM service_layers WHERE service_id = $1", [service_id]);
            var layer_ids = [];

            service.layers.forEach(layer => {
                layer_ids.push(layer.layer_id);
            });

            if (service.layers.length > 0) {
                // Get realting layer data
                service.layer_links = await t.any("SELECT * FROM layer_links WHERE layer_id = ANY (ARRAY[$1:csv])", [layer_ids]);
                service.layer_links.forEach(layer_links => {
                    md_ids.push(layer_links.md_id);
                });
            }

            if (md_ids.length > 0) {
                service.datasets = await t.any("SELECT * FROM datasets WHERE md_id = ANY (ARRAY[$1:csv])", [md_ids]);
            }
            else {
                service.datasets = [];
            }

            return service;
        }).then(data => {
            return data;
        }).catch(error => {
            debug("getServiceData", error);
            return {error: true};
        });

        return result;
    }
};

module.exports = service_importer;

var config = require("./../config.js"),
    xmldom = require("xmldom"),
    DOMParser = xmldom.DOMParser,
    xpath = require("xpath"),
    metadata_parser;

metadata_parser = {
    _getNamespaces: function () {
        return {"csw": "http://www.opengis.net/cat/csw/2.0.2", "gmd": "http://www.isotc211.org/2005/gmd", "gco": "http://www.isotc211.org/2005/gco", "srv": "http://www.isotc211.org/2005/srv", "gml": "http://www.opengis.net/gml", "gmx": "http://www.isotc211.org/2005/gmx", "gts": "http://www.isotc211.org/2005/gts", "xsi": "http://www.w3.org/2001/XMLSchema-instance"};
    },

    _parseXMLdoc: function (md) {
        var metadata_response_doc = new DOMParser().parseFromString(md, "text/xml"),
            select = xpath.useNamespaces(this._getNamespaces());

        return select("//gmd:MD_Metadata", metadata_response_doc)[0];
    },

    _getFees: function (md_doc) {
        var select = xpath.useNamespaces(this._getNamespaces()),
            MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc),
            fees = "";

        for (var i = 0; i < MD_LegalConstraints.length; i++) {
            var codeListValue_fees = select("gmd:useConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

            for (var j = 0; j < codeListValue_fees.length; j++) {
                if (codeListValue_fees[j].nodeValue.indexOf("otherRestrictions") > -1) {
                    var other_constraints = select("gmd:otherConstraints", MD_LegalConstraints[i]);

                    for (var k = 0; k < other_constraints.length; k++) {
                        var fees_str = select("string(gmx:Anchor)", other_constraints[k]);

                        if (fees_str === "") {
                            fees_str = select("string(gco:CharacterString)", other_constraints[k]);
                        }
                        if (fees_str !== "" && fees_str.indexOf("{") === -1) {
                            fees += fees_str + ", ";
                        }
                    }
                }
            }
        }

        return fees.substring(0, fees.length - 2);
    },

    _getFeesJSON: function (md_doc) {
        var select = xpath.useNamespaces(this._getNamespaces()),
            MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc),
            fees = "";

        for (var i = 0; i < MD_LegalConstraints.length; i++) {
            var codeListValue_fees = select("gmd:useConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

            for (var j = 0; j < codeListValue_fees.length; j++) {
                if (codeListValue_fees[j].nodeValue.indexOf("otherRestrictions") > -1) {
                    var other_constraints = select("gmd:otherConstraints", MD_LegalConstraints[i]);

                    for (var k = 0; k < other_constraints.length; k++) {
                        var fees_str = select("string(gmx:Anchor)", other_constraints[k]);

                        if (fees_str === "") {
                            fees_str = select("string(gco:CharacterString)", other_constraints[k]);
                        }
                        if (fees_str !== "" && fees_str.indexOf("{") > -1) {
                            fees = fees_str;
                        }
                    }
                }
            }
        }

        return fees;
    },

    _getAccessConstraints: function (md_doc) {
        var select = xpath.useNamespaces(this._getNamespaces()),
            MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc),
            access_constraints = "";

        for (var i = 0; i < MD_LegalConstraints.length; i++) {
            var codeListValue_ac = select("gmd:accessConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

            for (var j = 0; j < codeListValue_ac.length; j++) {
                if (codeListValue_ac[j].nodeValue.indexOf("otherRestrictions") > -1) {
                    var acc_str = select("string(gmd:otherConstraints/gmx:Anchor)", MD_LegalConstraints[i]);

                    if (acc_str === "") {
                        acc_str = select("string(gmd:otherConstraints/gco:CharacterString)", MD_LegalConstraints[i]);
                    }
                    if (acc_str !== "") {
                        access_constraints += acc_str + ", ";
                    }
                }
            }
        }

        return access_constraints.substring(0, access_constraints.length - 2);
    },

    _getKeywords: function (md_doc) {
        var select = xpath.useNamespaces(this._getNamespaces()),
            keyword_elements = select("//gmd:keyword", md_doc),
            keywords = "";

        for (var i = 0; i < keyword_elements.length; i++) {
            var keyword = select("string(gco:CharacterString)", keyword_elements[i]);

            keywords += keyword + "|";
        }

        return keywords.substring(0, keywords.length - 1);
    },

    parseServiceMD: function (servicemd) {
        var select = xpath.useNamespaces(this._getNamespaces()),
            md_metadata = this._parseXMLdoc(servicemd.data),
            inspire = false,
            md_uuid = select("string(//gmd:fileIdentifier/gco:CharacterString)", md_metadata),
            service_url = select("string(//srv:SV_OperationMetadata//srv:connectPoint//gmd:CI_OnlineResource//gmd:linkage//gmd:URL)", md_metadata),
            title = select("string(//gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString)", md_metadata);

        if (servicemd.data.indexOf("inspireidentifiziert") > 0) {
            inspire = true;
        }

        var service_metainfo = {
            title: title.replace("-in Arbeit", "").trim(),
            description: select("string(//gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString)", md_metadata).replace(/\r\n\r\n/g, "\r\n").replace(/\r\n\r\n/g, "\r\n"),
            service_url: service_url.replace("?", ""),
            md_uuid: md_uuid,
            inspire: inspire,
            fees: this._getFees(md_metadata),
            access_constraints: this._getAccessConstraints(md_metadata),
            keywords: []
        };

        return service_metainfo;
    },

    getCoupledRessources: function (servicemd) {
        var md_metadata = this._parseXMLdoc(servicemd.data),
            select = xpath.useNamespaces(this._getNamespaces()),
            md_uuids = select("//srv:operatesOn/@uuidref", md_metadata),
            uuid_links = select("//gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:description", md_metadata),
            dataset_uuids = [];

        for (let i = 0; i < md_uuids.length; i++) {
            dataset_uuids.push({
                md_uuid: md_uuids[i].nodeValue
            });
        }

        for (let i = 0; i < uuid_links.length; i++) {
            var uuid_link = select("string(gco:CharacterString)", uuid_links[i]);

            if (uuid_link.indexOf("#**#") > 0) {
                var md_uuid = uuid_link.split("#**#")[1];

                dataset_uuids.push({
                    md_uuid: md_uuid
                });
            }
        }

        return dataset_uuids;
    },

    getServiceMDID: function (servicemd) {
        var md_metadata = this._parseXMLdoc(servicemd.data),
            uuids = [];

        if (md_metadata) {
            var select = xpath.useNamespaces(this._getNamespaces()),
                uuids_doc = select("//gmd:fileIdentifier/gco:CharacterString", md_metadata);

            for (var i = 0; i < uuids_doc.length; i++) {
                uuids.push(uuids_doc[i].firstChild.data);
            }
        }
        return uuids;
    },

    parseDatasetMD: function (datasetmd) {
        var md_metadata = this._parseXMLdoc(datasetmd.data),
            select = xpath.useNamespaces(this._getNamespaces()),
            descriptiveKeywords = select("//gmd:descriptiveKeywords", md_metadata),
            CI_ResponsibleParty = select("//gmd:CI_ResponsibleParty", md_metadata),
            cat_opendata = "",
            cat_inspire = "",
            cat_hmbtg = "",
            cat_org = "",
            bbox_min_x = select("string(//gmd:EX_GeographicBoundingBox/gmd:westBoundLongitude/gco:Decimal)", md_metadata),
            bbox_min_y = select("string(//gmd:EX_GeographicBoundingBox/gmd:southBoundLatitude/gco:Decimal)", md_metadata),
            bbox_max_x = select("string(//gmd:EX_GeographicBoundingBox/gmd:eastBoundLongitude/gco:Decimal)", md_metadata),
            bbox_max_y = select("string(//gmd:EX_GeographicBoundingBox/gmd:northBoundLatitude/gco:Decimal)", md_metadata),
            hierarchyLevel = select("//gmd:hierarchyLevel", md_metadata),
            csw = select("string(gmd:MD_ScopeCode)", hierarchyLevel[0]);

        if (csw === "") {
            csw = select("string(gmd:MD_ScopeCode/@codeListValue)", hierarchyLevel[0]);
        }

        for (let i = 0; i < CI_ResponsibleParty.length; i++) {
            var role = select("string(gmd:role/gmd:CI_RoleCode/@codeListValue)", CI_ResponsibleParty[i]),
                organisationName = select("string(gmd:organisationName/gco:CharacterString)", CI_ResponsibleParty[i]);

            if (role === "publisher") {
                cat_org = organisationName;
            }

            if (i === CI_ResponsibleParty.length - 1 && cat_org === "") {
                cat_org = organisationName;
            }
        }

        for (let i = 0; i < config.cat_org.length; i++) {
            if (cat_org.indexOf(config.cat_org[i]) > -1) {
                cat_org = config.cat_org[i];
                break;
            }
        }

        for (let i = 0; i < descriptiveKeywords.length; i++) {
            var thesaurus = select("string(gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString)", descriptiveKeywords[i]),
                keywords = select("gmd:MD_Keywords/gmd:keyword", descriptiveKeywords[i]);

            for (let j = 0; j < keywords.length; j++) {
                var keyword = select("string(gco:CharacterString)", keywords[j]);

                if (config.openDataCat[keyword]) {
                    cat_opendata += config.openDataCat[keyword] + "|";
                }
                else if (thesaurus === "GEMET - INSPIRE themes, version 1.0") {
                    cat_inspire += keyword + "|";
                }
                else if (thesaurus === "HmbTG-Informationsgegenstand") {
                    cat_hmbtg += keyword + "|";
                }
            }
        }

        if (cat_inspire.substring(0, cat_inspire.length - 1) === "Kein INSPIRE-Thema") {
            cat_inspire = "nicht INSPIRE-identifiziert";
        }
        else {
            cat_inspire = cat_inspire.substring(0, cat_inspire.length - 1);
        }

        if (cat_opendata.substring(0, cat_opendata.length - 1) === "") {
            cat_opendata = "Sonstiges";
        }
        else {
            cat_opendata = cat_opendata.substring(0, cat_opendata.length - 1);
        }

        var dataset_params = {
            dataset_name: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString)", md_metadata),
            descr: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString)", md_metadata),
            keywords: this._getKeywords(md_metadata),
            rs_id: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString)", md_metadata),
            cat_opendata: cat_opendata,
            cat_inspire: cat_inspire,
            cat_hmbtg: cat_hmbtg.substring(0, cat_hmbtg.length - 1),
            cat_org: cat_org,
            bbox: bbox_min_x + " " + bbox_min_y + "," + bbox_max_x + " " + bbox_max_y,
            fees: this._getFees(md_metadata),
            access_constraints: this._getAccessConstraints(md_metadata),
            csw: csw
        };

        return dataset_params;
    }
};

module.exports = metadata_parser;

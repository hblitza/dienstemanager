var layer_json_gen = require("../layer_json_gen"),
    generate_layer_json;

generate_layer_json = {
    execute: function () {
        // alle layer JSON Objekte werden gelöscht und neu genriert
        layer_json_gen.prepare(undefined, undefined, undefined, undefined);
    }
};

module.exports = generate_layer_json;

var database = require("../database"),
    service_checker = require("../service_checker"),
    check_all_services;

check_all_services = {
    execute: function () {
        database.getServiceList().then(
            function (serivces) {
                var services_to_check = [];

                serivces.forEach(function (service) {
                    if (service.status === "Produktiv" && service.security_type === "keine" && service.software !== "Proxy" && service.software !== "ReverseProxy" && service.software !== "extern" && service.software !== "GWC" && (service.service_type === "WMS" || service.service_type === "WFS")) {
                        services_to_check.push(service);
                    }
                });
                // Dem service_checker wird der Array mit allen zu testenden Diensten übergeben, die dann in einem rekursiven Aufruf nacheinander getestet werden
                service_checker.start_rec(services_to_check, 0, {num_check: 0, num_warn: 0, num_error: 0, num_cancelled: 0});
            }
        );
    }
};

module.exports = check_all_services;

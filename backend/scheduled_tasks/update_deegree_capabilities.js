var inspire_cap_gen = require("../inspire_cap_gen"),
    database = require("../database"),
    debug = require("debug")("DM:update_deegree_capabilities"),
    moment = require("moment"),
    config = require("./../../config.js"),
    update_deegree_capabilities;

update_deegree_capabilities = {
    execute: function () {
        database.getServices().then(
            function (services) {
                var i = 0;

                // Bei allen deegree-Diensten mit gekoppelten Metadaten werden die Capabilities aktualisiert.
                // Zwischen jedem Aufruf wird 0,5 Sekunde pausiert, damit nicht zu viele parallele Anfragen geöffnet werden.
                function updateLoop () {
                    var valid_md_id = false;

                    if (services[i].service_md_id !== undefined) {
                        if (services[i].service_md_id.length > 0) {
                            valid_md_id = true;
                        }
                    }
                    if (valid_md_id && services[i].software === "deegree") {
                        setTimeout(function () {
                            if (config.log_level === "debug") {
                                debug("update deegree capabilities " + services[i].service_id);
                            }

                            inspire_cap_gen.updateDeegreeCapabilities(services[i]);
                            i++;
                            if (i < services.length) {
                                updateLoop();
                            }
                            else {
                                console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der deegree Capabilities abgeschlossen.");
                                database.updateScheduledTaskStatus(["update_deegree_capabilities", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle deegree Capabilities aktualisiert", "Erfolgreich"]);
                            }
                        }, 500);
                    }
                    else {
                        i++;
                        if (i < services.length) {
                            updateLoop();
                        }
                        else {
                            console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der deegree Capabilities abgeschlossen.");
                            database.updateScheduledTaskStatus(["update_deegree_capabilities", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle deegree Capabilities aktualisiert", "Erfolgreich"]);
                        }
                    }
                }
                if (services.length > 0) {
                    updateLoop();
                }
                else {
                    console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der deegree Capabilities abgeschlossen.");
                    database.updateScheduledTaskStatus(["update_deegree_capabilities", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle deegree Capabilities aktualisiert", "Erfolgreich"]);
                }
            }
        );
    }
};

module.exports = update_deegree_capabilities;

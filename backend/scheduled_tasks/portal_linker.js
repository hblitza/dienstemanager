const debug = require("debug")("DM:portal_linker"),
    config = require("./../../config.js"),
    database = require("./../database"),
    https_agent = require("./../https_agent"),
    moment = require("moment"),
    notifier = require("../notifier"),
    {createClient} = require("webdav");

var ids_array = [];

const portal_linker = {

    execute: async function () {

        if (config.hasOwnProperty("webdav")) {
            var apps = [];
            var qualified_domains = [];

            for (const webdav_conf of config.webdav) {
                var portals = await this.processPortals(webdav_conf).then(data => {
                    return data;
                }).catch(error => {
                    debug(error);
                });

                if (portals) {
                    qualified_domains.push(webdav_conf.full_qualified_domain);
                    apps = apps.concat(portals);
                }
                else {
                    debug(webdav_conf.base_url, "Konnte Portale nicht über WebDAV auswerten.");
                }
            }

            if (apps.length > 0) {

                for (const domain of qualified_domains) {
                    await database.deletePortalLinks(domain);
                }

                for (const app of apps) {
                    var portal_links = [];

                    for (const id of app.ids) {
                        portal_links.push({layer_id: id, title: app.title, url: app.url});
                    }
                    await database.addPortalLinks(portal_links);
                }

                var deleteLayerRequests = await database.getDeleteLayerRequests();

                for (const deleteLayerRequest of deleteLayerRequests) {
                    debug("delete layer Request for ID: " + deleteLayerRequest.layer_id);
                    var linkedPortals = await database.getPortalLinks(deleteLayerRequest.layer_id);

                    if (linkedPortals.length === 0) {
                        if (config.mail) {
                            await notifier.onNoMoreLinkedPortals(deleteLayerRequest.layer_id);
                        }
                    }
                }
            }

            console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Liste der in Portalen verlinkten LayerIDs abgeschlossen. " + apps.length + " Portale analysiert.");
            database.updateScheduledTaskStatus(["portal_linker", moment().format("YYYY-MM-DD HH:mm:ss"), "Aktualisierung der Liste der in Portalen verlinkten LayerIDs abgeschlossen. " + apps.length + " Portale analysiert.", "Erfolgreich"]);
        }
        else {
            debug("No webdav property found in config for portal_linker!");
            database.updateScheduledTaskStatus(["portal_linker", moment().format("YYYY-MM-DD HH:mm:ss"), "Portalliste konnte nicht aktualisiert werden, WebDAV nicht konfiguriert.", "Hinweis"]);
        }
    },

    processPortals: async function (webdav_conf) {
        var proxy_settings = webdav_conf.proxy ? config.proxy : false;
        const client = createClient(
            webdav_conf.base_url + "/" + webdav_conf.webdav_directory,
            {
                username: webdav_conf.username,
                password: webdav_conf.password,
                digest: webdav_conf.digest,
                httpsAgent: https_agent.getHttpsAgent(proxy_settings)
            }
        );

        var portals = await client.getDirectoryContents("/").then(data => {
            return data;
        }).catch(error => {
            debug("Error connecting to WebDAV:", webdav_conf.base_url);
            debug(error);
        });

        var apps = [];

        // Im webdav Verzeichnis liegen Ordner die den einzelnen Portalen entsprechen
        for (const portal of portals) {
            const basename = portal.basename.toLowerCase();

            if (portal.type === "directory" && !basename.includes("_abnahme") && !basename.includes("_test") && !basename.includes("_alt")) {
                const contents = await client.getDirectoryContents(portal.filename).then(data => {
                    return data;
                }).catch(error => {
                    debug("WebDAV Error (getDirectoryContents):", webdav_conf.base_url);
                    debug(error);
                });

                for (const file of contents) {
                    // enthält ein Ordner eine config.json
                    if (file.type === "file" && file.basename === "config.json") {
                        const portal_config = await client.getFileContents(portal.filename + "/config.json", {format: "text"}).then(data => {
                            return data;
                        }).catch(error => {
                            debug("WebDAV Error (getFileContents):", webdav_conf.base_url);
                            debug(error);
                        });
                        var ids = [];

                        if (portal_config.hasOwnProperty("Themenconfig")) {
                            // dann hole alle Layer IDs daraus
                            ids_array = [];
                            for (const key in portal_config.Themenconfig) {
                                ids = ids.concat(this.getIds(portal_config.Themenconfig[key]));
                            }
                            // IDs können doppelt vorkommen, daher ein eindeutiges Set erzeugen
                            ids = [...new Set(ids)];

                            if (ids.length > 0) {
                                var title = "";

                                if (portal_config.Portalconfig.hasOwnProperty("portalTitle")) {
                                    title = portal_config.Portalconfig.portalTitle.title;
                                }
                                if (title.trim() === "") {
                                    title = portal.basename;
                                }
                                apps.push({title: title, url: webdav_conf.full_qualified_domain + "/" + portal.basename, ids: ids});
                            }
                            else {
                                debug("No layer ids found: ", portal.basename, webdav_conf.base_url + "/" + portal.basename);
                            }
                        }
                        else {
                            debug("No Themenconfig found: ", portal.basename, webdav_conf.base_url + "/" + portal.basename);
                        }
                    }
                }
            }
        }

        return apps;
    },

    getIds: function (themeconfig) {
        var ids = [];

        if (themeconfig.hasOwnProperty("Layer")) {
            for (const layer of themeconfig.Layer) {
                ids = ids.concat(this.getIdsHelper(layer));
            }
        }
        if (themeconfig.hasOwnProperty("Ordner")) {
            this.getIdArrayHelper(themeconfig.Ordner, 0);

            ids = ids.concat(ids_array);
        }
        return ids;
    },

    getIdArrayHelper: function (folder_array, index) {
        if (index < folder_array.length) {
            if (folder_array[index].hasOwnProperty("Ordner")) {
                this.getIdArrayHelper(folder_array[index].Ordner, 0);
            }
            if (folder_array[index].hasOwnProperty("Layer")) {
                ids_array = ids_array.concat(this.getIdsHelper(folder_array[index].Layer));
            }
            this.getIdArrayHelper(folder_array, index + 1);
        }
    },

    getIdsHelper: function (id_obj) {
        var ids = [];

        if (Array.isArray(id_obj)) {
            for (const id of id_obj) {
                if (id.hasOwnProperty("id")) {
                    this.addId(ids, id.id);
                }
                else {
                    this.addId(ids, id);
                }

                if (id.hasOwnProperty("children")) {
                    ids = ids.concat(this.getIdsHelper(id.children));
                }
            }
        }
        else {
            if (id_obj.hasOwnProperty("id")) {
                if (id_obj.id.hasOwnProperty("id")) {
                    this.addId(ids, id_obj.id.id);
                    if (id_obj.id.id.hasOwnProperty("children")) {
                        ids = ids.concat(this.getIdsHelper(id_obj.id));
                    }
                }
                else {
                    if (Array.isArray(id_obj.id)) {
                        ids = ids.concat(this.getIdsHelper(id_obj.id));
                    }
                    else {
                        this.addId(ids, id_obj.id);
                    }
                    if (id_obj.hasOwnProperty("children")) {
                        ids = ids.concat(this.getIdsHelper(id_obj.children));
                    }
                }
            }
        }
        return ids;
    },

    isInt: function (str) {
        if (typeof str !== "string") {
            return false;
        }
        return !isNaN(str) && Number.isInteger(parseFloat(str));
    },

    addId: function (ids, id) {
        if (this.isInt(id)) {
            ids.push(parseFloat(id));
        }
        else if (Array.isArray(id)) {
            for (let i = 0; i < id.length; i++) {
                if (Number.isInteger(parseFloat(id[i]))) {
                    ids.push(parseFloat(id[i]));
                }
                else if (id[i].hasOwnProperty("id")) {
                    ids.push(parseFloat(id[i].id));
                }
            }
        }
    }
};

module.exports = portal_linker;

var axios_instances = require("./axios_instances"),
    _ = require("underscore"),
    config = require("./../config.js"),
    debug = require("debug")("DM:elasticsearch_connector"),
    elasticsearch_connector;

elasticsearch_connector = {
    updateIndex: function (service_data, layer_data, params) {
        var keywords = layer_data.keywords,
            status = service_data.status,
            only_internet = service_data.only_internet,
            only_intranet = service_data.only_intranet,
            es_server_names = config.es_params.es_server_name,
            url_obj = [];

        config.es_params.es_index.forEach(function (es_index_params) {
            var cross_incl = _.intersection(es_index_params.keyword_filter_include, keywords),
                cross_excl = _.intersection(es_index_params.keyword_filter_exclude, keywords),
                insert_intranet_internet = true,
                insert_prod = true;

            if ((!only_intranet && only_internet) && es_index_params.internal) {
                insert_intranet_internet = false;
            }
            else if ((only_intranet && !only_internet) && !es_index_params.internal) {
                insert_intranet_internet = false;
            }

            if (es_index_params.only_prod && status !== "Produktiv") {
                insert_prod = false;
            }

            if ((es_index_params.keyword_filter_include.length === 0 || cross_incl.length > 0) && cross_excl.length === 0 && insert_intranet_internet && insert_prod) {
                es_server_names.forEach(function (val_es_server_name) {
                    var url = "http://" + val_es_server_name + "/" + es_index_params.name + "/_doc/" + layer_data.layer_id;

                    if (es_index_params.internal) {
                        url_obj.push({domain: "http://" + val_es_server_name, url: url, params: params.json_intranet_es});
                    }
                    else {
                        url_obj.push({domain: "http://" + val_es_server_name, url: url, params: params.json_internet_es});
                    }
                });
            }
        });

        url_obj.forEach(function (obj) {
            var instance = axios_instances.getInstance(obj.domain);

            instance.post(obj.url, obj.params).catch((error) => {
                if (error.response) {
                    debug(error.response.status + " err = " + error.response.config.url);
                }
                else {
                    debug("Error", error.message);
                }
            });
        });
    },

    deleteFromIndex: function (layer_id) {
        var es_server_names = config.es_params.es_server_name,
            url_obj = [];

        config.es_params.es_index.forEach(function (es_index_params) {
            es_server_names.forEach(function (val_es_server_name) {
                var url = "http://" + val_es_server_name + "/" + es_index_params.name + "/_doc/" + layer_id;

                if (es_index_params.internal) {
                    url_obj.push({domain: "http://" + val_es_server_name, url: url});
                }
                else {
                    url_obj.push({domain: "http://" + val_es_server_name, url: url});
                }
            });
        });

        url_obj.forEach(function (obj) {
            var instance = axios_instances.getInstance(obj.domain);

            instance.delete(obj.url).catch((error) => {
                if (error.response) {
                    if (error.response.status !== 404) {
                        debug(error.response.status + " err = " + error.response.config.url);
                    }
                }
                else {
                    debug("Error", error.message);
                }
            });
        });
    }
};

module.exports = elasticsearch_connector;

module.exports = function (io) {
    io.on("connection", function (socket) {
        socket.on("service_details_saved", function () {
            socket.broadcast.emit("update_service_list");
        });
    });
};

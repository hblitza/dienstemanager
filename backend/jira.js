const config = require("./../config.js"),
    axios = require("axios"),
    https_agent = require("./https_agent"),
    debug = require("debug")("DM:jira"),
    database = require("./database"),
    _ = require("underscore"),
    fs = require("fs"),
    path = require("path");

const jira = {
    searchJiraIssues: async function (res) {
        const projects_to_search = _.where(config.jira.projects, {use_in_ticketsearch: true});
        var all_issues = [];

        try {
            for (let j = 0; j < projects_to_search.length; j++) {

                const url_issue_count = config.jira.url + "/search?jql=project=" + projects_to_search[j].name + "&maxResults=0",
                    url = config.jira.url + "/search?jql=project=" + projects_to_search[j].name + "&fields=id,key,summary,creator,assignee,project&maxResults=500&startAt=",
                    proxy_settings = config.jira.proxy ? config.proxy : false,
                    https_url = config.jira.url.indexOf("https:") > -1,
                    httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;


                var issue_count_response = await axios.get(url_issue_count, {
                    proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent,
                    auth: config.jira.auth
                });

                var iteratee = Math.ceil(issue_count_response.data.total / 500);

                for (let i = 0; i < iteratee; i++) {
                    var issues_resp = await axios.get(url + i * 500, {
                        proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                        httpsAgent: httpsAgent,
                        auth: config.jira.auth
                    });

                    if (issues_resp.data.hasOwnProperty("errorMessages")) {
                        debug("searchJiraIssues", issues_resp.data.errorMessages);
                    }
                    else {
                        all_issues = all_issues.concat(issues_resp.data.issues);
                    }
                }
            }
            res.json(all_issues);
        }
        catch (err) {
            debug("JIRA API Request", err);
            res.json({error: true, errorMessages: err.response.data.errorMessages, errors: err.response.data.errors});
        }
    },

    createJiraTicket: async function (data, res) {
        const url = config.jira.url + "/issue",
            proxy_settings = config.jira.proxy ? config.proxy : false,
            https_url = config.jira.url.indexOf("https:") > -1,
            httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null,
            me = this;

        var update_components = data.update_components,
            update_components_text = "*Komponenten, die aktualisiert werden müssen:* \r\n",
            fme_text = {schedule: "FME Scheduler: *", automation: "FME Automation: *"},
            fme_component_activate = data.fme_component_activate,
            servicetype = data.servicetype,
            servicetype_checkbox = data.servicetype_checkbox,
            servicetype_checkbox_values = Object.values(servicetype_checkbox),
            servicetype_text = "",
            chars_to_replace = {"ä": "ae", "ö": "oe", "ü": "ue", " ": "_"},
            description_template_type = data.tickettype.replace(/[ä-ü\s]/g, m => chars_to_replace[m]) + "_template",
            responsible_party = data.responsible_party,
            security_setting = data.security_setting,
            security_setting_text = "";


        if (Object.keys(update_components).length === 0 && update_components.constructor === Object) {
            update_components_text = "*Komponenten, die aktualisiert werden müssen:* siehe oben unter manuelle Angaben!";
        }
        else {
            if (update_components.serviceconfig) {
                update_components_text += "* Dienstkonfiguration: \r\n";
                if (typeof update_components.serviceconfig === "object") {
                    for (let i = 0; i < update_components.serviceconfig.length; i++) {
                        update_components_text += "** " + update_components.serviceconfig[i] + "\r\n";
                    }
                }
                else {
                    update_components_text += "** " + update_components.serviceconfig + "\r\n";
                }
            }
            if (update_components.data) {
                update_components_text += "* Daten: \r\n";
                if (typeof update_components.data === "object") {
                    for (let i = 0; i < update_components.data.length; i++) {
                        update_components_text += "** " + update_components.data[i] + "\r\n";
                    }
                }
                else {
                    update_components_text += "** " + update_components.data + "\r\n";
                }
            }
            if (update_components.fme) {
                update_components_text += "* FME: \r\n";
                if (typeof update_components.fme === "object") {
                    for (let i = 0; i < update_components.fme.length; i++) {
                        update_components_text += "** " + update_components.fme[i] + "\r\n";
                    }
                }
                else {
                    update_components_text += "** " + update_components.fme + "\r\n";
                }
            }
        }

        if (fme_component_activate === "FME Scheduler" && data.tickettype === "Dienst erstellen") {
            fme_text = fme_text.schedule += "Bitte aktivieren.* Prozessname: *" + data.fme_processname + ".* Hier zu finden: *" + data.fme_processlocation + "*";
        }
        else if (fme_component_activate === "FME Automation" && data.tickettype === "Dienst erstellen") {
            fme_text = fme_text.automation += "Bitte starten.* Prozessname: *" + data.fme_processname + ".* Hier zu finden: *" + data.fme_processlocation + "*";
        }
        else if (fme_component_activate === "Keine" && data.tickettype === "Dienst erstellen") {
            fme_text = "*Es muss kein FME Scheduler oder FME Automation aktiviert/gestartet werden!*";
        }

        if (update_components.fme && data.tickettype === "Dienst aktualisieren") {
            if (update_components.fme.includes("FME Schedule")) {
                fme_text = fme_text.schedule += "Bitte anpassen.* Prozessname: *" + data.fme_processname + ".* Hier zu finden: *" + data.fme_processlocation + "*";
            }
            else if (update_components.fme.includes("FME Automation")) {
                fme_text = fme_text.automation += "Bitte starten.* Name: *" + data.fme_processname + ".* Link zur Automation: *" + data.fme_processlocation + "*";
            }
            else {
                fme_text = "*Es muss kein FME Scheduler oder FME Automation angepasst/gestartet werden!*";
            }
        }
        else if (!update_components.fme && data.tickettype === "Dienst aktualisieren") {
            fme_text = "*Es muss kein FME Scheduler oder FME Automation angepasst/gestartet werden!*";
        }

        if (servicetype_checkbox_values.length > 1 && servicetype === "WMS" || servicetype === "WFS") {
            if (data.tickettype === "Dienst erstellen") {
                servicetype_text += "Bitte *" + servicetype_checkbox_values.join("* und *") + "* aus der angegebenen Datenquelle erzeugen.";
            }
            else if (data.tickettype === "Dienst aktualisieren") {
                servicetype_text += "Bitte *" + servicetype_checkbox_values.join("* und *") + "* anpassen.";
            }
            else if (data.tickettype === "Dienst löschen") {
                servicetype_text += "Bitte *" + servicetype_checkbox_values.join("* und *") + "* löschen.";
            }
        }
        else if (servicetype_checkbox_values.length === 1 && servicetype === "WMS" || servicetype === "WFS") {
            if (data.tickettype === "Dienst erstellen") {
                servicetype_text += "Bitte nur *" + servicetype_checkbox_values[0] + "* aus der angegebenen Datenquelle erzeugen.";
            }
            else if (data.tickettype === "Dienst aktualisieren") {
                servicetype_text += "Bitte nur *" + servicetype_checkbox_values[0] + "* anpassen.";
            }
            else if (data.tickettype === "Dienst löschen") {
                servicetype_text += "Bitte nur *" + servicetype_checkbox_values[0] + "* löschen.";
            }
        }

        if (!responsible_party) {
            responsible_party = "keine Verantwortliche Stelle eingetragen.";
        }

        if (security_setting && security_setting !== "Nein") {
            if (data.tickettype === "Dienst erstellen") {
                security_setting_text += "Bitte Absicherung einrichten - Art der Absicherung: *" + security_setting + "*";
            }
            else if (data.tickettype === "Dienst aktualisieren") {
                security_setting_text += "Bitte Absicherung anpassen - Art der Absicherung: *" + security_setting + "*";
            }
        }
        else {
            security_setting_text;
        }

        try {
            var read_template_file = fs.readFileSync(path.join(__dirname, "templates/" + description_template_type + ".txt"), "utf-8"),
                template_data = {
                    update_components_text: update_components_text,
                    fme_text: fme_text,
                    servicetype_text: servicetype_text,
                    datasource: data.datasource,
                    workspace: data.workspace,
                    servicename: data.servicename,
                    description: data.description,
                    responsible_party: responsible_party,
                    contact_person: data.user,
                    security_setting_text: security_setting_text
                },
                template = _.template(read_template_file),
                template_service_type = template(template_data),
                dataObjectForRequest = await me._getJiraApiRequestDataObject(data, template_service_type);

            var postTicketResponse = await axios({
                method: "post",
                url: url,
                httpsAgent: httpsAgent,
                proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                auth: config.jira.auth,
                headers: {
                    "Content-Type": "application/json"
                },
                data: dataObjectForRequest
            });

            const project_index = config.jira.projects.findIndex(project => project.name === data.jiraproject_name_to_write);

            if (config.jira.projects[project_index].transition_execution === true) {
                for (let i = 0; i < config.jira.projects[project_index].transition_id.length; i++) {
                    await axios({
                        method: "post",
                        url: "https://www.jira.geoportal-hamburg.de/rest/api/2/issue/" + postTicketResponse.data.key + "/transitions",
                        httpsAgent: httpsAgent,
                        proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                        auth: config.jira.auth,
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: {
                            "transition": {
                                "id": config.jira.projects[project_index].transition_id[i]
                            }
                        }
                    });
                }
            }

            await database.addServiceIssue({id: postTicketResponse.data.id, service_id: data.service_id, key: postTicketResponse.data.key, creator: data.user, assignee: data.user, project: data.jiraproject_name_to_write, summary: data.tickettype + ": " + data.service_id});

            res.json({success: true});
        }
        catch (err) {
            debug("JIRA API Request", err);
            res.json({error: true, errorMessages: err.response.data.errorMessages, errors: err.response.data.errors});
        }
    },

    _getJiraApiRequestDataObject: function (data, template_service_type) {
        return new Promise((resolve, reject) => {
            var proxy_settings = config.jira.proxy ? config.proxy : false,
                https_url = config.jira.url.indexOf("https:") > -1,
                httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null,
                get_customfields_name_url = config.jira.url + "/field",
                check_project_fields_url = config.jira.url + "/issue/createmeta?projectKeys=" + data.jiraproject_name_to_write + "&expand=projects.issuetypes.fields",
                get_jira_ticket_info = config.jira.url + "/issue/" + data.linkedticket,
                dataObject = {
                    "fields": {
                        "project": {
                            "key": data.jiraproject_name_to_write
                        },
                        "summary": data.tickettype + ": " + data.service_id,
                        "description": template_service_type,
                        "issuetype": {
                            "name": data.issuetype
                        },
                        "assignee": {
                            "name": config.jira.default_assignee
                        }
                    }
                },
                EpicLinkId,
                StoryPointsId;

            (async () => {
                try {
                    // customfield ids
                    var customfieldResponse = await axios({
                        method: "get",
                        url: get_customfields_name_url,
                        httpsAgent: httpsAgent,
                        proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                        auth: config.jira.auth,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });

                    for (let i = 0; i < customfieldResponse.data.length; i++) {
                        if (Object.values(customfieldResponse.data[i]).includes("Epic Link")) {
                            EpicLinkId = customfieldResponse.data[i].id;
                        }
                        if (Object.values(customfieldResponse.data[i]).includes("Story Points")) {
                            StoryPointsId = customfieldResponse.data[i].id;
                        }
                    }

                    // check available project fields
                    var checkProjectfieldResponse = await axios({
                        method: "get",
                        url: check_project_fields_url,
                        httpsAgent: httpsAgent,
                        proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                        auth: config.jira.auth,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });

                    var projectfields_of_certain_issuetype = checkProjectfieldResponse.data.projects[0].issuetypes.filter(x => Object.values(x).includes(data.issuetype))[0].fields,
                        field_keys = Object.keys(projectfields_of_certain_issuetype);

                    if (data.issuetype === "Sub-task") {
                        dataObject.fields.parent = {"key": data.linkedticket};
                        if (field_keys.includes("duedate")) {
                            dataObject.fields.duedate = data.duedate;
                        }
                        if (field_keys.includes("labels")) {
                            dataObject.fields.labels = data.jiraproject_label;
                        }
                        if (field_keys.includes(StoryPointsId)) {
                            dataObject.fields[data.story_points_customfield_id] = data.story_points_value;
                        }
                        if (field_keys.includes("components")) {
                            dataObject.fields.components = data.project_components;
                        }
                    }
                    else if (data.issuetype === "Task") {
                        if (field_keys.includes("duedate")) {
                            dataObject.fields.duedate = data.duedate;
                        }
                        if (field_keys.includes("labels")) {
                            dataObject.fields.labels = data.jiraproject_label;
                        }
                        if (field_keys.includes(EpicLinkId) && data.activate_epic_link === true) {
                            dataObject.fields[data.epic_link_customfield_id] = data.epic_link_key;
                        }
                        if (field_keys.includes(StoryPointsId)) {
                            dataObject.fields[data.story_points_customfield_id] = data.story_points_value;
                        }
                        if (field_keys.includes("components")) {
                            dataObject.fields.components = data.project_components;
                        }
                    }

                    // get priority id of linkedticket and set for sub-ticket
                    if (data.issuetype === "Sub-task") {
                        var priorityResponse = await axios({
                            method: "get",
                            url: get_jira_ticket_info + "?fields=priority",
                            httpsAgent: httpsAgent,
                            proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                            auth: config.jira.auth,
                            headers: {
                                "Content-Type": "application/json"
                            }
                        });

                        var linkedticket_prio_id = priorityResponse.data.fields.priority.id;

                        dataObject.fields.priority = {"id": linkedticket_prio_id};
                        resolve(dataObject);
                    }
                    else if (data.issuetype === "Task") {
                        resolve(dataObject);
                    }
                }
                catch (err) {
                    debug("JIRA API Request", err);
                    reject(err);
                }
            })();
        });
    }
};

module.exports = jira;

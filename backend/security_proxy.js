var pgpromise = require("pg-promise")(/* options */),
    config = require("./../config.js"),
    debug = require("debug")("DM:security_proxy"),
    sql = require("./sql"),
    securityProxyDB,
    connection,
    securityProxyDatabase,
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

if (env === "prod") {
    connection = config.security_proxy_database.prod;
}
else {
    connection = config.security_proxy_database.dev;
}

securityProxyDB = pgpromise(connection);

securityProxyDatabase = {

    dbQuery: function (query, params) {
        var result = securityProxyDB.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug(error, query);
            });

        return result;
    },

    usernameExists: function (params) {
        // checks if a given list of usernames already exists, returns all given and existing usernames
        var usernames = [];

        params.users.forEach(function (user) {
            usernames.push("[UP-ext]" + user.username);
        });

        return this.dbQuery("SELECT username FROM deeuser.saii_user WHERE username = ANY (ARRAY[$1:csv])", [usernames]);
    },

    getEndpointId: function (params) {
        return this.dbQuery("SELECT * FROM deeuser.saii_endpoint WHERE endpoint_name = $1 AND proxy_url = $2", [params.endpoint_name, params.proxy_url]);
    },

    getObjects: function (params) {
        return this.dbQuery(sql.securityproxy.getObjects, params);
    },

    getUserGroupRole: function (params) {
        return this.dbQuery(sql.securityproxy.getUserGroupRole, params);
    },

    deleteUsers: function (params) {
        var user_ids = [],
            group_ids = [],
            role_ids = [];

        params.users.forEach(function (user) {
            user_ids.push(user.user_id);
            group_ids.push(user.group_id);
            role_ids.push(user.role_id);
        });

        const queries = pgpromise.helpers.concat([
            {query: "DELETE FROM deeuser.saii_user_group WHERE user_id IN ($1:csv)", values: [user_ids]},
            {query: "DELETE FROM deeuser.saii_user CASCADE WHERE user_id IN ($1:csv)", values: [user_ids]},
            {query: "DELETE FROM deeuser.saii_group_role WHERE group_id IN ($1:csv)", values: [group_ids]},
            {query: "DELETE FROM deeuser.saii_group CASCADE WHERE group_id IN ($1:csv)", values: [group_ids]},
            {query: "DELETE FROM deeuser.saii_role_object WHERE role_id IN ($1:csv)", values: [role_ids]},
            {query: "DELETE FROM deeuser.saii_role CASCADE WHERE role_id IN ($1:csv)", values: [role_ids]}
        ]);

        var results = securityProxyDB.multi(queries)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("deleteUsers", error);
            });

        return results;
    },

    deleteEndpointObjects: function (params) {
        var object_ids = [];

        if (typeof params.objects === "string") {
            params.objects = JSON.parse(params.objects);
        }
        if (Array.isArray(params.objects)) {
            params.objects.forEach(function (object) {
                object_ids.push(object.object_id);
            });
        }
        var result = securityProxyDB.result("DELETE FROM deeuser.saii_object WHERE object_id IN ($1:csv)", [object_ids], r => r.rowCount)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("deleteEndpointObjects", error);
            });

        return result;
    },

    deleteEndpoint: function (params) {
        return this.dbQuery(sql.securityproxy.deleteEndpoint, params);
    },

    addEndpoint: function (params) {
        return this.dbQuery(sql.securityproxy.addEndpoint, params);
    },

    addEndpointURLs: function (params) {
        return this.dbQuery(sql.securityproxy.addEndpointURLs, params);
    },

    addEndpointObjects: function (params) {

        const table = new pgpromise.helpers.TableName("saii_object", "deeuser");

        const object_id_column = {
            name: "object_id",
            init: () => "nextval('deeuser.saii_object_seq')",
            mod: ":raw"
        };

        const cs = new pgpromise.helpers.ColumnSet([object_id_column, "object_type", "object_name", "object_title", "endpoint_id"], {table});

        const query = pgpromise.helpers.insert(params.objects, cs) + "RETURNING object_id";

        var result = securityProxyDB.many(query)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("addEndpointObjects", error, query);
            });

        return result;
    },

    assignRolesToObjects: function (params) {
        var queries = [];

        params.forEach(function (object) {
            params.role_ids.forEach(function (roles) {
                object.role_id = roles.role_id;
                const role_object_id_column = {
                    name: "role_object_id",
                    init: () => "nextval('deeuser.saii_role_object_seq')",
                    mod: ":raw"
                };
                const cs = new pgpromise.helpers.ColumnSet([role_object_id_column, "role_id", "object_id", "grant_right"], {table: "saii_role_object"});
                const q = pgpromise.helpers.insert(object, cs) + "RETURNING role_object_id";

                queries.push(q);
            });
        });

        const query = pgpromise.helpers.concat(queries);

        var results = securityProxyDB.multi(query)
            .then(data => {
                return data;
            }).catch(error => {
                debug("assignRolesToObjects", error, query);
            });

        return results;
    },

    addSecurityProxyUsers: function (params) {
        params.users.forEach(function (user) {
            user.username = "[UP-ext]" + user.username;
        });
        const table = new pgpromise.helpers.TableName("saii_user", "deeuser");
        const user_id_column = {
            name: "user_id",
            init: () => "nextval('deeuser.saii_user_seq')",
            mod: ":raw"
        };
        const cs = new pgpromise.helpers.ColumnSet([user_id_column, "username", "password", "email", "password_type"], {table});
        const query = pgpromise.helpers.insert(params.users, cs) + "RETURNING user_id, username";
        var result = securityProxyDB.many(query)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("addSecurityProxyUsers", error, query);
            });

        return result;
    },

    addSecurityProxyGroups: function (params) {
        const table = new pgpromise.helpers.TableName("saii_group", "deeuser");
        const group_id_column = {
            name: "group_id",
            init: () => "nextval('deeuser.saii_group_seq')",
            mod: ":raw"
        };
        var group_names = [];

        params.users.forEach(function (user) {
            group_names.push({name: user.group_name});
        });
        const cs = new pgpromise.helpers.ColumnSet([group_id_column, "name"], {table});
        const query = pgpromise.helpers.insert(group_names, cs) + "RETURNING group_id, name";

        var result = securityProxyDB.many(query)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("addSecurityProxyGroups", error, query);
            });

        return result;
    },

    addSecurityProxyRoles: function (params) {
        const table = new pgpromise.helpers.TableName("saii_role", "deeuser");
        const role_id_column = {
            name: "role_id",
            init: () => "nextval('deeuser.saii_role_seq')",
            mod: ":raw"
        };
        var role_names = [];

        params.users.forEach(function (user) {
            role_names.push({name: user.role_name, sort_order: 0});
        });
        const cs = new pgpromise.helpers.ColumnSet([role_id_column, "name", "sort_order"], {table});
        const query = pgpromise.helpers.insert(role_names, cs) + "RETURNING role_id, name";

        var result = securityProxyDB.many(query)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug("addSecurityProxyRoles", error, query);
            });

        return result;
    },

    assignUsersToGroupRoles: function (params) {
        var ids = [];

        params.users.forEach(function (user) {
            ids.push({user_id: user.user_id, group_id: user.group_id, role_id: user.role_id});
        });

        const user_group_cs = new pgpromise.helpers.ColumnSet(["user_id", "group_id"], {table: "saii_user_group"});
        const user_group_query = pgpromise.helpers.insert(ids, user_group_cs) + "RETURNING user_id, group_id";

        const group_role_cs = new pgpromise.helpers.ColumnSet(["group_id", "role_id"], {table: "saii_group_role"});
        const group_role_query = pgpromise.helpers.insert(ids, group_role_cs) + "RETURNING group_id, role_id";

        var result = securityProxyDB.many(user_group_query)
            .then(function () {
                securityProxyDB.many(group_role_query)
                    .then(data => {
                        return data;
                    })
                    .catch(error => {
                        debug(error, group_role_query);
                    });
            })
            .catch(error => {
                debug("assignUsersToGroupRoles", error, user_group_query);
            });

        return result;
    },

    updateEndpoint: function (params) {
        return this.dbQuery(sql.securityproxy.updateEndpoint, params);
    }
};

module.exports = securityProxyDatabase;

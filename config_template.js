var config =
{
    dev_ad_user: "MusterMax",
    log_level: "info",
    port: 9000,
    listenHost: "localhost",
    secret: "01234567890123456789012345678901",
    reject_unauthorized_SSL: false,
    use_https_tunnel_proxy: false,
    proxy: {
        dev: {
            host: "77",
            port: 80
        },
        prod: {
            host: "123",
            port: 80
        }
    },
    tomcatUser: {
        username: "name",
        password: "password"
    },
    security_proxy_database: {
        dev: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password",
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000
        },
        prod: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password",
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000
        }
    },
    database: {
        dev: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password",
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000
        },
        prod: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password",
            max: 20,
            idleTimeoutMillis: 30000,
            connectionTimeoutMillis: 2000
        }
    },
    ldap: {
        url: "ldap_url",
        baseDN: "base_dn",
        username: "username",
        password: "password",
        attributes: {
            user: ["mail", "sn", "givenName", "sAMAccountName", "company", "telephoneNumber"],
            group: ["cn", "sAMAccountName"]
        },
        tlsOptions: {
            rejectUnauthorized: false
        },
        reconnect: true,
        allowed_groups: [
            {name: "gruppe_1", read_only: false},
            {name: "gruppe2", read_only: true}
        ]
    },
    webdav: [
        {
            base_url: "https://webdav_url",
            webdav_directory: "webdav",
            full_qualified_domain: "https://geoportalserver.de",
            username: "username",
            password: "password",
            digest: true,
            proxy: true
        }
    ],
    auth_proxy_reload_url: {
        dev: "http://localhost:9006/config/reload",
        test: "http://localhost:9006/config/reload",
        prod: "http://localhost:9006/config/reload"
    },
    fme: {
        geoproxy_conf_update: "geoporxy_update_url",
        freigaben_update: "freigaben_update_url"
    },
    fmeUser: {
        username: "name",
        password: "password"
    },
    jira: {
        url: "jira-url/rest/api/2",
        browse_url: "jira-url/browse",
        projects: [
            {
                name: "Projektname (Key)",
                name_alias: "Alias Jira Projektname (Anzeigename GUI)",
                new_as_subtask: true,
                use_in_ticketsearch: true,
                epic_link_customfield_id: "Customfield ID - Epic Link",
                epic_link_key: "Epic Link Key",
                active_epic_link: false,
                story_points_customfield_id: "Customfield ID - Story Points",
                story_points_value: 2,
                transition_execution: true,
                transition_id: ["Spalten ID (1-n) des Jira Boards (als Integer)"],
                components: [{"name": "Name Komponente"}]
            },
            {
                name: "Projektname (Key)",
                name_alias: "Alias Jira Projektname (Anzeigename GUI)",
                new_as_subtask: false,
                use_in_ticketsearch: false,
                epic_link_customfield_id: "Customfield ID - Epic Link",
                epic_link_key: "Epic Link Key",
                active_epic_link: false,
                story_points_customfield_id: "Customfield ID - Story Points",
                story_points_value: 2,
                transition_execution: true,
                transition_id: ["Spalten ID (1-n) des Jira Boards (als Integer)"],
                components: [{}]
            }
        ],
        default_assignee: "Jira Username",
        labels: ["Name Label1", "Name Label2"],
        auth: {
            username: "name",
            password: "password!?"
        },
        proxy: true
    },
    apply_dpi_factor: true,
    esri_url_namespace: true,
    srs_services: 25832,
    verbose_url_check: false,
    mandatory_epsg_codes: [
        "EPSG:25832",
        "EPSG:4326"
    ],
    capabilities_metadata: {
        providername: "Name",
        providersite: "",
        individualname: "",
        positionname: "Betrieb",
        phone: "",
        facsimile: "",
        electronicmailaddress: "mail@mail.de",
        deliverypoint: "Adresse",
        city: "Stadt",
        administrativearea: "Landescode",
        postalcode: "PLZ",
        country: "Germany",
        onlineresource: "URL",
        hoursofservice: "",
        contactinstructions: "",
        metadataurl: "CSW_URL_Capabilities",
        authorityname: "Name_Institution",
        authorityurl: "URL_Institution"
    },
    mail: {
        smtp_host: "host",
        smtp_port: 25,
        from: "\"Absender\" <mail@mail.de>",
        qa_to: "mail@mail.de",
        prod_to: "mail@mail.de",
        delete_layer_to: "mail@mail.de",
        service_checker_to: "mail@mail.de"
    },
    preview_portal_url: "http://server/DienstemanagerPreview",
    scheduled_tasks: {
        delete_tmp_files: "0 22 * * *",
        update_datasets: "0 21 * * *",
        update_service_metadata: "0 20 * * *",
        check_all_services: "0 20 * * *",
        update_deegree_capabilities: "0 20 * * *",
        generate_layer_json: "0 20 * * *",
        check_linked_portal: "0 20 * * *"
    },
    cat_org: [
        "Name Amt"
    ],
    openDataCat: {
        Abkürzung_Kategorie: "Langform der Kategorie"
    },
    use_websockets: false,
    es_params: {
        es_server_name: ["IP:Port"],
        es_index: ["es-index-name"]
    },
    client_config: {
        map_preview: "/DienstemanagerPreview",
        map_preview_base_layer_id: "453",
        map_preview_complex_append: "../config.json",
        endpointer: "/endpointer",
        srs_dienste: "25832",
        bbox_getmap: [
            {name: "HH", bbox: ["546000,5916000,589000,5959000"]}
        ],
        esri_url_namespace: true,
        bearbeiter: [
            "Hans Dampf"
        ],
        responsible_party: [
            "Team 1",
            "Team 2"
        ],
        dienst_typen: [
            {name: "WMS", versionen: ["1.1.1", "1.3.0"]},
            {name: "WFS", versionen: ["1.1.0", "2.0.0"]}
        ],
        software: [
            "deegree",
            "ESRI"
        ],
        server: [
            {
                name: "name", domain: ".de", lb: true, server: ["server1", "server2"]
            }
        ],
        es_params: {
            es_server_name: ["10.61.130.20:9200", "10.61.130.21:9200"],
            es_index: [
                {name: "layers-internet", only_prod: true, internal: false, keyword_filter_include: [], keyword_filter_exclude: []}
            ]
        },
        externe_url_prefix: "https://geodienste.de/",
        geoportal_url: "http://server/portal/",
        geoportal_url_ext: "http://server/portal/",
        fme_json_modul: true,
        freigaben_modul: true,
        metadaten_modul: true,
        gekoppelteportale_modul: true,
        proxyupdate_modul: true,
        deegree_modul: true,
        anmerkungen_modul: true,
        verwaltung_modul: true,
        zugriffsstatistik_modul: true,
        ldap_modul: true,
        security_modul: true,
        security_type: [
            "keine",
            "Basic Auth",
            "AD SSO"
        ],
        security_proxy_url_sso: "https://server/security-proxy/sso/services/",
        security_proxy_url_auth: "https://server/security-proxy/auth/services/",
        jira_modul: true
    }
},
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

if (env === "dev") {
    config.proxy = config.proxy.dev;
    config.auth_proxy_reload_url = config.auth_proxy_reload_url.dev;
}
else if (env === "test") {
    config.proxy = config.proxy.prod;
    config.auth_proxy_reload_url = config.auth_proxy_reload_url.test;
}
else {
    config.proxy = config.proxy.prod;
    config.auth_proxy_reload_url = config.auth_proxy_reload_url.prod;
}

module.exports = config;
